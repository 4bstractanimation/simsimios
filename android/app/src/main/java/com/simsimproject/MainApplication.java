package com.simsimproject;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.reactnativecommunity.geolocation.GeolocationPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.dooboolab.RNIap.RNIapPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;

import cl.json.RNSharePackage;
import cl.json.ShareApplication;
import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.centaurwarchief.smslistener.SmsListenerPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.imagepicker.ImagePickerPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.reactnativedocumentpicker.ReactNativeDocumentPicker;
import java.util.Arrays;
import java.util.List;
import com.RNFetchBlob.RNFetchBlobPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import org.pweitz.reactnative.locationswitch.LocationSwitchPackage;
import com.airbnb.android.react.maps.MapsPackage;
// import com.dooboolab.RNIap.RNIapPackage;


public class MainApplication extends Application implements ShareApplication, ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
     protected List<ReactPackage> getPackages() {
          return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new GeolocationPackage(),
            new NetInfoPackage(),
            new RNCWebViewPackage(),
            new RNIapPackage(),
            new ReactNativeOneSignalPackage(),
            new RNSharePackage(),
            new LocationServicesDialogBoxPackage(),
            new RNDeviceInfo(),
            new SmsListenerPackage(),
            new PickerPackage(),
            new RNI18nPackage(),
            new ReactVideoPackage(),
            new SplashScreenReactPackage(),
            new ImagePickerPackage(),
            new MapsPackage(),
            new VectorIconsPackage(),
            new ReactNativeDocumentPicker(),
            new RNFetchBlobPackage(),
            new ImageResizerPackage(),
            new LocationSwitchPackage()
           
         );
     }
     
    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

    @Override
    public ReactNativeHost getReactNativeHost() {
      return mReactNativeHost;
    }

    @Override
    public void onCreate() {
      super.onCreate();
      SoLoader.init(this, /* native exopackage */ false);
    }

    @Override
    public String getFileProviderAuthority() {
          return "com.example.com.simsimproject.provider";
    }


}

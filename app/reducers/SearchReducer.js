import { searchPhotoAction } from '../actions/Search';
import {
	REQUEST_DATA,
	SUCCESS_DATA,
	ERROR_DATA,
	SET_LOCATION,
	SET_TOAST,
	SET_MODAL,
	SET_OFFER,
	SET_SELECTED_ID
} from '../actions/Search';

const initialState = {
	photos: [],
	status: 'IDLE',
	filterData: [],
	loader: false,
	location: '',
	status: '',
	toast: false,
	ad_status:'',
	modalOpen: false,
	specialOfferData: [],
	idCat:null
};

export default function searchPhotos(state = initialState, action) {
	// console.warn('action-->>',action)
	switch (action.type) {
		case REQUEST_DATA:
			return {
				...state,
				loader: action.loader
			};
		case SUCCESS_DATA:
			return {
				...state,
				loader: action.loader,
				filterData: action.filterData,
				status: action.status
			};
		case ERROR_DATA:
			return {
				...state,
				loader: action.loader
			};
		case SET_LOCATION:
			return {
				...state,
				location: action.location
			};
		case SET_TOAST:
			return {
				...state,
				toast: action.toast
			};
		case SET_MODAL:
			return {
				...state,
				modalOpen: action.modalOpen
			}
		case SET_OFFER: 
			return {
				...state,
				specialOfferData: action.response
			}
		case SET_SELECTED_ID:
			return {
				...state,
				selectedCat: action.selectedCat
			}
		default:
			return state;
	}
}

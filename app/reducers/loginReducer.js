
const initialState = {
 loginUserData: {}
}

export default function loginReducer(state = initialState, action) {
  switch (action.type) {
    case 'SET_LOGIN_DATA':
      return {
        ...state,
        loginUserData: action.payload
      }
    case 'RESET_LOGIN_DATA': 
    	return {
    		...state,
    		loginUserData: {},
    	}
    default:
    return state
  }
}
export const SET_LAYOUT = 'SET_LAYOUT'
export const GET_LAYOUT = 'GET_LAYOUT'
export const SET_COUNTRY_DETAIL = 'SET_COUNTRY_DETAIL'
export const OTP_VERIFY = 'OTP_VERIFY';
export const LOCATION_CHECK = 'LOCATION_CHECK';
export const SET_SELLER_TYPE = 'SET_SELLER_TYPE';

export function setLoginData(loginData) {
  return {
    type: 'SET_LOGIN_DATA',
    payload: loginData
  }
}

export function resetLoginData() {
  return {
    type: 'RESET_LOGIN_DATA'
  }
}

export function getLayout1() {
  return dispatch => {
    dispatch({type: CONNECTION_CHECKING})
    setTimeout(() => dispatch({type: CONNECTION_CHECKED}), 5000)
  }
}

export function setLayout() {
  return {
    type: SET_LAYOUT
  }
}

export function checkOtpVerify(mobile, cCode) {
  let data={mobile:mobile,cCode:cCode}
  return (dispatch)=>{
    dispatch({type:'OTP_VERIFY',data})
  }

}

export function getLayout() {
  return {
    type: GET_LAYOUT
  }
}

export function setCountryDetail(data) {
  return {
    type: 'SET_COUNTRY_DETAIL',
    countryDetail: data
  }
}

export function setSellerType(data){
  return{
    type: SET_SELLER_TYPE,
    sellerType: data
  }
}

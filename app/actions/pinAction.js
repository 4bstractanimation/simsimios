export const SET_PIN_DATA = 'SET_PIN_DATA'

export function setPinData(data) {
	return ({
		type:SET_PIN_DATA,
		data: data
	})
}
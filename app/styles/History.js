import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container:{
				flex: 1,
				justifyContent: 'center',
				backgroundColor:'#ecf2f9',
				marginHorizontal:10,
			},
			_title:{
				fontSize:15,
				fontWeight:'bold',
				marginBottom:5
			},
			_remove:{
				color:'#00ADDE', 
				fontSize:15
			},
			_messageBox:{
				flex:1,
				flexDirection: isRTL ? 'row-reverse' : 'row',
				alignItems: 'center',
				padding: 10,
				marginTop: 10,
				backgroundColor: '#FFFFFF',
			},
			_image:{
				width: 70,
			 	height:70,
			 	resizeMode: 'contain',
			},
			_textBox:{
				flex: 3,
			},
			_descriptionContainer:{
				flex: 2.5,
				flexDirection:isRTL ? 'row-reverse' : "row",
				justifyContent: "space-between",
			},
			_description:{
				flex: 2.5,
				fontSize: 15,
		 		color: '#000000',
		 		flexWrap: 'wrap',
		 		marginRight:20,
			},
    });
  }
}
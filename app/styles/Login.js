import i18n from 'react-native-i18n';
import {Text, View, Alert, TouchableHighlight, StyleSheet, Platform} from 'react-native';
export default class StyleSheetFactory{  
    static getSheet(isRTL){
        isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
        return StyleSheet.create({
            container: {
                flex:1,
                backgroundColor: '#ecf2f9'
            },
            logo: {
                width: '42.5%',
                height:110.5,
                overflow:'visible',
                resizeMode: 'contain'
            },
            _textbox:{
                flex:1,
                borderRadius:30,
                width:'100%',
                height:50,
                backgroundColor:'white',
                marginTop:20,
            },
            _textboxText: {
                fontSize:17,
                marginLeft: isRTL ? 0 : 20,
                marginRight: isRTL ? 20 : 0,
                paddingTop: Platform.OS === 'ios' ? 15 : 10,
                textAlign: isRTL ? 'right' : 'left',
            },
            _button:{
                borderRadius:25,
                width:'100%',
                height:50,
                backgroundColor:'#00ADDE',
                marginTop:20,
                alignItems: 'center',
            },
            _buttonText: {
                fontSize:17,
                fontWeight: 'bold',
                color: 'white',
                marginTop:10,
                alignItems: 'center'
            },
            _text: {
                marginTop:15, 
                color: 'grey',
                textAlign: isRTL ? 'right' : 'left',
            },
            ccode: {
                flex:0.63,
                borderRadius:30,
                // width:'30%',
                // width:60,
                // height:50,
                backgroundColor:'white',
                marginTop:20,
            }
        });
    }
}
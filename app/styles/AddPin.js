import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  logo: {
				width: '42.5%',
				height:117,
				overflow:'visible',
				resizeMode: 'contain',
			},
			_textbox:{
				borderBottomLeftRadius: 25,
			    borderBottomRightRadius: 25,
			    borderTopLeftRadius: 25,
			    borderTopRightRadius: 25,
			    width: '100%',
			    height: 50,
			    backgroundColor:'white',
			    marginTop:10,
			},
			ccode: {
				flex:0.63,
				borderRadius:30,
				backgroundColor:'white',
				marginTop:20,
			},
			_textboxText: {
				fontSize:17,
		        marginLeft: isRTL ? 0 : 20,
		        marginRight: isRTL ? 20 : 0,
		        paddingTop: 12,
		        color: "#545454",
		        textAlign: isRTL ? 'right' : 'left',
			},
			_radiotextbox:{
				borderBottomLeftRadius: 25,
			    borderBottomRightRadius: 25,
			    borderTopLeftRadius: 25,
			    borderTopRightRadius: 25,
			    width: '100%',
			    height: 50,
			    backgroundColor:'white',
			    marginTop:10,
			    flexDirection: isRTL ? 'row-reverse' : 'row',
			},
			uploadIcon:{
				width: 35,
				height: 35,
				marginHorizontal:10,
				marginTop:5
			},
			_descriptionbox:{
				borderBottomLeftRadius: 25,
			    borderBottomRightRadius: 25,
			    borderTopLeftRadius: 25,
			    borderTopRightRadius: 25,
			    width: '100%',
			    height: '15%',
			    backgroundColor:'white',
			    marginTop:10,
			},
			_button:{
				borderBottomLeftRadius: 25,
			    borderBottomRightRadius: 25,
			    borderTopLeftRadius: 25,
			    borderTopRightRadius: 25,
			    width:'100%',
			    height:50,
			    backgroundColor:'#00ADDE',
			    marginTop:15,
			    alignItems: 'center',
			    marginBottom:30,
			},
			_buttonText: {
				fontSize:17,
				fontWeight: 'bold',
				color: 'white',
				marginTop:13,
				textAlign: 'center',
				fontFamily: 'System',
			},
			_radioButton:{
				flexDirection: isRTL ? 'row-reverse' : 'row',
				marginLeft: 10,
			},
			_radioText:{
				marginBottom:7,
				color:'#7A7A7A'
			},
			mediaContainer:{
				borderRadius: 25,
			  height: 50,
			},
			_socialMediaContainer:{
				flexDirection: isRTL ? 'row-reverse' : 'row',
				justifyContent: 'space-between',
				marginTop: 10,
				alignItems: 'center',
			},
			_media:{
				width: '100%',
				backgroundColor:'#00ADDE',
				alignItems:'center',
			},
			submit:{
				backgroundColor:"#00ADDE",
				width:'100%',
				alignItems: 'center',
				marginBottom:20
			},
    });
  }
}
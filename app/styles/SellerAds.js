import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container: {
				flex:1,
				padding: 10,
				marginLeft: 10,
				marginRight: 10,
				marginTop: 10,
				backgroundColor: '#FFFFFF',
			},
			container1: {
				flexDirection: isRTL ? 'row-reverse' : 'row',
			},
			adOffer: {
				flex:1,
				marginBottom: 10,
			},
			image:{
			 	flex:1,
		   	width:(width <= 320) ? 40 : 80,
		   	height:(height <=470) ? 40: 80,
		   	resizeMode: 'contain'
			},
			title:{
		    fontSize:17,
		    marginTop:5,
		    color: "#000",
		    fontWeight: "bold",
		    paddingBottom: 5,
		 	},
		 	description: {
		 		fontSize: 15,
		 		color: '#000000',
		 	},
		 	price: {
		 		fontSize: 12,
		 		color: '#44a7e5',
		 		marginTop:3.5
		 	},
		 	button:{
		 		borderRadius:25,
		 		backgroundColor:'black',
		 		// height:40
		 		
		 	},
		 	buttonText:{
		 		marginHorizontal:10,
		 		marginVertical:5,
		 		color:'#ffff',
		 		fontSize:12
		 	},
		 	icons:{
		 		width:(width >= 320) ? 23 : 30, 
		 		height: Platform.OS === 'ios' ? 23 : 23,
		 	}
    });
  }
}
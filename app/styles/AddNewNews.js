import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions,Platform } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
	  	container: {
			flex:1,
			backgroundColor:'#ecf2f9',
		},
		content:{
			marginHorizontal: 20,
			marginVertical: 25,
		},
		titleBox:{
			backgroundColor: '#fff',
			borderRadius:25,
			paddingHorizontal:30,		
			paddingVertical:15,
			
		},
		title: {
			backgroundColor: '#fff',
			borderRadius:25,
			paddingHorizontal : Platform.OS === 'ios' ? 0 : 30,
			borderWidth:1,
			borderColor: '#fff',
			 
		},
		descBox: {
			marginTop:20,
			backgroundColor: '#fff',	
			borderRadius:20,
		},
		desc: {
			height : 100,
			backgroundColor: '#fff',
			borderRadius:20,
			borderWidth:1,
			borderColor: '#fff',
			paddingHorizontal:30,
		},
		mediaBox: {
			flexDirection: isRTL ? 'row-reverse' : 'row',
	   		backgroundColor: '#fff',
			borderRadius:25,
			marginTop: 20,
			justifyContent: 'space-between'
		},
		mediaInput: {
			flex:1,
			paddingHorizontal:30,
		},
		linkIcon: {
			padding: 10,
			paddingRight: 20
		},
		socialBox: {
			flexDirection: isRTL ? 'row-reverse' : 'row',
			marginTop: 20,
			justifyContent: 'space-between'
		},
		socialMediaButton: {
			flex:2,
			borderRadius: 25,
			backgroundColor:'gray',
			alignItems: 'center',
			marginLeft: 10,
		},
		socialMedia: {
			flex: 1.5,
			flexDirection:  isRTL ? 'row-reverse' : 'row',
			backgroundColor: '#fff',
			borderRadius: 25,
			alignItems: 'center',
			justifyContent: 'space-between',
			paddingHorizontal:10,
		},
		socialMediaText: {
			color: '#fff',		
			paddingVertical: 12,
			fontSize: 20,
			paddingHorizontal: 20,
		},
		submit: {
			flexDirection: 'row',
			marginTop: 20,
			borderRadius: 25,
			backgroundColor:'#229ce2',
			alignItems: 'center',
			justifyContent: 'center',
		},
		submitText: {
			color: '#fff',		
			paddingVertical: 12,
			fontSize: 20,
		},
		socialIcon: {
			marginHorizontal: 10,
		}
    });
  }
}
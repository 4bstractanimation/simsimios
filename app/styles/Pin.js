import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container:{
				flex: 1,
				justifyContent: 'center',
				// marginVertical:10,
				// marginHorizontal:10,
				padding:10,
				backgroundColor:'#ecf2f9'
			},
			toast:{
				shadowColor:'#AAACAB',
				shadowOpacity:1,
				shadowOffset:{ width: 100, height:100}
			},
			_title:{
				fontWeight: 'bold',
				fontSize: 16,
				flex:0.9,
				color: '#000000'
			},
			_searchText:{
				borderBottomLeftRadius: 25,
		    borderBottomRightRadius: 25,
		    borderTopLeftRadius: 25,
		    borderTopRightRadius: 25,
		    width:250,
		    height:50,
		    backgroundColor:'white',
		    marginTop:10,
				textAlign: 'center',
		   	marginLeft: 10,
		   	fontSize: 15,
		   	color: "#6A6A6A"
			},
			_buttonFilter:{
				borderBottomLeftRadius: 20,
		    borderBottomRightRadius: 20,
		    borderTopLeftRadius: 20,
		    borderTopRightRadius: 20,
		    width: "25%",
		    height:45,
		    backgroundColor:'white',
		    marginTop:10,
		    alignItems: 'center',
		    marginLeft: 10,
		    paddingTop:4,
			},
			_messageBox:{
				flex:1,
				flexDirection: isRTL ? 'row-reverse' : 'row',
				alignItems: 'center',
				padding: 10,
				marginTop: 5,
				backgroundColor: '#FFFFFF',
			},
			_image:{
				width: 80,
			 	height:80,
				 resizeMode: 'contain',
				 left: isRTL ? 25 : null,
			},
			_icon:{
				width:20,
				height: 30,
				marginTop:20
			},
			_iconContainer:{
				flex:1,
				justifyContent: 'center',
				marginRight:20
			},
			_textBox:{
				flex:2.9
			},
			_descriptionContainer:{
				flexDirection:isRTL ? 'row-reverse' : "row",
				justifyContent: "space-between",
			//borderWidth:1,
				height:  55,
			},
			_description:{
			
				fontSize: 15,
		 		color: '#000000',
		 		flexWrap: 'wrap',
				// marginRight:20,
				 //borderWidth:1,
			},
			bottomImage: {
		  	position: 'absolute',
		  	bottom: 20,
		  	right: 0,
		  	height: 50,
			},
			image: {
				height: (width === 320) ? 30 : 25,
				width: (width === 320) ? 30 : 25
			}
    });
  }
}
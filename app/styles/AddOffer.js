import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
	  	container:{
			flex:1, 
			marginHorizontal:10,
			backgroundColor:'#ecf2f9'
		},
		category:{
			borderRadius: 25,
			width: '100%',
	  	height: 50,
	  	backgroundColor:'white',
	  	marginTop:10,
		},
		buttonContainer:{
			flexDirection: isRTL ? 'row-reverse' : 'row',
			justifyContent:'space-between',
			marginTop:10
		},
		Button:{
			borderRadius:15,
			alignItems:'center',
			backgroundColor:'#ffff',
			height:50,
			width:'100%',
			justifyContent:'center'
		},
		text:{
			color:'black',
			fontSize:18,
			textAlign:'center',
			marginHorizontal:'18%'
		},
		category:{
			borderRadius: 25,
			width: '100%',
	  	height: 50,
	  	backgroundColor:'white',
	  	marginTop:10,
		},
    });
  }
}
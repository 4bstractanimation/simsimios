import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  logo: {
				width: '42.5%',
				height: 110,
				overflow:'visible',
				resizeMode: 'contain'
			},
			_textbox:{
				borderBottomLeftRadius: 25,
			    borderBottomRightRadius: 25,
			    borderTopLeftRadius: 25,
			    borderTopRightRadius: 25,
			    width:"100%",
			    height:50,
			    backgroundColor:'white',
					marginTop:25,
					
			},
			_textboxText: {
				fontSize:17,
				marginLeft: 20,
				paddingTop: 15
			},
			_button:{
				borderBottomLeftRadius: 25,
			    borderBottomRightRadius: 25,
			    borderTopLeftRadius: 25,
			    borderTopRightRadius: 25,
			    width:"100%",
			    height:50,
			    backgroundColor:'#00ADDE',
			    marginTop:20,
			    alignItems: 'center'
			},
			_buttonText: {
				fontSize:17,
				fontWeight: 'bold',
				color: 'white',
				paddingTop:13,
				alignItems: 'center'
			},
			ccode: {
				flex:0.63,
				borderRadius:30,
				// width:'30%',
				// width:60,
				 height:50,
				backgroundColor:'white',
				marginTop:25,
		}
    });
  }
}
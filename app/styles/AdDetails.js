import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
	  	container: {
			flex: 1, 
			backgroundColor:'#ecf2f9',
		},
		content: {
			alignItems: 'center',
			padding: 10,
			marginLeft: 10,
			marginRight: 10,
			marginTop: 10,
			backgroundColor: '#FFFFFF',
		},
		rating: {
			flex: 1,
			flexDirection: isRTL ? 'row-reverse' : 'row',
		},
		ratingHeading: {
			fontWeight: 'bold',
			fontSize: 20,
		},
		avrRating: {
			flex: 1,
			alignItems: 'center',
		},
		avrRatingText: {
			fontSize: 40,
			color: '#33a037',
		},
		image: {
			width:width-20,
			resizeMode: 'contain'
		},
		buttonBox: {
			margin:10, 
			justifyContent: 'center', 
			flexDirection:isRTL ? 'row-reverse' : 'row',
		},
		button: {
			alignItems: 'center',
			backgroundColor: '#737477',
			paddingVertical:10,
			paddingHorizontal:30,
			borderRadius:30,		
		},
		buttonText: {
			color:'#ffffff', 
			fontWeight: 'bold',
		},
		contentItem: {
			flexDirection: isRTL ? 'row-reverse' : 'row', 
			marginBottom:15,
		},
		textHeading: {
			fontSize: 16, 
			fontWeight: 'bold', 
			color: '#000000'
		},
		textColor: {
			color: '#000000'
		},
		icons: {
			marginTop: 4,
			marginRight: 5,
		},
		iconsText: {
			fontSize: 12
		}, 
		totalRating: {
			flexDirection: isRTL ? 'row-reverse' : 'row', 
			marginTop:5,
			justifyContent: 'center'
		},
		ratingBar: {
			flex:1,
			height: 10,
			backgroundColor: '#ecf2f9',
			borderRadius:25,
			marginTop: 3,
			
		},
		ratingColorBar: {
			height: 10,
			borderRadius: 25,
		},
		review:{
			marginHorizontal:10,
		},
		_textbox:{
			borderBottomLeftRadius: 25,
		    borderBottomRightRadius: 25,
		    borderTopLeftRadius: 25,
		    borderTopRightRadius: 25,
		    width: '100%',
		    height: 50,
		    backgroundColor:'white',
		    marginTop:10,
		},
		_textboxText: {
			fontSize:17,
			marginLeft: 20,
			paddingTop: 15,
		},
		_descriptionbox:{
			borderBottomLeftRadius: 25,
		    borderBottomRightRadius: 25,
		    borderTopLeftRadius: 25,
		    borderTopRightRadius: 25,
		    width: '100%',
		    height: '30%',
		    backgroundColor:'white',
		    marginTop:10,
		},
		adNumber: {
			flex:0.8, 
			marginLeft:10, 
			marginTop:2,
		},
		adNumberText: {
			color:'#41b5ea', 
			fontSize:16
		},
		 reviewRating: {
	      marginTop: 20,
	      marginBottom: 15,
	      height: 50,
		    justifyContent: 'center',
		    alignItems: 'center',
		    alignItems: 'flex-start',
		    padding: 12
      },
      userName: {
      	fontWeight: 'bold',
      	// marginBottom:10
      },
      comment: {
      	marginTop:0
      },
      viewMore: {
				justifyContent: 'center', 
				alignItems: 'center', 
				backgroundColor: '#ecf2f9',
				marginHorizontal:100,
				borderRadius:25,
				paddingVertical:10,
				marginTop: 20,

			},
			viewMoreText: {
				fontWeight: 'bold'
			}
    });
  }
}
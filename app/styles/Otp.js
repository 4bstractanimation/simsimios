import i18n from 'react-native-i18n';
import {Text, View, Alert, TouchableHighlight, StyleSheet} from 'react-native';
export default class StyleSheetFactory { 
 static getSheet(isRTL) {
 isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
 return StyleSheet.create({
	container:{
	flex:1,
	marginTop:20,
	backgroundColor:'#ecf2f9',
	marginHorizontal:10
	},
	textContainer:{
	alignItems:'center',
	},
	_textbox:{
	borderBottomLeftRadius: 25,
	borderBottomRightRadius: 25,
	borderTopLeftRadius: 25,
	borderTopRightRadius: 25,
	width:'100%',
	height:50,
	backgroundColor:'white',
	marginTop:20,
	alignItems:'center',
	},
	_textboxText: {
	fontSize:17,
	textAlign: 'center',
	textAlign:'center',
	fontWeight:'bold',
	width:'100%',
	height:50,
	},
	_button:{
	borderRadius: 25,
	width:'100%',
	height:50,
	backgroundColor:'#00ADDE',
	marginTop:20,
	alignItems: 'center',
	},
	_buttonText: {
	fontSize:17,
	fontWeight: 'bold',
	color: 'white',
	marginVertical:12 
	}
 });
 }
}
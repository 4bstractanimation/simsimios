import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container: {
				flex:1,
				backgroundColor:'#ecf2f9',
			},
			content:{
				backgroundColor: '#fff',
				marginHorizontal: 10,
				marginTop: 30,
				padding: 20,	
			},
			contentText: {
				color: '#000000',
				lineHeight:25,
				textAlign: 'justify',
			},
			urlBox: {
				flex: 3,
		    flexDirection: isRTL ? 'row-reverse' : 'row',
		    backgroundColor: '#fff',
				borderRadius:25,
				margin: 10
			},
			urlInput: {
				flex:1,
				fontWeight: 'bold',
				color:'gray',
				marginLeft: 10
			},
			linkIcon: {
				padding: 10,
			},
			copyButton: {
				flex:1,
				margin: 10,
				borderRadius:25,
				backgroundColor: '#33a5ea',
				alignItems: 'center',
				justifyContent: 'center'
			},
			copyButtonText: {
				color: '#fff',
				fontSize: 15,
			},
			otherOptions: {
				alignItems: 'center',
				marginTop: 30
			},
			otherOptionsText: {
				fontWeight: 'bold',
				fontSize: 25
			},
			inviteFriends: {
				flex:1,
				margin: 30,
				borderRadius:25,
				backgroundColor: '#33a5ea',
				alignItems: 'center',
				justifyContent: 'center',
				paddingVertical: 15,
			},
    });
  }
}
import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{ 
 static getSheet(isRTL){
 isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
 return StyleSheet.create ({
	logo: {
	width: '42.5%',
	height:111,
	resizeMode: 'contain'
	},
	_textbox:{
	borderBottomLeftRadius: 25,
	borderBottomRightRadius: 25,
	borderTopLeftRadius: 25,
	borderTopRightRadius: 25,
	width:"100%",
	height:50,
	backgroundColor:'white',
	marginTop: Platform.OS === "ios" ? 30 : 20,
	},
	_textboxText: {
	fontSize:17,
	marginLeft: isRTL ? 0 : 20,
 	marginRight: isRTL ? 20 : 0,
	textAlign: isRTL ? 'right' : 'left',
	paddingTop: Platform.OS === "ios" ? 15 : 11,
	},
	_button:{
	borderBottomLeftRadius: 25,
	borderBottomRightRadius: 25,
	borderTopLeftRadius: 25,
	borderTopRightRadius: 25,
	width:"100%",
	height:50,
	backgroundColor:'#00ADDE',
	marginTop:20,
	alignItems: 'center'
	},
	_buttonText: {
	fontSize:17,
	fontWeight: 'bold',
	color: 'white',
	paddingTop:13,
	alignItems: 'center'
	},
 });
 }
}
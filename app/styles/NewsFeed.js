import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container: {
				flex: 1,
				marginBottom: 10		
			},
			newsContent: {
				padding: 10,
				marginLeft: 10,
				marginRight: 10,
				marginTop: 10,
				backgroundColor: '#FFFFFF',
			},
			userDetail: {
				flexDirection: isRTL ? 'row-reverse' : "row",
				justifyContent: 'space-between',
			},
			userImage: {
				flex:1,	
				flexDirection: isRTL ? 'row-reverse' : "row",	
			},
			image: {		
				width: 50,
				height: 50,
				borderRadius:60,		
				resizeMode: 'cover',	
				
			},
			userNameBox: {
				marginLeft: 10,
				// marginRight: 10,
				justifyContent: 'center',
			},
			userNameText: {
				fontSize: 16,
				color: '#000000',
				fontWeight: 'bold',		
			},
			time: {
				justifyContent: 'center',
			},
			mediaBox: {
				flex:1,
				marginVertical: 10,
			},
			media: {
				width: null,
				height:250,
				resizeMode: 'cover'

			},
			newsText: {
				color: '#000000',
				textAlign: 'justify',
				lineHeight:25,
			}
    });
  }
}
import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
	  	newAdContainer:{
			position:'absolute',
			marginHorizontal:10,
			marginVertical:10
		},
		adnum_imgContainer:{
			flexDirection: isRTL ? 'row-reverse' : 'row',
			padding: 5,
			justifyContent: 'space-between'
		},
		adnumberContainer:{
			borderRadius: 25,
			width: '63%',
		  	height: 50,
		  	backgroundColor:'white',
		  	marginTop:10,
		},
		adnum:{
			fontSize:15,
			marginLeft: isRTL ? 0 : 10,
      		marginRight: isRTL ? 10 : 0,			
			fontWeight: 'bold',
			color:'#7A7A7A',
			textAlign: isRTL ? 'right' : 'left',
		},
		addimg:{
			width: '35%',
			backgroundColor:'#00ADDE',
			paddingHorizontal:5,

		},
		_buttonText: {
			fontSize:15,
			fontWeight: 'bold',
			color: 'white',
			marginVertical:15,
			alignItems: 'center',
			marginHorizontal:10,
			marginTop:13,
			textAlign: 'center',
		},
		status_priceContainer:{
			
			
			flexDirection: isRTL ? 'row-reverse' : 'row',
			justifyContent:'space-between',
		},
		statusContainer:{
			
			borderBottomLeftRadius: 25,
			
			borderTopLeftRadius: 25,
			width: '45%',
			height: 50,
			backgroundColor:'#00ADDE',
			marginTop:10,
			alignItems: 'center',
			flexDirection: isRTL ? 'row-reverse' : 'row',
		},

		statusContainerRev:{
			
			borderBottomRightRadius: 25,
			borderTopRightRadius: 25,
			width: '45%',
			height: 50,
			backgroundColor:'#00ADDE',
			marginTop:10,
			alignItems: 'center',
			flexDirection: isRTL ? 'row-reverse' : 'row',
		},

		status:{
			
			fontSize:15,
			fontWeight: 'bold',
			color: 'white',
			marginVertical:15,
			alignItems: 'center',
			marginHorizontal:10,
			marginTop:10,
			marginLeft:15,
		},
		choose:{
			//borderWidth:1,
			borderBottomRightRadius: 25,
			borderTopRightRadius: 25,
			width: '55%',
	  	height: 50,
	  	marginTop:10,
	  	backgroundColor:'white',
	  	flexDirection: isRTL ? 'row-reverse' : 'row',
		},

		chooseRev:{
			//borderWidth:1,
			borderBottomLeftRadius: 25,
			borderTopLeftRadius: 25,
			width: '55%',
	  	height: 50,
	  	marginTop:10,
	  	backgroundColor:'white',
	  	flexDirection: isRTL ? 'row-reverse' : 'row',
		},

		chooseDOC:{
			borderBottomRightRadius: 25,
			borderTopRightRadius: 25,
			borderRadius:25,
			width: '100%',
	  	height: 50,
	  	marginTop:10,
	  	backgroundColor:'white',
	  	flexDirection: isRTL ? 'row-reverse' : 'row',
			paddingLeft: 20,
			paddingTop:15
	},

		priceContainer:{
			//borderWidth:1,
			borderRadius:25,
			width: '35%',
			backgroundColor:'white',
			paddingHorizontal:5,
			marginLeft:5,
			marginTop:10
		},
		product:{
			width:'100%',
		},
		subCat:{
			width:'47%',
		},
		_descriptionbox:{
			borderBottomLeftRadius: 25,
		    borderBottomRightRadius: 25,
		    borderTopLeftRadius: 25,
		    borderTopRightRadius: 25,
		    width:'100%',
		    height: 150,
		    backgroundColor:'white',
		    marginTop:10,
		   
		},
		_socialMediaContainer:{
			flexDirection: isRTL ? 'row-reverse' : 'row',
			justifyContent: 'space-between',
		},
		_iconContainer:{
			width: '50%',
			backgroundColor: 'white',
			flexDirection: isRTL ? 'row-reverse' : 'row',
			justifyContent: 'center',
		},
		_media:{
			width: '45%',
			backgroundColor:'#7A7A7A',
			alignItems:'center',
		},
		submit:{
			backgroundColor:"#00ADDE",
			width:'100%',
			alignItems: 'center',
			marginBottom:30
		},
		imgContainer:{
			flexDirection: isRTL ? 'row-reverse' : 'row',
			marginTop:20,
		},
		image:{
			height:100,
			width:100,
			borderRadius:25,
			marginLeft:10,
		},
		blankSquare: {
			width: 90,
			height: 90,
			borderRadius: 25,
			marginLeft:10,
			//borderWidth: 0.5,
			//borderColor: '#73AD21',
			backgroundColor: '#808080',
		// borderColor: 'red',
		// borderWidth: 2,

		},

		serviceContainer:{
			
			//borderWidth:1,
			flex:0.75,
			
			height:60,
			flexDirection: "row",
			justifyContent: 'flex-start',
		}
    });
  }
}
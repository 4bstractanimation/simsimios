import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet,Platform, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container:{
				flex: 1,
				backgroundColor:'#ecf2f9',
				marginHorizontal: 5,
				padding: 5
			},
			searchIcon:{
				marginLeft:190,
				marginTop:15,
				width:15,
				height:15,
				overflow:'visible'
			},
			searchContainer:{
				flex: 1,
				borderBottomLeftRadius: 25,
		    borderBottomRightRadius: 25,
		    borderTopLeftRadius: 25,
		    borderTopRightRadius: 25,
		    width: (width === 320) ? 180 : 230,
		    height:50,
		    marginRight:10,
		    backgroundColor:'white',
			},
			_searchText:{
		   	fontSize: 15 ,
		   	color: "#6A6A6A", 
		   	marginLeft: isRTL ? 0: ((width === 320) ? 50 : 20),
		   	marginRight: isRTL ? ((width === 320) ? 50 : 20) : 0,
			},
			_buttonFilter:{
		    borderBottomLeftRadius: 20,
		    borderBottomRightRadius: 20,
		    borderTopLeftRadius: 20,
		    borderTopRightRadius: 20,
		    width: (width === 320) ? "35%" : "30%",
		    height:45,
		    backgroundColor:'white',
		    justifyContent:'center',
		    marginLeft: 10,
		    // alignItems: 'center',
		    marginVertical: 10
			},
			_title:{
				fontSize:18,
				fontWeight:'bold',
				marginBottom:5,
			},
			_messageBox:{
				flex:1,
				flexDirection: isRTL ? 'row-reverse' : 'row',
				alignItems: 'center',
				padding: 7,
				backgroundColor: '#FFFFFF',
				marginTop: 2
			},
			_image:{
				width: (width === 320) ? 60: 70,
			 	height: (width === 320) ? 60: 70,
			 	resizeMode: 'contain',
			},
			_date:{
				color:'#545454'
			},
			_icon:{
				flex: 0.3,
				width:(width === 320) ? 20: 25,
				height: 30,
				marginTop:12,
				overflow:'visible',
				resizeMode:'contain',
				position: Platform.OS === 'ios'? 'absolute': null,
				alignItems: isRTL ? 'flex-start' : 'flex-end',
			},
			_textBox:{
				flex: 2.5,
				padding: 2,
				flexWrap: 'wrap',
			},
			_descriptionContainer:{
				flexDirection:isRTL ? 'row-reverse' : "row",
				flexWrap: 'wrap',
				//borderColor: 'green', borderWidth:2
			},
			_description:{
				fontSize: 15,
		 		color: '#000000',
		 		flexWrap: 'wrap',
			},
    });
  }
}
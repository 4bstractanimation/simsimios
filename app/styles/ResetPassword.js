import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
			container:{
				flex:1,
				backgroundColor:'#ecf2f9'
			},
			logo: {
				width: '42.5%',
                height:110.5,
                overflow:'visible',
                resizeMode: 'contain'
			},
			_textbox:{
				borderRadius:30,
			    width:'100%',
			    height:60,
			    backgroundColor:'white',
			    marginTop:20,
			    alignItems: 'center'
			},
			_textboxText: {
				fontSize:17,
				fontWeight:'bold',
				marginLeft: 20,
				width:'100%',
			    height:60,
			},
			_button:{
				borderBottomLeftRadius: 25,
			    borderBottomRightRadius: 25,
			    borderTopLeftRadius: 25,
			    borderTopRightRadius: 25,
			    width:'100%',
			    height:50,
			    backgroundColor:'#00ADDE',
			    marginTop:30,
			    alignItems: 'center'
			},
			_buttonText: {
					fontSize:17,
					fontWeight: 'bold',
					color: 'white',
					paddingTop:15,
					alignItems: 'center'
			},
    });
  }
}
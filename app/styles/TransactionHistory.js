import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
			container:{
		flex: 1,
		},
		
		_price:{
			fontSize:15,
			fontWeight:'bold',
			flex:0.2,
		},
		_title:{
			fontSize:18,
			fontWeight: 'bold'
		},
		_messageBox:{
				flex:1,
				
				padding: 10,
				marginHorizontal:10,
				marginTop:10,
				backgroundColor: '#FFFFFF',
		},
		_textBox:{
				flex: 1,
				
		}
    });
  }
}
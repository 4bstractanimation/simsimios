import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
	  	logo: {
			width: '44.5%',
			height:110,
			overflow:'visible',
		},
		chooseMessage:{
			borderRadius:25,
			backgroundColor:'#fff',
			height:50,
			width:'100%',
			marginTop:5,
		},
		_textbox:{
			borderBottomLeftRadius: 25,
	    borderBottomRightRadius: 25,
	    borderTopLeftRadius: 25,
	    borderTopRightRadius: 25,
	    width: '100%',
	    height: 50,
	    backgroundColor:'white',
	    marginTop:10,
		},
		_textboxText: {
			fontSize:17,
			marginLeft: isRTL ? 20 : 20,
      		marginRight: isRTL ? 20 : 0,
			paddingTop: 10,
			textAlign: isRTL ? 'right' : 'left',
			fontWeight: 'bold'
		},
		_descriptionbox:{
			borderBottomLeftRadius: 25,
	    borderBottomRightRadius: 25,
	    borderTopLeftRadius: 25,
	    borderTopRightRadius: 25,
	    width: '100%',
	    height: '20%',
	    backgroundColor:'white',
	    marginTop:10,
		},
		uploadIcon:{
			width: 30,
			height:30,
			marginHorizontal:10,
			marginTop:10
		},
		tostMsg:{
				marginHorizontal: "5%"
		},
		_button:{
			borderRadius:25,
		    width:'100%',
		    height:50,
		    backgroundColor:'#00ADDE',
		    marginTop:20,
		    alignItems: 'center',
		    marginBottom: 170,
		},
		_buttonText: {
			fontSize:17,
			fontWeight: 'bold',
			color: 'white',
			marginTop:12
		},
    });
  }
}
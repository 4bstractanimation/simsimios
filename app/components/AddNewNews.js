import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, ActivityIndicator, Image, StyleSheet,Platform, ScrollView, AsyncStorage,  Share, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/AddNewNews';
import NotificationHeader from './NotificationHeader';
import PreLoader from './PreLoader';
import ImageResizer from 'react-native-image-resizer';
import RNFetchBlob from 'rn-fetch-blob';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

class AddNewNews extends Component {

	constructor(props){
		super(props);
		this.state = {
			styleTitle: '',
			styleDesc: '',
			avatarSource: null,
			title: '',
			description: '',
			media: '',
			newsId: '',
			photos:'',
			// toggle: false,
			isLoading:false,
			pinId: '',
			imageName: '',
			editImage:false
		};
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	static navigationOptions = ({navigation}) => {
		let  state = navigation.state;
		if (state.params !== undefined) {
			return {		    
		    title: state.params.news ? (state.params.isRTL === true) ? 'آخر الأخبار' : 'Update News' : (state.params.isRTL === true) ? 'أضف أخبار' : 'Add News', 
		    headerRight: <NotificationHeader navigation={navigation}/>
	  	}
		}
	}

	componentDidMount() {
		const {setParams} = this.props.navigation;
    	setParams({isRTL: this.props.isRTL});
		const { params } = this.props.navigation.state;
		if(params.news) {
			const news = params.news;
			this.setState({
				'newsId': news.id, 
				'title': news.title,
				'description': news.description,
				'media': news.news_document
			});
		} else {
			this.setState({pinId: params.pinId, userId: params.loginUserId})
		}
	
	}
	
	shareOnSocialMedia = () => {
	  	let shareOptions = {
				title: this.state.title,
				message: this.state.description,
				url: "http://facebook.github.io/react-native/",
				subject: "This is news data" // for email
			}
			Share.share(shareOptions);
	 }

	async handleSubmit() {
		this.setState({isLoading:true});
		const { isRTL } = this.props;
		const { navigate } = this.props.navigation;
		const getUserData = await AsyncStorage.getItem('user_data');
		const userData = JSON.parse(getUserData);
		const token = userData.sData;
		const { params } = this.props.navigation.state;
		if(params.news) {
			fetch(API_URL+'/editNews', { 
			method: 'POST', 
			headers: { 
				'Accept': 'application/json', 
				'Content-Type': 'application/json', 
				'Authorization': token 
			}, 
			body: JSON.stringify({ 
				newsId: this.state.newsId,
				title: this.state.title,
				description: this.state.description,
				newsDocument: this.state.editImage? this.state.media: ''
				 }) 
			}).then((response) => response.json()) 
			.then((responseData) => { 
				// console.warn("Edit --->> ",responseData)
				if(responseData.status == 654864 && responseData.eData.eCode == 545864) { 
					this.setState({isLoading:false})
					const message = isRTL ? 'فشل في تحديث الاخبار' : "Failed to updated News";
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);	
				 }else if(responseData.status == 805465 && responseData.eData === null) { 
				 	
				 	const message = isRTL ? 'تم تحديث الاخبار بنجاح' : "News updated successfully!";
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);			 	
					this.setState({title:'', description: '', media:'', isLoading: false}); 
				 	this.props.navigation.goBack();
				 }   
			})
			.catch((err) => { 
				this.setState({isLoading:false})
				Alert.alert('Error', err.message);
			});
		} else {
			fetch(API_URL+'/add-news', { 
			method: 'POST', 
			headers: { 
				'Accept': 'application/json', 
				'Content-Type': 'application/json', 
				'Authorization': token 
			}, 
			body: JSON.stringify({ 
				title: this.state.title,
				description: this.state.description,
				newsDocument: this.state.media, 
				pinId: this.state.pinId,
				userId: this.state.userId
				 }) 
			}).then((response) => response.json()) 
			.then(async (responseData) => { 
				 // console.warn('Add--->>',responseData)
				if(responseData.status == 654864 && responseData.eData.eCode == 545864){
					this.setState({isLoading:false}) 
					
					const message = isRTL ? 'فشل في إضافة الاخبار' : "Failed to add news";
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);			
				 }else if(responseData.status == 805465 && responseData.eData === null){ 
					const message = isRTL ? 'تم نشر الاخبار بنجاح' : "News submitted successfully!";
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);	
					this.setState({title:'', description: '', media:'', imageName: '', isLoading: false});
				 	this.props.navigation.goBack();
				 }   
				}).catch((err) => { 
					this.setState({isLoading:false})
				Alert.alert('Error', err.message);
			});
		}	
		// this.setState({title:'', description: '', media:''});
	}
	
	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
    	this.props.navigation.setParams({ isRTL: nextProps.isRTL });
    	this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}

	async uploadMedia() {
		ImagePicker.openPicker({
			includeBase64: true,
			compressImageQuality: 0.2
			})
			.then(item => {
			const str = item.path;
			var filename = str.replace(/^.*[\\\/]/, '');
			let img = item.data;
			this.setState({
			media: img,
			imageName: filename,
			editImage: true
			});

		})
	.catch(e => alert(e));
	}



  // uploadMedia(){
  // 	ImagePicker.openPicker({
  //     includeBase64: true,
  //   }).then(r =>  {
	//     const str = r.path;
	// 	var filename = str.replace(/^.*[\\\/]/, '');

	// 	{/* Compressing image here */}
	// 	ImageResizer.createResizedImage(str, 600, 400, 'JPEG', 50, 0).then( async (response) => {

	// 		let base64Image = await this.convertIntoBase64(response.uri);
			
	// 		this.setState({
	//           media: base64Image,
	//           imageName: filename,
	//           editImage:true
	//         });
	// 	})

    	
    	
  //   }).catch(e => alert(e));

  // }

  // 	async convertIntoBase64(fileUri) {
	// 	return RNFetchBlob.fs.readFile(fileUri, 'base64')
	// 		.then( async (data) => {
	// 			return data;
				
	// 		})
	// }

	render() {

		const { params } = this.props.navigation.state;
		const news = params ? params.news : null;
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		return(
			<View style={styles.container}>
				
				<View style={styles.content}>
				<ScrollView keyboardShouldPersistTaps="always">
					<View style={[ Platform.OS === 'ios' ? styles.titleBox : null , this.state.styleTitle]}>
						<TextInput 
							style={[styles.title, {textAlign : this.props.isRTL ? 'right' : null}]}
							placeholder={strings('addNewNews.title')}
							onFocus={() => this.setState({styleTitle: {borderWidth:1, borderColor:'#71c1f2'}})}
							onBlur={() => this.setState({styleTitle: {borderColor: '#fff'}})}
							underlineColorAndroid='transparent'
							onChangeText={(text) => this.setState({title: text})}
							value={this.state.title}
							multiline={false}
						/>
					</View>
					<View style={[Platform.OS === 'ios' ? styles.descBox : {marginTop:20,}]}>
						<TextInput 
							//multiline={true}
							keyboardShouldPersistTaps='never'
							returnKeyType={'done'}
    						numberOfLines={3}
    						maxLength={100}
							style={[styles.desc, this.state.styleDesc ,{textAlign : this.props.isRTL ? 'right' : null}]}
							placeholder={strings('addNewNews.description')}
							textAlignVertical="top"
							onFocus={() => this.setState({styleDesc: {borderWidth:1, borderColor:'#71c1f2'}})}
							onBlur={() => this.setState({styleDesc:{borderColor: '#fff'}})}
							underlineColorAndroid='transparent'
							onChangeText={(text) => this.setState({description:  text})}
							value={this.state.description}
						/>
					</View>
					<TouchableOpacity style={styles.mediaBox} onPress={this.uploadMedia.bind(this)} >					
						
						<TextInput 
							placeholder={strings('addNewNews.upload')}
							style={[styles.mediaInput,{textAlign : this.props.isRTL ? 'right' : null}]}
							underlineColorAndroid='transparent'
							value={this.state.imageName}
							editable={false}
						/>
						
							<Image style={ {marginLeft : this.props.isRTL ? 10 :5 ,marginRight:15, marginVertical:11, height:25, width:28.5}} source={require('./../icons/upload2x.png')}/>
								
					</TouchableOpacity>
					
					{ this.state.isLoading ? 
						<PreLoader /> 
						: <TouchableOpacity style={styles.submit}
							onPress={this.handleSubmit}
						>
							<Text style={styles.submitText}>
							
								{ news ?  strings('addNewNews.update') : strings('addNewNews.submit')}
							
							</Text>
						</TouchableOpacity>
					}
				</ScrollView>
				</View>
			</View>
		)
	}
}


function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(AddNewNews)




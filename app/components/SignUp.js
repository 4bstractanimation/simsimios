import React, { Component } from 'react';
import {
	Text,
	View,
	Alert,
	ScrollView,
	TextInput,
	Button,
	StyleSheet,
	Dimensions,
	Image,
	TouchableOpacity,
	Picker,
	Keyboard,
	Platform,
} from 'react-native';
import Home from './FloatingButton';
import Otp from './Otp';
import PreLoader from './PreLoader';
import DeviceInfo from 'react-native-device-info';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/SignUp';
import countriesCode from './CountriesCode';
import RNPickerSelect from 'react-native-picker-select';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';
import CheckBox from 'react-native-check-box';
import {AppEventsLogger} from "react-native-fbsdk";
//import database from "@react-native-firebase/database";
import {db} from '../configuration/Config';
class SignUp extends Component {
	static navigationOptions = ({ navigation }) => {
		let { state } = navigation;
		if (state.params !== undefined) {
			return {
				title: state.params.isRTL === true ? 'تسجيل' : 'Sign Up'
			};
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			secureTextEntry: true,
			username: '',
			mobile: '',
			password: '',
			code: '+966',
			list: [{ name: '10' }, { name: '20' }, { name: '30' }],
			error: false,
			checkNumber: false,
			isLoading: false,
			checked: false,
		};
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount() {
		const { setParams } = this.props.navigation;
		setParams({ isRTL: this.props.isRTL });
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
			this.props.navigation.setParams({ isRTL: nextProps.isRTL });
			this.props.navigation.state.params.isRTL = nextProps.isRTL;
		}
	}

	showPass() {
		if (this.state.secureTextEntry) {
			this.setState({ secureTextEntry: false });
		} else {
			this.setState({ secureTextEntry: true });
		}
	}

	/*SignUp submit */
	async handleSubmit() {	
	try{
       AppEventsLogger.logEvent( "Signup Clicked" );
      }catch (e) {


      }
       db.ref('S/SignUp/SignUp')
      .push({
        device: 'Ios',
        date_time:new Date().toISOString() ,
      })
      	
		this.setState({ isLoading: true });
		Keyboard.dismiss();		
		const isRTL = this.props.isRTL;

		if(this.state.username === ''){
			const message = isRTL ? 'يرجى ادخال اسم المستخدم' : "Please enter the username";
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			this.setState({ isLoading: false });
			return;
		}

		if(this.state.checkNumber || this.state.mobile === '' || this.state.error) {			
			const message = isRTL ? 'يرجى ادخال رقم جوال صحيح' : "Please Enter the valid Mobile number.";
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			this.setState({ isLoading: false });
			return;
		}
		if(this.state.password === ''){
			
			const message = isRTL ? 'يرجى ادخال كلمة المرور' : "Please enter the Password.";
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			this.setState({ isLoading: false });
			return;
		}

		const { navigate } = this.props.navigation;
		if (this.state.mobile !== '' && this.state.password !== '') {
			const mobile = this.state.mobile;
			
			if (mobile.charAt(0) == 0) {
				this.setState({ error: true, isLoading: false })
			} 
			else {

				const deviceId = await DeviceInfo.getDeviceId();
				//send otp request to the user
				fetch(API_URL+'/sendregotp', {
					method: 'POST',
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						countryCode: this.state.code,
						mobile: this.state.mobile,
						deviceId: deviceId
					})
				})
					.then(response => response.json())
					.then(responseData => {
						this.setState({ isLoading: false });
						// console.warn("Signup Response",responseData);
						if (
							responseData.status == 502368 &&
							responseData.eData.eCode == 684605
						) {
							Alert.alert('');
							const message = isRTL ? 'رقم الجوال المدخل مسجل سابقا' : "Mobile Number is already registered";
							Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
						} else if (
							responseData.status == 502368 &&
							responseData.eData.eCode == 564626
						) {
							
							const message = isRTL ? 'حدث خطأ' : 'Something went wrong';
							Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
						} else if (
							responseData.status == 805465 &&
							responseData.eData === null
						) {
							const mobile = this.state.mobile;
							const code = this.state.code;
							const pass = this.state.password;
							const name = this.state.username;
							let signup = {
								mobile, code, pass, name
							}
							navigate('Otp', { signup });
						} else {
							const message = isRTL ? 'حدث خطأ' : 'Something went wrong';
							Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
						}
					})
					.catch(err => {
						const errorText = isRTL ? 'خطأ' : "Error";
						this.setState({ isLoading: false });
						Alert.alert(errorText, err.message);
					});
			}
		}

	}

	Code(text) {
		this.setState({ code: text.replace(/[^0-9]/g, '') });
	}

	onChanged(text) {
		this.setState({
			mobile: text.replace(/[^0-9]/g, '')
		});
	}


	checkNum = async () => {

		const mobile = this.state.mobile;
		const { isRTL } = this.props;
		if(this.state.error === true){
			this.setState({ error: false })
		}
		
		if (mobile.charAt(0) === '0') {
			
			this.setState({ error: true })
		} 

		if (mobile === '') {
			this.setState({ checkNumber: false })
			const message = isRTL ? 'رقم الجوال مطلوب!' : 'Mobile Number is Required!';
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			return;
		}

		await fetch(API_URL+"/checkMobile", {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				mobile: mobile,

			})
		})
			.then(response => response.json())
			.then(responseData => {
				// console.warn(responseData);
				if (responseData.status == 201) {

					//Alert.alert('Mobile Number is already registered');
					this.setState({ checkNumber: true })
				}

				else if (responseData.status == 200) {

					this.setState({ checkNumber: false })
					//alert("Success");
				}

				else {
					const message = isRTL ? 'حدث خطأ' : 'Something went wrong';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}




	render() {
		// console.warn(countriesCode)
		for (let i = 0; i < countriesCode.length; i++) {
			countriesCode[i].value = countriesCode[i].dial_code;
			countriesCode[i].label = countriesCode[i].dial_code + '-' + countriesCode[i].name;
			countriesCode[i].color = '#000000';
		}
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		return (
			<ScrollView style={{ padding: 20, backgroundColor: '#F1F6F7' }} keyboardShouldPersistTaps="always">
				<View style={{ alignItems: 'center' }}>
					<Image
						style={styles.logo}
						source={require('../icons/logo1x.png')}
					/>
				</View>
				<View style={styles._textbox}>
					<TextInput
						style={styles._textboxText}
						underlineColorAndroid="white"
						onChangeText={text => this.setState({ username: text })}
						keyboardType={'default'}
						returnKeyType="next"
						placeholderTextColor="#7A7A7A"
						placeholder={strings('signUp.userName')}
					/>
				</View>

				<View
					style={{
						flexDirection: 'row',
						flex: 1,
						justifyContent: 'space-between'
					}}
				>
					<View style={[styles._textbox, { width: '50%' }]}>
						{Platform.OS === 'android' ?
							<Picker
								selectedValue={this.state.code}
								style={{}}
								onValueChange={(itemValue, itemIndex) =>
									this.setState({ code: itemValue })
								}
							>
								<Picker.Item label="CountryCode" value="" />
								{countriesCode.map((item, key) => (
									<Picker.Item
										key={key}
										label={item.dial_code + '-' + item.name}
										value={item.dial_code}
									/>
								))}
							</Picker> :
							<View style={{ left: 20, top: 15 }}>
								<RNPickerSelect
									placeholder={{
										label: 'Select CountryCode',
										value: null
									}}
									value={
										this.state.code
									}
									onValueChange={(itemValue, index) =>
										this.setState({ code: itemValue })
									}
									style={{
										placeholderColor: 'grey'
									}}
									hideIcon
									items={countriesCode}
								/>
							</View>

						}
					</View>
					<View style={[styles._textbox, { width: '50%' },this.state.checkNumber || this.state.error
										? {
											borderWidth: 2,
											borderColor: 'red'
										}
										: {}]}>
						<TextInput
							style={[styles._textboxText, { flex: 0.8 }]}
							underlineColorAndroid="white"
							maxLength={10}
							value={this.state.mobile}
							onChangeText={text => this.onChanged(text)}
							keyboardType={'numeric'}
							returnKeyType="next"
							placeholderTextColor="#7A7A7A"
							placeholder={strings('signUp.mobile')}
							onEndEditing={this.checkNum}
							editable={true}
						/>
					</View>
				</View>

				{this.state.checkNumber === true ?
					<View style={{ justifyContent: "flex-end", alignItems: "flex-end",flexDirection: this.props.isRTL
								? 'row-reverse'
								: 'row', }}>
						<Text style={{ color: "red" }}>
						{this.props.isRTL ?   "رقم الجوال مسجل بالفعل"  :  "Mobile Number is already registered"}
							</Text>
					</View>

					: null

				}

				{this.state.error && <Text style={{ color: "red" }}>{this.props.isRTL ? "اكتب رقم هاتفك المحمول بدون الصفر في البداية" : " Write your mobile number without zero at begining, example: 50xxxxx77"}</Text>}
				<View
					style={[
						styles._textbox,
						{
							flexDirection: this.props.isRTL
								? 'row-reverse'
								: 'row',
							justifyContent: 'space-between',
							marginRight: 5
						}
					]}
				>
					<View
						style={[
							styles._textbox,
							{ width: '80%', marginTop: 0 }
						]}
					>
						<TextInput
							style={[styles._textboxText]}
							onChangeText={text =>
								this.setState({ password: text })
							}
							underlineColorAndroid="white"
							secureTextEntry={this.state.secureTextEntry}
							returnKeyType="done"
							placeholderTextColor="#545454"
							placeholder={strings('signUp.password')}
							returnKeyType="go"
						/>
					</View>
					<TouchableOpacity onPress={this.showPass.bind(this)}>
						<Image
							style={{
								width: 40,
								height: 30,
								marginVertical: 10,
								marginHorizontal: 5
							}}
							source={require('./../icons/show2x.png')}
						/>
					</TouchableOpacity>
				</View>
				<View
					style={{
							flexDirection: this.props.isRTL? 'row-reverse': 'row'
					}}
				>
				<CheckBox
					style={{ padding: 10 ,}}
					onClick={() =>
						this.setState({
							checked: !this.state.checked
						})
					}
					isChecked={this.state.checked}
					checkBoxColor={'#5DADE2'}
				/>
				<TouchableOpacity style={{ marginLeft: 0}}
					onPress={() => this.props.navigation.navigate('TermAndCondition')}
				>
					<Text
						style={{
							marginTop:10,	
							color: '#0000FF',
							fontSize: 15,
							textDecorationLine: 'underline'

							}}

					>
					{strings('addPin.terms')}
					</Text>
					</TouchableOpacity>
				</View>
				{ this.state.isLoading ? <PreLoader /> : <TouchableOpacity
					style={[styles._button,{backgroundColor:!this.state.checked?'#00ADDE99':'#00ADDE',}]}
					onPress={this.handleSubmit}
					disabled={!this.state.checked}
				>
					<Text style={styles._buttonText}>
						{strings('signUp.sign')}
					</Text>
				</TouchableOpacity>}
			</ScrollView>
		);
	}
}

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);

import React from 'react';
import {
	Text,
	View,
	Alert,
	StyleSheet,
	TouchableOpacity,
	ScrollView,
	AsyncStorageLayoutAnimation,
	UIManager,
	Platform,
	Image,
	LayoutAnimation,
	AsyncStorage,
	ActivityIndicator,
} from 'react-native';
//import options from './options.list';
import DeviceInfo from 'react-native-device-info';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as layoutActions from '../actions/layout';
import * as seachActions from '../actions/Search';
import Modal from "react-native-simple-modal";
import LocationSwitch from 'react-native-location-switch';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';
import {AppEventsLogger} from "react-native-fbsdk";
import {db} from '../configuration/Config';

class TabView extends React.Component {
	constructor(props) {
		super(props);
        if( Platform.OS === 'android' )
        {
          UIManager.setLayoutAnimationEnabledExperimental( true )
        }
		this.state = {
			tabs: true,
			status: false,
			level1: true,
			level2: false,
			selectedValue: '',
			selectedValue1: '',
			selectedValue2: '',
			filterData: '',
			category: [],
			subCategory: [],
			subSubCategory: '',
			activeButton: '',
			activeButtonLevel1: '',
			activeButtonLevel2: '',
			activeButtonLevel3: '',
			modalOpen: false,
			onLayoutHeight: 0, 
			modifiedHeight: 0, 
			expanded: false,
			lat:'',
			lng:'',
			specialLoader: false,
			isAllSelect: false,
			token: '',
			userId: '',
		 fb_main_cat: '',
      fb_sub_cat: '',
		};
		this.handleTabs = this.handleTabs.bind(this);
		this.handleLevel1 = this.handleLevel1.bind(this);
		this.handleLevel2 = this.handleLevel2.bind(this);
	}

	getMainCategories() {
		const { isRTL } = this.props;
		fetch(API_URL+'/get-category', {
		method: 'GET',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		}
		})
		.then(response => response.json())
		.then(async responseData => {
			//console.warn(responseData);
			if (
				responseData.status == 654864 &&
				responseData.eData.eCode == 545864
			) {
				const message = isRTL ? 'فشل في الوصول الى الفئات ' : 'Failed to get category';
				Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			} else if (
				responseData.status == 516324 &&
				responseData.eData === null
			) {
				// console.warn(responseData)
				this.setState({ category: responseData.categoryList});
				//setTimeout(() => {console.warn(this.state.category);}, 300);
			}
		})
		.catch(err => {
			const errorText = isRTL ? 'خطأ' : "Error";
			Alert.alert(errorText, err.message);
		});
	}

	componentDidMount() {	
		AsyncStorage.getItem('user_data').then(json => {
      if (json) {
        const userData = JSON.parse(json);
        this.setState({ token: userData.sData,userId: userData.userId });
      }
    });
			// console.warn('list-->>',this.props.mapView)
			this.getMainCategories();
			AsyncStorage.getItem('region').then( json => {
		        const region = JSON.parse(json);
		        const lat  = region.coords.latitude;
		        const lng = region.coords.longitude;
		       	this.setState({lat, lng});
		    })
    
	}

	async handleTabs(event, name) {
		try {
      AppEventsLogger.logEvent("Category Clicked");
    }catch (e) {

    }
    this.setState({fb_main_cat: name});
       db
      .ref('CATS/CAT/' + name + '/ios')
      .transaction(total => {
        return (total || 0) + 1;
      });
      db
      .ref('CATS/CAT/' + name + '/total')
      .transaction(total => {
        return (total || 0) + 1;
      });
      db
      .ref(
        'CATS/CAT/' + name + '/date/' + new Date().toDateString() + '/ios',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
      db
      .ref('CATS/CAT/' + name + '/date/' + new Date().toDateString() + '/total')
      .transaction(total => {
        return (total || 0) + 1;
      });

		const { isRTL } = this.props;
		this.props.setSelectedcategory({cat:'catList', id:event});
		const url = API_URL+'/get-sub-category?cid=' +
			event;
		const deviceId = this.props.mapView ? 'map' : 'listing';
		fetch(url, {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.then(responseData => {
				console.log('-subcat->>',responseData)
				if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					if (this.state.category.id == event) {
						this.setState({
							status: false,
							isAllSelect:false,
							level2: true,
							level1: false,
							activeButton: ''
						});
					} else {		
						let subCat = [...responseData.subCategoryList];
						// console.warn(subCat);
						subCat = subCat.filter(cat => cat.name !== "Special Offers");
						subCat.unshift({id:'spl',name:"Special Offers",arb_name:'عروض خاصة'})
						this.setState({
							level1: true,
							subCategory: subCat,
							selectedValue: name,
							status: true,
							isAllSelect:false,
							activeButton: event,
							subSubCategory: [],
							modifiedHeight: 45
						});
					}
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
			// event = event+'_'+name;
		this.props.searchWithLocationAPI(
			this.state.userId,
			this.state.lat,
			this.state.lng,
			deviceId,
			'',
			event
		);
	}

	async handleLevel1(event, name) {
		try {
      AppEventsLogger.logEvent("Category Clicked");
    }catch (e) {

    }
        this.setState({fb_sub_cat: name});


      db
      .ref(
        'CATS/CAT/' +
          this.state.fb_main_cat +
          '/SUBCATS/SUBCAT/' +
          name +
          '/ios',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
      db
      .ref(
        'CATS/CAT/' +
          this.state.fb_main_cat +
          '/SUBCATS/SUBCAT/' +
          name +
          '/total',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
      db
      .ref(
        'CATS/CAT/' +
          this.state.fb_main_cat +
          '/SUBCATS/SUBCAT/' +
          name +
          '/date/' +
          new Date().toDateString() +
          '/ios',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
      db
      .ref(
        'CATS/CAT/' +
          this.state.fb_main_cat +
          '/SUBCATS/SUBCAT/' +
          name +
          '/date/' +
          new Date().toDateString() +
          '/total',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
		const { isRTL } = this.props;
		this.props.setSelectedcategory({cat:'subCatList', id:event});
		const url = API_URL+'/get-sub-sub-category?scid=' +
			event;
		const deviceId = this.props.mapView ? 'map' : 'listing';
		fetch(url, {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.then(async responseData => {
				if(
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					let { subCategory, status, level1, level2 } = this.state;
					if (subCategory.id === event) {
						this.setState({
							status: true,
							level1: false,
							level2: true,
							isAllSelect:false,
							// subCategory: {},
							activeButtonLevel1: ''
						});
					} else {
						// console.warn('subSubCategory-->>',responseData.subSubcategoryList)
						this.setState({
							subSubCategory: responseData.subSubcategoryList,
							selectedValue1: name,
							level2: true,
							status: true,
							isAllSelect:false,
							activeButtonLevel1: event,
							modifiedHeight: 45
						});
					}
					//setTimeout(() => {console.warn(this.state.subSubCategory);}, 300);
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
			//event = event+'_'+name;
		this.props.searchWithLocationAPI(
			this.state.userId,
			this.state.lat,
			this.state.lng,
			deviceId,
			'',
			null,
			event
		);
	}

	async handleLevel2(event) {
		try {
      AppEventsLogger.logEvent("Category Clicked");
    }catch (e) {

    }
      db.ref(
        'CATS/CAT/' +
          this.state.fb_main_cat +
          '/SUBCATS/SUBCAT/' +
          this.state.fb_sub_cat +
          '/SUBSUBCATS/SUBSUBCAT/' +
          event.name +
          '/ios',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
      db.ref(
        'CATS/CAT/' +
          this.state.fb_main_cat +
          '/SUBCATS/SUBCAT/' +
          this.state.fb_sub_cat +
          '/SUBSUBCATS/SUBSUBCAT/' +
          event.name +
          '/total',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
      db.ref(
        'CATS/CAT/' +
          this.state.fb_main_cat +
          '/SUBCATS/SUBCAT/' +
          this.state.fb_sub_cat +
          '/SUBSUBCATS/SUBSUBCAT/' +
          event.name +
          '/date/' +
          new Date().toDateString() +
          '/ios',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
      db.ref(
        'CATS/CAT/' +
          this.state.fb_main_cat +
          '/SUBCATS/SUBCAT/' +
          this.state.fb_sub_cat +
          '/SUBSUBCATS/SUBSUBCAT/' +
          event.name +
          '/date/' +
          new Date().toDateString() +
          '/total',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
		const { isRTL } = this.props;
		this.props.setSelectedcategory({cat:'subsubList', id:event.id});
		const deviceId = this.props.mapView ? 'map' : 'listing';
		this.setState({
			level1: true,
			level2: false,
			status: false,
			isAllSelect:false,
			selectedValue2: event.name,
			activeButtonLevel2: event.id,
			modifiedHeight: 0
		});
		this.props.searchWithLocationAPI(
			this.state.userId,
			this.state.lat,
			this.state.lng,
			deviceId,
			'',
			null,
			null,
			event.id
		);
		this.changeLayout();
	}

	Capitalize(str) {
		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	scrollListToStart = (event, contentHeight) => {
		// console.log('.sa......',event.nativeEvent.contentOffset)
		// if(this.props.isRTL)
		// this.scrollView.scrollTo({x: event.nativeEvent.contentOffset.x});
	}

	showSpecialOffers(lat, lng) {
		
		const catId = this.state.activeButton;
		this.props.openModal(true);
		this.getSpecialOffers(catId, lat, lng);
	}

	getSpecialOffers(catId, lat, lng) {
		console.warn("catId", catId)
		const { isRTL } = this.props;
		fetch(API_URL+'/specialOfferList', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				catid: catId,
				lat: lat,
				lng: lng
			})
		})
			.then(response => response.json())
			.then(responseData => {
				console.warn("amanss",responseData);
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 564626
				) {
					this.props.offerData('No Data');
					// console.warn('Data not found');
				} else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					// console.warn('offers:-',responseData.offerlist.length)
					this.props.offerData(responseData.offerlist)
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	scrollToEnd = () => {
		if(this.props.isRTL) {
			this.scrollView.scrollToEnd({animated: false});
			this.scrollViewSubCat ? this.scrollViewSubCat.scrollToEnd({animated: false}) : null;
			this.scrollViewSubSubCat ? this.scrollViewSubSubCat.scrollToEnd({animated: false}) : null;
		} else {
			this.scrollView.scrollTo({x:0});
			this.scrollViewSubCat ? this.scrollViewSubCat.scrollTo({x:0}) : null;
			this.scrollViewSubSubCat ? this.scrollViewSubSubCat.scrollTo({x:0}) : null;
		}
	}

	scrollToEndSubCat = () => {
		if(this.props.isRTL) {
			this.scrollViewSubCat.scrollToEnd({animated: false});
		} else {
			// this.scrollViewSubCat.scrollTo({x:0});
		}
	}

	scrollToEndSubSubCat = () => {
		if(this.props.isRTL) {
			this.scrollViewSubSubCat.scrollToEnd({animated: false});
		} else {
			// this.scrollViewSubSubCat.scrollTo({x:0});
		}
	}

	enableUserLocation() {
		const { isRTL } = this.props;
	    LocationSwitch.enableLocationService(1000, true,
	          () => { 
	            this.getSpecialOffersHandler();
	        },
	          () => { 
	          	const message = isRTL ? 'تعذر تمكين الموقع' : 'Location could not enable.'
	          	Alert.alert('',message)
	        },
	      );
	  }

	isLocationEnableHandler = () => {
		const { isRTL } = this.props;
	    LocationSwitch.isLocationEnabled(
	        () => {
	          this.getSpecialOffersHandler();       
	        },
	        () => { 
	        	const title = isRTL ? 'استخدام موقعك؟' : 'Use Location ?';
	        	const message = isRTL ? 'لاظهار العروض الخاصة حول موقعك ، يرجى تفعيل خاصية اظهار الموقعGPS' : 'To show special offers around you, turn on GPS.'
	        	const notNowText = isRTL ? 'ليس الان' : 'Not Now';
	        	const enableText = isRTL ? 'مكن' : 'Enable';
	          	Alert.alert(
	                title,
	                message,
	                [
	                  {
	                    text: notNowText,
	                    
	                  },
	                  {
	                    text: enableText,
	                    onPress: () => {
	                      this.enableUserLocation();
	                    }
	                  }
	                ],
	                { cancelable: false }
	            );
	        },
	      );
	};

	getSpecialOffersHandler() {
		this.setState({specialLoader: true});
		navigator.geolocation.getCurrentPosition(
		      (position) => {
		        // this.setState({
		        //   latitude: position.coords.latitude,
		        //   longitude: position.coords.longitude,
		        //   error: null,
				// });
				this.setState({specialLoader: false});
		        this.showSpecialOffers(position.coords.latitude, position.coords.longitude);
		      },
		      (error) =>  {this.setState({specialLoader: false});
				  Alert.alert(' Error',error.message)},
		      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
		    );		
	}
	checkSpecialOffer(item) {
						
		if(item.name === "Special Offers") {				
		    this.isLocationEnableHandler()	
				   					
		} else {
			this.handleLevel1(item.id,item.name)
		}
													
	}

	// componentWillUnmount() {
	// 	if(Platform.OS === 'android')
	// 	LocationServicesDialogBox.stopListener();
	// }
	// handleDown() {
	// 	this.setState({level1: !this.state.level1});
	// 	if(this.state.level2){
	// 		this.setState({status: !this.state.status})
	// 	}
	// }

	changeLayout = () => {
        LayoutAnimation.configureNext( LayoutAnimation.Presets.easeInEaseOut );

        if( this.state.expanded === false )
            this.setState({ modifiedHeight: this.state.onLayoutHeight, expanded: true });
        else
            this.setState({ modifiedHeight: 0, expanded: false });
    }

    getViewHeight( height )
    {
        this.setState({ onLayoutHeight: height });
    }

	handleAll = () => {
		const deviceId = this.props.mapView ? 'map' : 'listing';
		this.setState({
		isAllSelect: true,
		activeButton: '',
		activeButtonLevel1: '',
		activeButtonLevel2: '',
		activeButtonLevel3: '',
		status: false,
		level1: true,
		level2: false,
		modifiedHeight: 0,
		selectedValue: '',
		selectedValue1: '',
		selectedValue2: '',
		});
		this.props.setSelectedcategory({cat:'', id:''});
		this.props.searchWithLocationAPI(
			this.state.userId,
			this.state.lat,
			this.state.lng,
			deviceId,
			'',
		);




	}

	render() {
		// console.warn('modifiedHeight', this.state.modifiedHeight)
		const isRTL = this.props.isRTL;
		const Category = this.state.category;
		const Sub_category = this.state.subCategory;
		const sub_Sub_category = this.state.subSubCategory;
		return (
			<View style={[styles.container, Platform.OS === 'ios' ? {marginTop:10} : null]}>
				{ this.state.level1 ? 
					<ScrollView
						ref={ref => this.scrollView = ref}
						onMomentumScrollEnd={this.scrollListToStart}
						showsHorizontalScrollIndicator={false}
						horizontal={true}
						onContentSizeChange={this.scrollToEnd}
						contentContainerStyle={{ 
							marginTop:10,
							padding:5,
							flexDirection: isRTL ? 'row-reverse' : 'row',

						}}
					>
					<View style={[styles.button, this.state.isAllSelect ? { backgroundColor: '#000000'} : { backgroundColor: '#FFFFFF'}]}>
							
						<TouchableOpacity onPress={() => this.handleAll()}>

							<Text style={[ styles.button_text, this.state.isAllSelect ? { color: '#FFFFFF'} : { color: '#000000'}]}>
								
								{isRTL ? "الكل" : "All"}
							</Text>
						</TouchableOpacity>
					</View>
					{Category.map((item, index) => {
						return (
							<View
								key={index}
								style={[
									styles.button,
									this.state.activeButton === item.id
										? { backgroundColor: '#000000' }
										: {}
								]}
							>
								<TouchableOpacity
									onPress={() =>
										this.handleTabs(item.id, item.name)
									}
								>
									<Text
										style={[
											styles.button_text,
											this.state.activeButton === item.id
												? { color: '#FFFFFF' }
												: {}
										]}
									>
										{isRTL ? item.arb_name : this.Capitalize(item.name)}
									</Text>
								</TouchableOpacity>
							</View>
						);
					})}
					
				</ScrollView> : null
			}
			<View style={{height: this.state.modifiedHeight, overflow: 'hidden'}}>
			{
				this.state.subCategory &&
				this.state.status && (
					<ScrollView
						showsHorizontalScrollIndicator={false}
						style={styles.sub_btn}
						horizontal={true}
						ref={ref => this.scrollViewSubCat = ref}
						onContentSizeChange={this.scrollToEndSubCat}
						contentContainerStyle={{
							flexDirection: isRTL ? 'row-reverse' : 'row',
							justifyContent:'center',
							alignItems: 'center'

						}}
					>
						{Sub_category.map((item, index) => {
							return (
								<TouchableOpacity 
									onPress={() => {this.checkSpecialOffer(item)}}
									key={item.id}
									style={[
										styles.subButton,
										this.state.activeButtonLevel1 ===
										item.id
											? { backgroundColor: '#000000' }
											: {},
											item.name === "Special Offers" ? 
													{backgroundColor: "red" } : {}
									]}
								>
									<View style={{flexDirection:"row"}}>											
										<Text
											style={[
												styles.button_text,
												this.state
													.activeButtonLevel1 ===
												item.id
													? { color: '#FFFFFF' }
													: {},
													item.name === "Special Offers" ? 
												{color: "#FFFFFF" } : {}

											]}
										>												
											{isRTL ? item.arb_name : this.Capitalize(item.name)}
										</Text>
											
										
										{item.name === "Special Offers" ? this.state.specialLoader ? <ActivityIndicator size="small" color="#ffffff" /> : null : null }
									</View>
								</TouchableOpacity>
							);
						})}
					</ScrollView>
				)
			}
			</View>		
			<View style={{height:this.state.modifiedHeight, overflow: 'hidden'}}>
			{this.state.level2 && (
				<ScrollView
					showsHorizontalScrollIndicator={false}
					style={styles.sub_btn}
					horizontal={true}
					ref={ref => this.scrollViewSubSubCat = ref}
					onContentSizeChange={this.scrollToEndSubSubCat}
					contentContainerStyle={{
						flexDirection: isRTL ? 'row-reverse' : 'row',alignItems: 'center'
					}}
				>
					{sub_Sub_category.map((item, index) => {
						return (
							<View key={index} style={[styles.subButton,this.state.activeButtonLevel2 ===
										item.id
											? { backgroundColor: '#000000' }
											: {}
							]}>
								<TouchableOpacity
									onPress={() => this.handleLevel2(item)}
								>
									<Text style={[styles.button_text, this.state.activeButtonLevel2 === item.id
													? { color: '#FFFFFF' }
													: {}]}>
										
										{isRTL ? item.arb_name : this.Capitalize(item.name)}
									</Text>
								</TouchableOpacity>
							</View>
						);
					})}
				</ScrollView>
			)}
			</View>
				
		</View>
		);
	}
}

const styles = StyleSheet.create({
	constainer: {
		flex: 1,
		// borderWidth: 3,
		justifyContent: 'space-between'
	},
	button: {
		borderRadius: 25,
		backgroundColor: '#FFFFFF',
		padding: 3,
		marginHorizontal: 5,
		flexDirection: 'row'
	},
	subButton: {
		borderRadius: 25,
		// padding: 5,
		marginHorizontal: 5,
		backgroundColor: '#FFFFFF',
	},
	sub_btn: {
		paddingVertical: 5,
	},
	button_text: {
		color: '#000000',
		marginTop: 4,
		marginHorizontal: 20,
		marginBottom: 6,
		fontFamily: 'System',
		fontSize: 16,
		textAlign: 'center'
	}
});

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL,
		location: state.search.location,
		status: state.search.status
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ ...layoutActions, ...seachActions }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TabView);

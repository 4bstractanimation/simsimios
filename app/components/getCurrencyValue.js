import React from 'react';

export default async function setCurrencyValue(currencyCode) {	
		
		const convertTo = 'SAR_'+currencyCode;
		try {
		    let response = await fetch(
		      `https://free.currencyconverterapi.com/api/v6/convert?q=${convertTo}&compact=ultra&apiKey=7c844ddc9a6fbc0a4d7c`
		    );
		    let responseJson = await response.json();
		    
		    const currentCurrencyValue = responseJson[convertTo];
		   
		    return currentCurrencyValue;
		} catch (error) {
		     alert(error.message);
		}
}



import API_URL from '../configuration/configuration';

export default async function getUserBalance(token) {		
		
		try {
		    let response = await fetch(API_URL+'/wallet-current-balance', {
					method: 'POST',
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						Authorization: token
					}
				});
		    let responseJson = await response.json();
		   	if(responseJson.status == 200 ) {
		   		// console.warn(responseJson)
		   		return responseJson.data;
		   	}
		} catch (error) {
		     alert(error.message);
		}
}

import React, { Component } from 'react';
import OneSignal from 'react-native-onesignal'; // Import package from node modules

export default class PushNotification extends Component {

constructor(props) {
    super(props);
    // OneSignal.init("11a642c1-9671-4e10-ac73-ca5fba2c0cdf");

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    // OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    // OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log('recieve notification')
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.warn('Message: ', openResult.notification.payload.body);
    console.warn('Data: ', openResult.notification.payload.additionalData);
    console.warn('isActive: ', openResult.notification.isAppInFocus);
    console.warn('openResult: ', openResult);
  }

  // onIds(device) {
  //   console.warn('Device info: ', device);
  // }

  render() {
    return null;
  }
}
import React, { Component } from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import {
  AppRegistry,
  View,
  Dimensions,
  StyleSheet,
  Text,
  AsyncStorage,
  TouchableOpacity,
  Alert,
} from 'react-native';
// import Toast from 'react-native-simple-toast';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as layoutActions from '../actions/layout';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0122;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

class GetUserlocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
    };
  }
  
  static navigationOptions = ({navigation}) => ({      
      title: 'Google Map',     
  });

  componentDidMount() {
    // console.warn(this.props.isRTL)
    const isRTL = this.props.isRTL;
    const message = isRTL ? 'اضغط بشكل مستمر على موقع نقطة البيع لبعض ثواني لسحبها الى موقعك او انقر على الخريطة لتغيير الموقع ثم اضغط على الرز ارسل' : 'Press the pin for few seconds to drag to your location or click over the map to change the location and then click on submit button.'
    Alert.alert('',message)
    navigator.geolocation.getCurrentPosition(
      position => {
        const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
            accuracy: position.coords.accuracy
        }
        // this.selectedLocation = region;
        this.setState({region});
      },
      error => alert(error.message),
      { timeout: 10000 }
    );

  }

   handleSubmitButtonHandler = async () => {    
      await AsyncStorage.setItem('coordinates',JSON.stringify(this.state.region));
      this.props.navigation.goBack(null);
      return true;
  }

  updateLocationHandler = (event) => {
    const markerLocation = {
      ...event.nativeEvent.coordinate,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA
    }
    this.setState({region: markerLocation})

  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={this.state.region}
          showsUserLocation={true}
          followUserLocation={false}
          onPress={this.updateLocationHandler}
          provider={PROVIDER_GOOGLE}
        >
          <MapView.Marker
            draggable
            onDragEnd={this.updateLocationHandler}
            coordinate={this.state.region}
            
          />
          </MapView>
        
        <TouchableOpacity onPress={this.handleSubmitButtonHandler} style={{bottom:20, borderRadius:15,backgroundColor:'#33a5ea', paddingHorizontal:40, paddingVertical:10,justifyContent:'center', alignItems:'center'}}>
          <Text style={{color:'#FFF'}}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
});

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL
  };
}

export default connect(mapStateToProps)(GetUserlocation);
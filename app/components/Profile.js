import React, { Component } from 'react';
import {
	Text,
	TextInput,
	View,
	ScrollView,
	TouchableOpacity,
	StyleSheet,
	Switch,
	Image,
	DatePickerIOS,
	Alert,
	AsyncStorage,
	Picker,
	Platform,
	ActivityIndicator,
	Keyboard,
	Dimensions,
} from 'react-native';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import SignUp from './SignUp';
import countriesCode from './CountriesCode';
import CheckBox from 'react-native-check-box';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Profile';
import PreLoader from './PreLoader';
import Modal from 'react-native-modalbox';
import RNPickerSelect from 'react-native-picker-select';
import Payment from './Payment';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

// const { width, height } = Dimensions.get('window');

class Profile extends Component {
	static navigationOptions = ({ navigation }) => {
		let { state } = navigation;
		if (state.params !== undefined) {
			return {
				title: state.params.isRTL === true ? 'الملف الشخصي' : 'Profile'
			};
		}
	};

	constructor(props) {
		super(props);

		this.state = {
			isLoading: true,
			fullName: '',
			mobileNo: '',
			email: '',
			password: '',
			gender: '',
			dob: '',
			countryCode: '',
			showmobile: false,
			token: '',
			isDatePickerVisible: false,
			index: '',
			highLightField: '',
			isChecked: false,
			sellerType: '',
			sellerIndex: '',
			enterprise_document: '',
			filename: '',
			isLoading: true,
			isNotValidEmail: false,
			url: '',
			oldMobileNo: '',
			oldCountryCode: '',
			userData: ',',
			checkNumber: false,
			error : false,
			isUpdate: false,
			showIosDate:false,
			isIosPicker : false,
			dobIOS : new Date(),
			isDisabled: false,
		};
		this.verifyMobile = this.verifyMobile.bind(this);
	}

	_showDateTimePicker = () =>
		this.setState({ isDatePickerVisible: true, highLightField: 'dob' });

	_hideDateTimePicker = () =>
		this.setState({ isDatePickerVisible: false, highLightField: '' });

	onFocus(status) {
		this.setState({ highLightField: status });
	}

	_handleDobPicked = date => {
		const chosenDate = moment(date).format('YYYY-MM-DD');
		this.setState({ dob: chosenDate, highLightField: '' });
		this._hideDateTimePicker();
	};


//***********************change********************************** */
	_handleUpdate = async () => {
		const isRTL = this.props.isRTL;
		Keyboard.dismiss()
		
		if (this.state.isNotValidEmail) {
			const message = isRTL ? 'ادخل بريد الكتروني صحيح' : 'Enter correct email';
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			return;
		}

		if(this.state.checkNumber || this.state.mobileNo === '' || this.state.error){
			const message = isRTL ? 'يرجى ادخال رقم جوال صحيح ' : 'Please Enter the valid Mobile number.';
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			return;
		}

		const { navigate } = this.props.navigation;
		const { countryCode, oldCountryCode, oldMobileNo, mobileNo, token } = this.state;

		const data = {
			email: this.state.email,
			fullName: this.state.fullName,
			mobile: this.state.mobileNo,
			gender: this.state.gender,
			dob: Platform.OS === 'ios' ? this.state.dobIOS : this.state.dob,
			country_code: this.state.countryCode
		}

		const newUserdata = { ...this.state.userData };
		newUserdata.userData.dob = data.dob;
		AsyncStorage.setItem('user_data', JSON.stringify(newUserdata));

		if (oldCountryCode !== countryCode || mobileNo !== oldMobileNo ) {
			//console.warn('true')
			if(!this.state.mobileVerified) {
				const message = isRTL ? 'ادخل بريد الكتروني صحيح' : 'Plesae wait mobile number is verifying...';
				Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				return;
			}

			this.verifyMobile(data)
			return;
		}
		this.setState({isUpdate:true});
		fetch(API_URL+'/profile-edit', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: token
			},
			body: JSON.stringify(data)
		})
			.then(response => response.json())
			.then(responseData => {
					
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 236486
				) {
					const errors = responseData.eData.errors;
					
					Alert.alert(errors[0]);
					this.setState({isUpdate:false});
				} else if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 545864
				) {
					this.setState({isUpdate:false});
					const message = isRTL ? ' فشل في تحديث الملف الشخصي' : 'Failed to update';
					Toast.showWithGravity(message, Toast.LONG, Toast.CENTER);
				} else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					
					const { gender, dob, name, mobile, email } = responseData.userData;
					
					if (gender && dob && name && mobile) {
						AsyncStorage.setItem('isProfileCompleted', JSON.stringify({
							isCompleted: true,
							counter: 0,
							userProfile: { gender, dob, name, mobile, email }
						}))
					}
					
					const message = isRTL ? 'تم تحديث الملف الشخصي بنجاح' : 'Profile updated successfully'
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
					this.setState({isUpdate:false});
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	deleteProfileHandler = () => {
		Keyboard.dismiss();
		
		const { cancel_status, subscription_plan } = this.state.sellerSubscriptionPlan;
		// console.warn(subscription_plan)
		if(cancel_status == 0 && subscription_plan && subscription_plan.toLowerCase() !== "free" ) {
			const isRTL = this.props.isRTL;
      const message = isRTL
        ? 'ليتم مسح حسابك يرجى اولا الغاء اشتراكك في باقة التميز او الذهبيه'
        : `In order to delete your SimSim account you need to cancel your current running subscription plan first.`;
      Alert.alert('', message);
      return;

		}

		const isRTL = this.props.isRTL; 
		const message = isRTL ? 'هل تريد مسح ملفك الشخصي؟' : 'Do you want to delete your account?';
		const yesButtonText = isRTL ? 'نعم' : 'Yes';
  		const noButtonText = isRTL ? 'لا' : 'No';
		Alert.alert(
			'', message,
			[
				{ text: yesButtonText, onPress: this._handleDelete },
				{ text: noButtonText, style: 'cancel' },
			],
			{ cancelable: false }
		)
	}

	_handleDelete = () => {
		const { navigate } = this.props.navigation;
		const isRTL = this.props.isRTL;
		AsyncStorage.removeItem('token', err => console.log('finished', err));
		const token = this.state.token;
		fetch(API_URL+'/delete-account', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: token
			},
			body: JSON.stringify({
				confirmation: 'yes'
			})
		})
			.then(response => response.json())
			.then(responseData => {
				console.log('delete account',responseData);
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 236486
				) {
					const message = isRTL ? 'حدث خطأ' : 'Something went wrong';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 545864
				) {
					const message = isRTL ? 'حدث خطأ' : 'Something went wrong';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if (
					responseData.status == 516324 &&
					responseData.eData == null
				) {
					this.props.resetLoginData()
					
					//AsyncStorage.removeItem('user_data')
					let keys = ['user_data', 'userCredential', 'isProfileCompleted'];
					AsyncStorage.multiRemove(keys);
					navigate('Home');
					const message = isRTL ? 'تم مسح الملف الشخصي' : 'Profile delete successfully';
					Alert.alert('', message);
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	async componentWillMount() {

		const { setParams } = this.props.navigation;
		setParams({ isRTL: this.props.isRTL });
		const getUserData = await AsyncStorage.getItem('user_data');
		const userData = JSON.parse(getUserData);
		const token = userData.sData;
		this.setState({ token: token, userData: userData });
		this.getUserDetails(token)
		this.getSellerSubscriptionPlan(token);
		this.props.navigation.addListener('didFocus', async event => {
			this.getUserDetails(token);
		});

		// this.props.navigation.addListener('didFocus', async event => {
		// 	console.warn('goBack called....');
		// });
		// const token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd3d3LnNpbXRlc3QubjEuaXdvcmtsYWIuY29tL3B1YmxpYy9hcGkvbG9naW4iLCJpYXQiOjE1NDAyMDkwMDksImV4cCI6MTU0MDgwMzAwOSwibmJmIjoxNTQwMjA5MDA5LCJqdGkiOiJoTGM4TkVmTTFVTlBjT3VjIiwic3ViIjo0LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7Tk4HKMCUK0FJcNfITz6OMTAW-kUtJMW4Yo3ricMQSs"
		//get the details of user

	}

	getSellerSubscriptionPlan(token) {

	    const url = API_URL+'/get-seller-subscription-plan';

	    fetch(url, {
	      method: 'POST',
	      headers: {
	        Accept: 'application/json',
	        'Content-Type': 'application/json',
	        Authorization: token
	      }
	    })
	      .then(response => response.json())
	      .then(async responseData => {
	        //console.warn('get-seller-subscription-plan-->>',responseData);
	        if( responseData && responseData.status == 200 ) {
	          this.setState({sellerSubscriptionPlan:responseData.data});
	        }
	        
	      })
	      .catch(err => {        
	       Alert.alert('Error', err.message)
	      });
	}

	getUserDetails(token) {
		fetch(API_URL+'/get-user-detail', {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: token
			}
		})
		.then(response => response.json())
		.then(responseData => {
			//console.warn(responseData.userData.dob)
			if (
				responseData.userData.name !== null ||
				responseData.userData.email !== null ||
				responseData.userData.mobile !== null ||
				responseData.userData.gender !== null ||
				responseData.userData.dob !== null
			) {
				this.setState({
					isLoading: false,
					fullName: responseData.userData.name,
					email: responseData.userData.email,
					mobileNo: responseData.userData.mobile,
					oldMobileNo: responseData.userData.mobile,
					gender: responseData.userData.gender,
					dob: responseData.userData.dob,
					dobIOS:responseData.userData.dob,
					countryCode: responseData.userData.country_code,
					oldCountryCode: responseData.userData.country_code,
					checkNumber: false
				});
			}
		})
		.catch(err => {
			const errorText = isRTL ? 'خطأ' : "Error";
			Alert.alert(errorText, err.message);
		});

	}

	getGender() {
		let index = 0;
		if (this.state.gender === 'male') {
			index = 0;
		} else if (this.state.gender === 'female') {
			index = 1;
		} else {
			index = 2;
		}
		return index;
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
			this.props.navigation.setParams({ isRTL: nextProps.isRTL });
			this.props.navigation.state.params.isRTL = nextProps.isRTL;
		}
	}

	onSelect(index, value) {
		this.setState({
			sellerType: value,
			sellerIndex: index
		});
	}

	verifyMobile = async (data) => {
		const mobile = this.state.mobileNo;
		const isRTL = this.props.isRTL;

		fetch(API_URL+'/sendpinsms', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: this.state.token
			},
			body: JSON.stringify({
				countryCode: data.country_code,
				mobile: mobile

			})
		})
			.then(response => response.json())
			.then(responseData => {
				console.warn('responseData-->>',responseData)
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 236486
				) {
					alert('Failed to add pin');
				} else if (
					responseData.status == 805465 &&
					responseData.sData !== null
				) {
					// console.warn('sData-->>',responseData.sData);
					Alert.alert('', responseData.sData)
				} else if (
					responseData.status == 805465 &&
					responseData.sData === null
				) {
					const code = this.state.countryCode;
					const title = isRTL ? 'شكرا لك' : 'Thank You';
					const message = isRTL ? 'تم ارسال رسالة توثيق الى جوالك' : 'OTP has been sent';
					const okButtonText = isRTL ? 'حسنا' : 'OK';
					Alert.alert(title, message, [
						{
							text: okButtonText,
							onPress: () => {
								this.props.navigation.navigate('Otp', { mobile, code, type: 'profile', data });
							}
						}
					]);
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	uploadImage() {
		const isRTL = this.props.isRTL;
		ImagePicker.openPicker({
			includeBase64: true
		})
			.then(item => {
				const str = item.path;
				var filename = str.replace(/^.*[\\\/]/, '');
				this.setState({
					enterprise_document: item.data,
					fileName: filename
				});
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	changeCountryCodeHandler = (itemValue, itemIndex) => {
		//const newCountryCode = itemValue;
		this.setState({ countryCode: itemValue })
		//this.verifyMobile(null, newCountryCode)	
	}

	checkEmail = () => {
		if(this.state.email !== '') {
			let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if (reg.test(this.state.email) === false) {

				this.setState({ isNotValidEmail: true })
				//return false;
			} else {
				this.setState({ isNotValidEmail: false })
			}
		}
	}

	checkNum = async () => {
		const mobile = this.state.mobileNo.trim();
		const oldMobile = this.state.oldMobileNo;
		const isRTL = this.props.isRTL;
		
		if(mobile === ''){
			const message = isRTL ? 'رقم الجوال مطلوب!' : 'Mobile Number is Required!'
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			return;
		}

		if(mobile.charAt(0) == 0){
			this.setState({error:true,checkNumber:false})
			return;
		}

		if(mobile === oldMobile){
			this.setState({checkNumber:false,error:false});
		}

		if(mobile !== oldMobile){
			
		await fetch(API_URL+"/checkMobile", {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({mobile: mobile})
		})
			.then(response => response.json())
			.then(responseData => {
				//console.warn(responseData);
				if (responseData.status == 201) {

					//Alert.alert('Mobile Number is already registered');
					this.setState({ checkNumber: true,error:false, varifyingMobile: false })
				}

				else if (responseData.status == 200) {

					this.setState({ checkNumber: false, mobileVerified: true })
					//alert("Success");
				}

				else {
					const message = isRTL ? 'حدث خطأ' : 'Something went wrong';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
		}
 	}

	 setDateIOS = (newDate) => {
		this.setState({dobIOS: newDate});
		}
		
	
	showIosDatePicker = () => {
		//console.warn('TCL: showIosDatePicker -> showIosDatePicker')
		
		this.refs.modal1.open()
		//this.setState({isIosPicker : true, showIosDate : true});
	}
	
	hideIosDatePicekr = () => {
		this.refs.modal1.close()
		//this.setState({isIosPicker : false});
	}

	render() {
		//console.warn("sellerType --> ",this.props.sellerType)
		for (let i = 0; i < countriesCode.length; i++) {
			countriesCode[i].value = countriesCode[i].dial_code;
			countriesCode[i].label = countriesCode[i].dial_code + '-' + countriesCode[i].name;
			countriesCode[i].color = '#000000';
		}
		const { navigate } = this.props.navigation;
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		const arContactError = "يرجى كتابة رقم الجوال من دون صفر في البداية ، مثلا: 50xxxxxxx77";
		const enContactError = "Write your mobile number without zero at begining, example: 50xxxxx77";
		const contactErrorMsg = this.props.isRTL ? arContactError : enContactError;
		return (
			<View
				style={{
					paddingHorizontal: 20,
					backgroundColor: '#ecf2f9',
					paddingVertical: 15
				}}
			>
				<ScrollView keyboardShouldPersistTaps="handled">
					<View style={{ flex: 1, alignItems: 'center' }}>
						<Image
							style={styles.logo}
							source={require('../icons/logo1x.png')}
						/>
						
						{this.state.isLoading ? <PreLoader /> : null}
						
					</View>
					<View style={{ marginTop: 10 }}>
						<View
							style={[
								styles._textbox,
								this.state.highLightField === 'fullName'
									? { borderWidth: 2, borderColor: '#71c1f2' }
									: {}
							]}
						>
							<TextInput
								ref="fullName"
								style={[styles._textboxText]}
								onChangeText={text =>
									this.setState({ fullName: text })
								}
								underlineColorAndroid="white"
								onFocus={() => this.onFocus('fullName')}
								onBlur={() =>
									this.setState({ highLightField: '' })
								}
								maxLength={25}
								keyboardType={'default'}
								placeholderTextColor="#545454"
								placeholder={strings('getDetail.fullname')}
								value={this.state.fullName}
								editable={true}
							/>
						</View>

						<View
							style={{
								
								flexDirection: this.props.isRTL
									? 'row-reverse'
									: 'row'
							}}
						>
							<View
								style={[
									styles._textbox,
									{ flex: 0.5,  },
									this.props.isRTL
										? { marginLeft: 5 }
										: { marginRight: 5 }
								]}
							>
								<View style={{  }}>
									{Platform.OS === 'android' ?
										<Picker
											selectedValue={this.state.countryCode}
											// style={{ height: 50, width: 100 }}
											onValueChange={this.changeCountryCodeHandler}
										>
											<Picker.Item label="Country Code" value="" />
											{countriesCode.map((item, key) => (
												<Picker.Item
													key={key}
													label={item.dial_code + '-' + item.name}
													value={item.dial_code}
												/>
											))}
										</Picker>
										:
										<View style={{ left: 10, top: 15 }}>
											<RNPickerSelect
												placeholder={{
													label: 'Select CountryCode',
													value: null
												}}
												value={
													this.state.countryCode
												}
												onValueChange={(itemValue, index) =>
													this.setState({ countryCode: itemValue })
												}
												style={{ 
														inputIOS:{
														//placeholderTextColor: 'black',
														color:'#4d4d4d',
														fontWeight:'bold',														
													}
												}}
												hideIcon
												items={countriesCode}
											/>
										</View>
									}
								</View>
							</View>
							<View
								style={[
									{ flex: 0.5, },
									styles._textbox,
									this.state.highLightField === 'mobile'
										? {
											borderWidth: 2,
											borderColor: '#71c1f2'
										}
										: {},

										this.state.checkNumber === true
										? {
											borderWidth: 2,
											borderColor: 'red'
										}
										: {}
								]}
							>



								{/* {************************Mobile**********************} */}

								<TextInput
									style={styles._textboxText}
									onChangeText={text =>
										this.setState({ mobileNo: text })
									}
									onEndEditing={this.checkNum}
									onFocus={() => this.onFocus('mobile')}
									onBlur={() =>
										this.setState({ highLightField: '' })
									}
									underlineColorAndroid="white"
									keyboardType={'numeric'}
									placeholderTextColor="#545454"
									placeholder={strings('getDetail.mobile')}
									//onEndEditing={this.verifyMobile}
									//returnKeyType="next"
									value={this.state.mobileNo}
									editable={true}
								/>
								
							</View>
							
						</View>
								{this.state.checkNumber == true ? 
						<View style={{ justifyContent:"flex-end", alignItems:"flex-end", flexDirection: this.props.isRTL
								? 'row-reverse'
								: 'row', }}>
							<Text style={{ color: "red"  }}>
							  {this.props.isRTL ?   "رقم الجوال مسجل بالفعل"  :  "Mobile Number is already registered"}
							</Text>
						</View>

						: null

								}

								{this.state.error && <Text style={{color:"red"}}>{contactErrorMsg}</Text>}

						<View
							style={[
								styles._textbox,
								this.state.highLightField === 'email'
									? { borderWidth: 2, borderColor: '#71c1f2' }
									: {}
							]}
						>
							<TextInput
								style={styles._textboxText}
								onChangeText={text =>
									this.setState({ email: text.trim() })
								}
								onFocus={() => this.onFocus('email')}
								onBlur={() =>
									this.setState({ highLightField: '' })
								}
								underlineColorAndroid="white"
								keyboardType={'default'}
								placeholderTextColor="#545454"
								placeholder={strings('getDetail.email')}
								value={this.state.email}
								editable={true}
								onBlur={this.checkEmail}
							/>
						</View>
						{this.state.isNotValidEmail && <Text style={{ color: 'red' }}>{'Invalid Email'}</Text>}
						<TouchableOpacity
							style={[styles._UpdateButton, styles._button]}
							onPress={() => navigate('ChangePassword')}
						>
							<Text
								style={[
									styles._updatebuttonText,
									styles._buttonText
								]}
							>
								{strings('getDetail.changePassword')}
							</Text>
						</TouchableOpacity>
						<View style={styles._radiotextbox}>
							<Text
								style={[
									styles._textboxText,
									{ marginTop: 13, flex: 1 }
								]}
							>
								{strings('getDetail.gender')}{' '}
							</Text>
							<RadioGroup
								onSelect={(index, value) =>
									this.setState({
										gender: value,
										index: index
									})
								}
								color="#545454"
								activeColor="#00ADDE"
								selectedIndex={this.getGender()}
								style={styles._radioButton}
							>
								<RadioButton
									value="male"
									style={{ marginTop: 5 }}
								>
									<Text
										style={{
											color: '#545454',
											paddingBottom: 2
										}}
									>
										{strings('getDetail.male')}
									</Text>
								</RadioButton>
								<RadioButton
									value="female"
									style={{ marginTop: 5 }}
								>
									<Text
										style={{
											color: '#545454',
											paddingBottom: 2
										}}
									>
										{strings('getDetail.female')}
									</Text>
								</RadioButton>
								<RadioButton
									value="others"
									style={{ marginTop: 5 }}
								>
									<Text
										style={{
											color: '#545454',
											paddingBottom: 2
										}}
									>
										{strings('getDetail.others')}
									</Text>
								</RadioButton>
							</RadioGroup>
						</View>

					{
					
						Platform.OS === 'ios' ? 

						<View
									style={[
										styles._textbox,
									
									 {}
									]}
								>
								
							<TouchableOpacity
								onPress={this.showIosDatePicker}
									style={{
														flexDirection: this.props.isRTL
															? 'row-reverse'
															: 'row',
														justifyContent: 'space-between',
														
												}}
							>
							
								<View style={[styles._textbox,
										
									 {marginTop:0,flexDirection:"row",justifyContent: 'space-between',}		
									]}>
								
								{
									this.state.dobIOS ? (
															<Text style={[styles._textboxText,{marginTop:15}]}>
																{ this.state.dobIOS.toString().substring(0,16)}
															</Text>
														) : (
																<Text style={[styles._textboxText,{marginTop:15}]}>
																	{strings('getDetail.dob')}
																</Text>
															)
							
								}
								<Image
										style={{
											marginRight: this.props.isRTL ? 0 : 15,
											marginVertical: 8,
											marginLeft: this.props.isRTL ? 15 : 0,
											resizeMode: 'contain',
											width: 20
										}}
										source={require('./../icons/calender2x.png')}
								/>
									
								</View>
					</TouchableOpacity>
					{ 
								//this.state.isIosPicker &&
								
					}	
				</View>

						:

						<View
							style={[
								styles._textbox,
								this.state.highLightField === 'dob'
									? { borderWidth: 2, borderColor: '#71c1f2' }
									: {}
							]}
						>
							<TouchableOpacity
								onPress={this._showDateTimePicker}
								style={{
									flexDirection: this.props.isRTL
										? 'row-reverse'
										: 'row',
									justifyContent: 'space-between'
								}}
							>
								{this.state.dob ? (
									<Text
										style={[
											styles._textboxText,
											{ marginTop: 15 }
										]}
									>
										{this.state.dob}
									</Text>
								) : (
										<Text
											style={[
												styles._textboxText,
												{ marginTop: 15 }
											]}
										>
											{strings('getDetail.dob')}
										</Text>
									)}
							</TouchableOpacity>
							<DateTimePicker
								isVisible={this.state.isDatePickerVisible}
								onConfirm={this._handleDobPicked.bind(this)}
								onCancel={this._hideDateTimePicker.bind(this)}
								mode="date"
								maximumDate={new Date()}
							/>
						</View>
					}





						{
							// <View style={{flexDirection:'row', marginTop:10}}>
							// 	<CheckBox
							// 			style={{ padding: 10 }}
							// 			onClick={() =>
							// 				this.setState({
							// 					isChecked: !this.state.isChecked
							// 				})
							// 			}
							// 			isChecked={this.state.isChecked}
							// 			checkBoxColor={'#5DADE2'}
							// 		/>
							// 	<Text style={{flex:1, flexWrap:'wrap'}}>Do you want to sell products or services in SimSim Markets?</Text>
							// </View>
						}
						{
							this.state.isChecked &&
							<View style={[styles._radiotextbox, { justifyContent: 'center' }]}>
								<Text
									style={{
										alignSelf: 'center',
										color: '#7A7A7A',
										fontWeight: 'bold'
									}}
								>
									{strings('addPin.type')}{' '}
								</Text>
								<RadioGroup
									color="#7A7A7A"
									activeColor="#00ADDE"
									selectedIndex={this.state.sellerIndex}
									onSelect={(index, value) =>
										this.onSelect(index, value)
									}
									style={styles._radioButton}
								>
									<RadioButton
										color="#00ADDE"
										value="Individual"
										style={{ marginTop: 6 }}
									>
										<Text style={styles._radioText}
											style={{
												color: '#545454',
												paddingBottom: 2
											}}
										>
											{strings('addPin.individual')}
										</Text>
									</RadioButton>
									<RadioButton
										color="#00ADDE"
										value="Enterprise"
										style={{ marginTop: 6 }}
									>
										<Text style={styles._radioText}
											style={{
												color: '#545454',
												paddingBottom: 2
											}}
										>
											{strings('addPin.enterprises')}
										</Text>
									</RadioButton>
								</RadioGroup>
							</View>
						}
						{
							this.state.sellerType === 'Enterprise' && this.state.isChecked &&
							<View style={[styles._textbox, { justifyContent: 'center' }]}>
								<TouchableOpacity
									onPress={this.uploadImage.bind(
										this
									)}
									style={{
										flexDirection: this.props.isRTL
											? 'row-reverse'
											: 'row',
										justifyContent: 'space-between',
										marginLeft: 10
									}}
								>
									{this.state.fileName ? (
										<Text
											style={{
												alignSelf: 'center',
												color: '#7A7A7A',
												fontWeight: 'bold'
											}}
										>
											{this.state.fileName}
										</Text>
									) : (
											<Text
												style={styles._textboxText}
											>
												{strings('addPin.upload')}
											</Text>
										)}
									<Image
										style={{
											marginRight: 15,
											marginVertical: 8,
											width: 25,
											height: 25,
											resizeMode: 'contain',

										}}
										source={require('./../icons/upload2x.png')}
									/>
								</TouchableOpacity>
							</View>
						}

						<View
							style={[{
								flexDirection: this.props.isRTL
									? 'row-reverse'
									: 'row',
								justifyContent: 'space-between',
								marginTop: 20,
								
							},//this.state.isIosPicker ? {marginTop:260} : {marginTop:10}
							]}
						>
			{/* {*****************************Change Here & in state **************************************************} */}

							<TouchableOpacity
								style={[styles._UpdateButton, styles._button]}
								onPress={this._handleUpdate.bind(this)}
							>
								<View style={{flexDirection:"row",justifyContent:"space-between"}}>
								<View>
								<Text
									style={[
										styles._updatebuttonText,
										styles._buttonText
									]}
								>
									{strings('getDetail.update')}
								</Text>
								</View>
								<View style={{marginTop:15,marginLeft:10}}>{this.state.isUpdate ? <ActivityIndicator size="small" color="#FFFFFF" /> : null}</View>
								</View>
							</TouchableOpacity>

							<TouchableOpacity
								style={[styles._deleteButton, styles._button]}
								onPress={this.deleteProfileHandler}
							>
								<Text
									style={[
										styles._deletebuttonText,
										styles._buttonText
									]}
								>
									{strings('getDetail.delete_account')}
								</Text>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
				<Modal style={{ borderRadius:20,justifyContent:"center",alignItems:"center",height:"42%",width:"80%" }}
						position={"center"} ref={"modal1"} isDisabled={this.state.isDisabled}>
							<View style={{backgroundColor:"white",marginTop:15,borderRadius:20}}>									
								<DatePickerIOS
									date={new Date(this.state.dobIOS)}
									onDateChange={this.setDateIOS}
									maximumDate = {new Date()}
									mode='date'
								/>
								<TouchableOpacity onPress={this.hideIosDatePicekr}> 
									<View style={{justifyContent:"center",alignItems:"center",}}>
									<Text style={{ fontSize:25,fontWeight:"bold",color:"grey",marginHorizontal:"42%",marginBottom:8,marginTop:3,width:50}}>OK</Text>
									</View>
								</TouchableOpacity>
							</View>
							
				</Modal>

			</View>
		);
	}
}

function mapStateToProps(state) {
	//console.warn(state.network)
	return {
		isRTL: state.network.isRTL
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

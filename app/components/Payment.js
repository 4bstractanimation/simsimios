import React, { Component } from 'react';
import {
	Text,
	View,	
	StyleSheet,
	TouchableOpacity,	
	Alert,
	AsyncStorage,
	Platform,
	// WebView
} from 'react-native';
import { WebView } from 'react-native-webview';
import PreLoader from './PreLoader';
import { connect } from 'react-redux';
import API_URL from '../configuration/configuration';

class Payment extends Component {

	constructor(props) {
		super();
		this.state = {
			isLoading : true,
			isPaymentCompleted: false
		}
	}

	static navigationOptions = ({ navigation }) => ({
		 title: 'Payment Gateway',
		 headerTitleStyle : {textAlign: 'center',alignSelf:'center'},
		 headerStyle:{
		 	backgroundColor:'white',
		 },
	 });

	handleLoading = () => {
		this.setState({isLoading: false})
	}
	_onNavigationStateChange = (webViewState) => {
		//console.warn('onNavigationStateChange--->>',webViewState)

		if(webViewState.url === this.props.navigation.state.params.urls.close && !webViewState.loading) {
			this.processCompleted();
		} else if(webViewState.url === this.props.navigation.state.params.urls.abort && !webViewState.loading) {
			this.processAborted();
		} 

	}

	processCompleted = () => {
		// console.warn('processCompleted')
		const request = "<?xml version='1.0' encoding='UTF-8'?>\
				<mobile>\
				  <store>20793</store>\
				  <key>KnXhH-xHFZV#3FNf</key>\
				  <complete>"+this.props.navigation.state.params.urls.code+"</complete>\
				</mobile>";
				this.sendSecondRequest(request);

	}

	processAborted = () => {
		// console.warn('processAborted')		
		const request = "<?xml version='1.0' encoding='UTF-8'?>\
				<mobile>\
				  <store>20793</store>\
				  <key>KnXhH-xHFZV#3FNf</key>\
				  <abort>"+this.props.navigation.state.params.urls.code+"</abort>\
				</mobile>";
				this.sendSecondRequest(request);
	}

	sendSecondRequest = (data) => {
		const { isRTL } = this.props;
		const token = this.props.navigation.state.params.token;
		const orderId = this.props.navigation.state.params.orderId;
		const tId = this.props.navigation.state.params.tId;
		const cTid = this.props.navigation.state.params.cTid;
		const type = this.props.navigation.state.params.type;
		const favCatId = this.props.navigation.state.params.favCatId;
		const paymentStatus = this.props.navigation.state.params.paymentStatus;
		const deductedAmount = this.props.navigation.state.params.deductedAmount;
		const remainingCommercialBalance = this.props.navigation.state.params.remainingCommercialBalance;

		
		const url = "https://secure.innovatepayments.com/gateway/mobile_complete.xml";
		fetch(url, {
			method: 'POST',
			headers: {
				'Accept': 'application/xml',
				'Content-Type': 'text',				
			},
			body:data				
		}).then(response => response.text())
		.then(responseData => {	
				//console.warn('response-->>',responseData)
				const status = responseData.match('<status>(.*)</status>')[1];
				const code = responseData.match('<code>(.*)</code>')[1];
				const message = responseData.match('<message>(.*)</message>')[1];
				const tranref = responseData.match('<tranref>(.*)</tranref>')[1];
				const cvv = responseData.match('<cvv>(.*)</cvv>')[1];
				const avs = responseData.match('<avs>(.*)</avs>')[1];

				const paymentResponse = { 
					status: status, 
					code: code, 
					message: message, 
					tranref: tranref, 
					cvv: cvv, 
					avs: avs, 
					orderId: orderId,
					tId: tId,
					cTid: cTid,
					type: type,
					favCatId: favCatId
				};
				// console.warn('paymentResponse-->>',paymentResponse)
				//Alert.alert('Payment Status',message)
				this.setState({isPaymentCompleted: true});

				if(type === 'buyExtraSalesPoint') {
					
					
					if(status !== 'A') {
						Alert.alert('Payment Status',message);
						this.props.navigation.goBack();
						return;
					}

					const requestData = {...this.props.navigation.state.params.requestData, status, code, message, tranref, orderId };
					fetch(API_URL+'/extra-seller-pin', {
			        method: 'POST',
			        headers: {
			          Accept: 'application/json',
			          'Content-Type': 'application/json',
			          Authorization: token
			        },
			        body: JSON.stringify(requestData)
			      })
			      .then(response => response.json())
			      .then(async responseData => {
			         // console.warn('after payment for ext SP-->>',responseData);		
			        if (
			          responseData.status == 654864 &&
			          responseData.eData.eCode == 545864
			        ) {
			          alert('Data Not Found');
			        } else if (
			          responseData.status == 516324 &&
			          responseData.eData === null
			        ) {
			          // console.warn('after payment for ext SP-->>',responseData);	
			          	this.props.navigation.state.params.onGoBack();
						this.props.navigation.goBack();	
						const title = isRTL ? 'نجاح:' : 'Success:';
						const message = isRTL ? 'تم إضافة نقاط بيع إضافية بنجاح الى حساب ' : 'Extra sales points has been added in you account!';
			          	Alert.alert(title,message);			         
			        }
			      })
			      .catch(err => {
			        Alert.alert('Error', err.message);
			      });
				} else {
					if(status === 'A' && deductedAmount > 0) {
						this.updateWalletBalance(deductedAmount, remainingCommercialBalance, paymentStatus, orderId, tId, cTid, type)
					}
					fetch(API_URL+"/update-transaction-data", {
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							Authorization: token				
						},
						body: JSON.stringify(paymentResponse)				
					}).then(response => response.json())
					.then(responseData => {	
							// console.warn('after pay responseData--->>>', responseData)
							Alert.alert('Payment Status',message)

							if(this.props.navigation.state.params.onGoBack && this.props.navigation.state.params.screen ) {
								this.props.navigation.state.params.onGoBack();
								this.props.navigation.goBack();	
								
							} else {
								this.props.navigation.state.params.onGoBack();
							}

					}).catch(err => {
							Alert.alert('Error:',err.message);
							this.props.navigation.goBack();
					});
				}
				

		}).catch(err => {
				Alert.alert('Error:',err.message);
		});
	}

	updateWalletBalance = (deductedAmount, remainingBalance, paymentStatus, orderId, tId, cTid, type) => {
		
		if( deductedAmount === 0 ) {
			return;
		}
		const { isRTL } = this.props;
		const reason = type === "commercial" ? "دفع لتطبيق عرض خاص على الإعلانات" : "Payment for applying special offer on Ads";
		const commercialMessage = isRTL ? "تم ارسال طلب الإعلان التجاري بنجاح !" : "Commercial request submitted successfully!";
		const offerMessage = isRTL ? "تم تطبيق العرض بنجاح " : "Offer applied successfully!";
		const message = type === "commercial" ? commercialMessage : offerMessage;
		
		const token = this.props.navigation.state.params.token

		fetch(API_URL+'/update-wallet-balance', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': token,
			},
			body: JSON.stringify({
				reason: reason,
				deduct_amount: deductedAmount, 
				remaining_balance: remainingBalance,
				status: paymentStatus, 
				orderId, 
				tId, 
				cTid
			})
		}).then((response) => response.json())
		.then(async (responseData) => {
			// console.warn('responseData-->>',responseData)
			if (responseData.status == 200) {				
					Alert.alert('Success!', message);							
			}
		})
		.catch((err) => {
			Alert.alert('Error',err.message);
				
				
		});
	}

	render() {
		const urls = this.props.navigation.state.params.urls;
		// console.warn(urls)
		// console.warn(urls.start)
		
		return(	<View style={{ flex: 1, backgroundColor: '#FFF' }}>
					{this.state.isLoading && <PreLoader />}
					{ this.state.isPaymentCompleted ?
						<PreLoader />
						:
						<WebView						
							onLoad={this.handleLoading}
				        	source={{uri: urls.start}}  
				        	onNavigationStateChange={this._onNavigationStateChange} 
				      	/>
			      	}
		      	</View>
			)
	}
}


function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL
  };
}

export default connect(mapStateToProps)(Payment);
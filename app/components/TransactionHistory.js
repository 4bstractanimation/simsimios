import React, { Component } from 'react';
import { 
	Text, 
	View, 
	ScrollView, 
	StyleSheet, 
	Image, 
	Alert,
	AsyncStorage,
	TouchableOpacity,
	FlatList
} from 'react-native';
import Header from './Header';
import Data from './data';
import Tabs from './TabView';
import FloatingButton from './FloatingButton';
import moment from 'moment';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/TransactionHistory';
import PreLoader from './PreLoader';
import NoDataFound from './NoDataFound';
import API_URL from '../configuration/configuration';

class TransactionHistory extends Component {

  static navigationOptions = ({ navigation }) => {
		let {state} = navigation;
		if (state.params !== undefined) {
			return {
    			title: (state.params.isRTL === true) ? 'تاريخ التحويلات' : 'Transaction History'
  		}
		}
	}

	constructor() {
		super();
		this.state={
			isLoading: true,
			transaction:[],
			currentCurrencyValue: 1
		}
	}

	async getAllTransaction(){
		const getUserData = await AsyncStorage.getItem('user_data'); 
		const userData = JSON.parse(getUserData); 
		const token = userData.sData; 
		const userid = userData.userId;
		
		fetch(API_URL+'/transaction-history', {
        method: 'POST',
        headers: { 
         'Accept': 'application/json', 
         'Content-Type': 'application/json',
         'Authorization': token
        },
        body: JSON.stringify({ userId: userid
         })
        }).then((response) => response.json())
        .then((responseData) => {
        // console.warn('transaction-history-->>',responseData);
        if(responseData.status == 654864 && responseData.eData == null){
            this.setState({transaction: [], isLoading: false});
        }else if(responseData.status == 516324 && responseData.eData === null){
          this.setState({transaction: responseData.transactionHistory.reverse(), isLoading: false});
        } })
       .catch((err) => { 
      		Alert.alert('Error',err.messsage);
       });
	}

	async componentDidMount() {
		const { currencyCode } = this.props.country;
		const {setParams} = this.props.navigation;
		if (currencyCode !== 'SAR') {
			this.setState({
				currentCurrencyValue: await getCurrencyValue(currencyCode),
			})
		}
  		setParams({isRTL: this.props.isRTL});
		this.getAllTransaction();
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
    	this.props.navigation.setParams({ isRTL: nextProps.isRTL });
    	this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}

	render() {
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		const { currencyCode } = this.props.country;
		const { currentCurrencyValue } = this.state;
		const isRTL = this.props.isRTL;
		return(
			<View style={ styles.container }>
				{ this.state.isLoading ?
					<PreLoader />
					:
				<ScrollView>
				{this.state.transaction.length ? this.state.transaction.map((item, i) => {
					const dateTime = item.created_at;
					
					return(
					<View style={ styles._messageBox } key={i}>
	                	<View style={ styles._textBox }>	                		
			                <Text style={{color:'#000', textAlign: isRTL ? 'right' : 'left'}}>{isRTL ? 'رقم التحويل' : 'Transaction ID:'} {item.id}</Text>
			                <Text style={{color:'#000', textAlign: isRTL ? 'right' : 'left'}}>{dateTime}</Text>			               	
			                <Text style={{fontWeight: 'bold', color: '#000000', textAlign: isRTL ? 'right' : 'left'}}>{ isRTL ? 'المبلغ' : 'Price:'} {item.amount * currentCurrencyValue} {currencyCode}</Text>
											<Text style={{color: '#000',textAlign: isRTL ? 'right' : 'left',}}>{isRTL ? 'الغرض من الصفقة' : 'Transaction Purpose:'}{' '}{item.transaction_for}</Text>
		                	<Text style={{color:'#000', textAlign: isRTL ? 'right' : 'left'}}>{item.comments}</Text>		               		
		                </View>
	               	</View>
					)}
				) : <NoDataFound /> 
				}
				</ScrollView>
				}
			</View>
			)
	}
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    country: state.network.countryLocation,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionHistory)

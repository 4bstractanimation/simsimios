import React, { Component } from 'react';
import {
	Text,
	View,
	ScrollView,
	StyleSheet,
	Image,
	TouchableOpacity,
	Platform,
	AsyncStorage,
	Alert
} from 'react-native';

import Data from './data';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/PromotionAlert';
import NotificationHeader from './NotificationHeader';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import Toast from 'react-native-simple-toast';

import API_URL from '../configuration/configuration';

class Promotion extends Component {
	static navigationOptions = ({ navigation }) => {
		let state = navigation.state;
		if (state.params !== undefined) {
			return {
				alignSelf: Platform.OS === 'android' ? 'flex-end' : 'center',
				title:
					state.params.isRTL === true
						? 'منبه العروض'
						: 'Offer Alarm',
				headerRight: <NotificationHeader navigation={navigation} />
			};
		}
	};

	constructor() {
		super();
		this.state = {
			isLoading: true,
			promotion: []
		};
	}

	Capitalize(str) {
		if(!str) {
			return;
		}

		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	getBackSoon() {
		console.warn('getBackSoon')
	}

	async getAllAlerts() {
		const isRTL = this.props.isRTL;
		const getUserData = await AsyncStorage.getItem('user_data');
		const userData = JSON.parse(getUserData);
		const token = userData.sData;
		fetch(API_URL+'/offerAlertList', {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: token
			}
		})
			.then(response => response.json())
			.then(responseData => {
				//console.warn('offer alarm-->>',responseData)
				if (responseData.status == 654864) {
					this.setState({ isLoading: false, promotion: []});
				} else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					this.setState({
						promotion: responseData.OfferAlertList,
						isLoading: false
					});
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	componentDidMount() {
		const { setParams } = this.props.navigation;
		setParams({ isRTL: this.props.isRTL });
		this.getAllAlerts();
		
		this.props.navigation.addListener('didFocus', async event => {
			this.getAllAlerts();
		})
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
			this.props.navigation.setParams({ isRTL: nextProps.isRTL });
			this.props.navigation.state.params.isRTL = nextProps.isRTL;
		}
	}

	addOffer = () => {
		const { navigate } = this.props.navigation;
		navigate('AddOffer');
	}


	deleteAlertConfirm(id) {
		const message = this.props.isRTL ? 'هل تريد حذف هذا العرض إنذار ؟' : 'Do you want to delete this offer Alert?';
		const yesText = this.props.isRTL ? 'نعم' : 'Yes';
		const noText = this.props.isRTL ? 'لا' :  'No';
		Alert.alert(
		  '',
		  message,
		  [		    
		    {text: noText, style: 'cancel'},
		    {text: yesText, onPress: () => this.deleteAlert(id)},
		  ],
		  { cancelable: false }
		);

		
	}


	async deleteAlert(id) {
		const getUserData = await AsyncStorage.getItem('user_data');
		const userData = JSON.parse(getUserData);
		const token = userData.sData;
		const isRTL = this.props.isRTL;
		fetch(API_URL+'/deleteOfferAlert',
			{
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: token
				},
				body: JSON.stringify({
					id: id
				})
			}
		)
			.then(response => response.json())
			.then(async responseData => {
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 545864
				) {
					
					const message = isRTL ? 'فشل في مسح منبه العروض' : 'Failed to delete Promotion Alert'
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);

				} else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					const message = isRTL ? 'تم مسح منبه العروض بنجاح' : 'Offer Alarm is successfully deleted!'
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
					this.getAllAlerts();
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	render() {
		// console.warn(this.state.promotion)
		const isRTL = this.props.isRTL;
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		return (
			<View style={styles.container}>
				<View style={{ justifyContent: 'flex-end' }}>
					<TouchableOpacity
						style={styles._button}
						onPress={this.addOffer}
					>
						<Text style={styles._buttonText}>
							{strings('promotion.alert_button')}
						</Text>
					</TouchableOpacity>
				</View>
				<ScrollView>
					{this.state.isLoading ? (
						<PreLoader />
					) : this.state.promotion.length ? (
						this.state.promotion.map((arr, i) => {
							const catName = isRTL ? arr.arb_cat_name : this.Capitalize(arr.cat_name);
							const subCatName = isRTL ? arr.arb_subCat_name : arr.subCat_name;
							return (
								<View style={styles._messageBox} key={i}>
									<View style={styles._textBox}>
										<View
											style={{
												flexDirection: this.props.isRTL
													? 'row-reverse'
													: 'row',
												justifyContent: 'space-between'
											}}
										>
											<Text style={[styles._title, {flex:3, textAlign: isRTL ? 'right' : null}]}>
												{catName}
												{subCatName ? ' -->' : ''}{subCatName}
											</Text>										
												
											<TouchableOpacity onPress={this.deleteAlertConfirm.bind(this, arr.id)}>
												<Image style={[styles.icons, { marginHorizontal: 2, width:30, height:30 }]} source={require('./../icons/delete2x.png')} />
											</TouchableOpacity>
											
										</View>
									</View>
								</View>
							);
						})
					) : (
						<NoDataFound />
					)}
				</ScrollView>
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Promotion);

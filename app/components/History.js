import React, { Component } from 'react';
import { 
	Text, 
	View, 
	ScrollView, 
	StyleSheet, 
	Image,
	AsyncStorage,
	Alert, 
	TouchableOpacity,
	ActivityIndicator,
	FlatList
} from 'react-native';
import HomePageHeader from './maps/HomePageHeader';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as seachActions from '../actions/Search';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/History';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import Toast from 'react-native-simple-toast';

import API_URL from '../configuration/configuration';

class History extends Component {
	static navigationOptions = ({ navigation }) => {
		let {state} = navigation;
		if (state.params !== undefined) {
			return {
    		title: (state.params.isRTL === true) ? 'المحفوظات' : 'History'
    }}
	}
	constructor() {
		super();
		this.state = {
			isLoading: true,
			history: [],
		}
	}

	async getAllHistory(){ 
		this.setState({isLoading: true})
		const getUserData = await AsyncStorage.getItem('user_data'); 
		const userData = JSON.parse(getUserData); 
		const token = userData.sData; 
		fetch(API_URL+'/get-pin-history-list', 
			{ method: 'GET',
			 headers: 
			 { 
			 'Accept': 'application/json', 
			 'Content-Type': 'application/json', 
			 'Authorization': token 
			}
			 }).then((response) => response.json()) 
			 .then((responseData) => { 
			 	 //console.warn(" history ->> ",responseData.Pinhistory[1]);
			 	if (responseData.status == 654864 && responseData.eData.eCode == 545864) { 			 		
			 		this.setState({history: [], isLoading: false})
		 		} else if (responseData.status == 516324 && responseData.eData === null) { 
		 			this.setState({history: responseData.Pinhistory.reverse(), isLoading: false}); 
		 		}}) 
			 .catch((err) => { 
			  // console.warn(err); 
			}); 
	}

	componentDidMount() {
		this.props.setSelectedcategory({cat:'', id:''}); // reset selected categories
		const {setParams} = this.props.navigation;
    	setParams({isRTL: this.props.isRTL});
		this.getAllHistory();
	}

	deleteHistoryConfirmHandler = (id) => {

		const message = this.props.isRTL ? 'هل تريد إزالة نقطة البيع من المحفوظات؟' : 'Do you want to remove this Sales Point from history?';
		const yesButtonText = this.props.isRTL ? 'نعم' : 'Yes';
		const noButtonText = this.props.isRTL ? 'لا' :  'No';
		Alert.alert(
		  '',
		  message,
		  [		    
		    {text: noButtonText, style: 'cancel'},
		    {text: yesButtonText, onPress: () => this.deleteHistory(id)},
		  ],
		  { cancelable: false }
		);
	}

	async deleteHistory(id) {
		
		const getUserData = await AsyncStorage.getItem('user_data');
		 const userData = JSON.parse(getUserData);
		 const isRTL = this.props.isRTL;
		 const token = userData.sData;
		fetch(API_URL+'/remove-pin-history', {
  			method: 'POST',
    		headers: { 
     			'Accept': 'application/json', 
     			'Content-Type': 'application/json',
     			'Authorization': token,
    			},
    		body: JSON.stringify({ 
    			pinHistoryId:id
    		})
		}).then((response) => response.json())
		  .then(async (responseData) => { 
		  	 //console.warn(responseData);
		  	if(responseData.status == 654864 && responseData.eData.eCode == 545864){
		  		const message = isRTL ? 'فشل في الإزالة من المحفوظات' : 'Failed to delete history';
		  		Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
		  	} else if(responseData.status == 516324 && responseData.eData === null){
		  		const message = isRTL ? ' تم مسح المحفوظات بنجاح' : 'History deleted successfully!';
		  		Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
		  		this.getAllHistory();
		  	} 
		  })
		  .catch((err) => { 
		   Alert.alert('Error', err.message);
		});

	}

	render() {
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		const { state,navigate } = this.props.navigation
		return (
			<View style={ styles.container }>	
				
					{ this.state.isLoading ? 
						<PreLoader />
						: 

						this.state.history.length ? 
						<FlatList
						data={this.state.history}
						keyExtractor={(item) => item.id.toString()}
						renderItem={({ item }) =>  <TouchableOpacity key={item.id} onPress={() => navigate('SellerContainer', {item})}>
				              <View style={ styles._messageBox } >
				              	<View style={{ flex: 1.2 }}>
				              		<Image style={styles._image} source={{uri:(item.pin_image === undefined) ? '' :  item.pin_image}} />
				              	</View>
				              	<View style={ styles._textBox }>
				              		<View style={{flexDirection: this.props.isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between'}}>
				              			<Text style={ styles._title }>{item.pin_name}</Text>
					                  
					                  	<TouchableOpacity onPress={this.deleteHistoryConfirmHandler.bind(this, item.id)}>
											<Image style={[styles.icons, { marginHorizontal: 2, width:30, height:30 }]} source={require('./../icons/delete2x.png')} />
										</TouchableOpacity>
					                </View>
					                <View style={ styles._descriptionContainer }>
					                  	<Text style={ styles._description }>{item.description}</Text>
						            </View>
				                </View>
				              </View>
				            </TouchableOpacity>
			            
			          }
					/>
				 : <NoDataFound  />
        }
				
			</View>
		)
	}
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...layoutActions, ...seachActions}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(History)
import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

NoDataFound = ( props ) => {
		const isRTL = props.isRTL;
		return (
				<Text style={styles.message}>{props.message ? props.message : isRTL? 'لا يوجد' : 'No Record Found!'}</Text>
			)	
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

export default connect(mapStateToProps)(NoDataFound)


const styles = StyleSheet.create({
	message: {
		fontSize:20,
		fontWeight: 'bold', 
		textAlign:'center', 
		marginTop:25}
})
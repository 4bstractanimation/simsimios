import React, { Component } from 'react';
import {
	
	Alert,
	AsyncStorage,
	
} from 'react-native';

import API_URL from '../configuration/configuration';

export default function (token) {
	
		return fetch(API_URL+'/getPackage', {
				method: 'GET',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: token
				}
			}
		)
		.then(response => response.json())
		.then(responseData => {
			 //console.warn('package-->>',responseData);
			if (
				responseData.status == 654864 &&
				responseData.eData.eCode == 545864
			) {

				return 'No Data Found';
			} else if (
				responseData.status == 516324 &&
				responseData.eData === null
			) {
				//console.warn('package-->>>',responseData.package)
				return responseData.package;
			}
		})
		.catch(err => {
			Alert.alert('Error',err.message);
		});
}
import React, { Component } from 'react';
import {
	View, 
	Text, 
	StyleSheet, 
	TouchableOpacity, 
	TextInput, 
	ScrollView, 
	Alert,
	AsyncStorage,
	ActivityIndicator,	
} from 'react-native';
import moment from 'moment';
import API_URL from '../configuration/configuration';

export default class ChatMessages extends Component {

	static navigationOptions = ({navigation}) => {
		let  {state} = navigation;
		return {
			title: state.params.arr.to_user_name ? state.params.arr.to_user_name : 'No Name'
		}
		// if (state.params !== undefined) {
		// 	return {
		//     title: (state.params.isRTL === true) ? 'رسالة' : 'Messages', 
		//   }
		// }
	}

	constructor(props) {
		super(props);
		this.state = {
			messages: [],
			userId: '',
			token: '',
		}
	} 

	async getAllMessage(){ 
		const { arr } = this.props.navigation.state.params;
		const fromUserId = arr.from_user_id;
		const toUserId = arr.to_user_id;
		const getUserData = await AsyncStorage.getItem('user_data'); 
		const userData = JSON.parse(getUserData); 
		this.userDetail = userData
		const token = userData.sData; 
		fetch(API_URL+'/threadMessages', {
  			method: 'POST',
    		headers: { 
     			'Accept': 'application/json', 
     			'Content-Type': 'application/json',
     			'Authorization': token,
    			},
    		body: JSON.stringify({ 
    			from_user_id: fromUserId,
    			to_user_id: toUserId
    		})
		}).then((response) => response.json())
		  .then(async (responseData) => { 
		  	if(responseData.status == 654864 && responseData.eData.eCode == 545864){
		  		Alert.alert('Failed to fetch Messages');
		  	} else if(responseData.status == 516324 && responseData.eData === null){
		  		// console.warn(responseData.Allmessage)
		  		this.setState({messages:responseData.Allmessage, userId:userData.userId})
		  	} 
		  })
		  .catch((err) => {
		   console.log(err); 
		});
	} 

	componentDidMount() {
		this.getAllMessage()
		AsyncStorage.getItem('user_data').then(json => {
	    const userData = JSON.parse(json);
	    // console.warn(userData)
	    if(userData.sData)
	    this.setState({token: userData.sData, userId: userData.userId})
	  });
	}

	render() {
		
		const messages = this.state.messages;
		console.warn(messages)
		const userId = this.state.userId;
		const items = messages.map((item, index) => {
			const dateTime = moment(item.created_at).format('lll');
			// var date = moment(dateTime).format("DD MMM")
			return (
				<TouchableOpacity key={index} onPress={() => Alert.alert('Delete here ')}>
					<View style={[styles.messageBox, item.from_user_id == userId ? styles.ownmessage : {marginRight:50}]}>
						<Text style={{color:'#FFF'}}>{item.message}</Text>
						<Text style={{fontSize:10,marginTop:10}}>{dateTime}</Text>
					</View>
				</TouchableOpacity>
				);
		})
		return (
				<ScrollView style={styles.container}>
					{items.length ? 
						items : 
						<ActivityIndicator size="large" color="#40b2ef" />
					}
				</ScrollView>
			);
	}
}

const styles = StyleSheet.create({
	container: {
				flex:1,
				// flexDirection: isRTL ? 'row-reverse' : 'row',
				paddingTop: 10,
				marginLeft: 10,
				marginRight: 10,
				marginTop: 10,
				backgroundColor: '#FFFFFF',
			},
			messageBox: {
				marginBottom:10,
				paddingVertical:10,
				paddingHorizontal:10,
				backgroundColor:'#ba8c18',
				borderRadius:10,
			},
			ownmessage: {
				alignItems:'flex-end', 
				marginLeft:50, 
				backgroundColor:'#40b2ef',
			}
})

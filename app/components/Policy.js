import React, { Component } from 'react';
import Header from './Header';
import {Text, View, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import PreLoader from './PreLoader';
import { WEB_URL } from '../configuration/configuration';

class Policy extends Component { 

  static navigationOptions = ({ navigation }) => {
		let {state} = navigation;
		if (state.params !== undefined) {
			return {
    		title: (state.params.isRTL === true) ? 'سياسات' : 'Policy'
    }}
	}
	constructor(props) {
		super();
		this.state = {
			isLoading : true
		}
	}
	
	componentDidMount() {
		const {setParams} = this.props.navigation;
    setParams({isRTL: this.props.isRTL});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
    	this.props.navigation.setParams({ isRTL: nextProps.isRTL });
    	this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}

	handleLoading = () => {
		this.setState({isLoading: false})
	}

	getContent() {
		const isRTL = this.props.isRTL;
		if(isRTL) {
			return <WebView
				onLoad={this.handleLoading}
		        source={{uri: WEB_URL+'/privacyPolicyForApp?id=ar'}}			        
			/>	
		} else {
			return <WebView
				onLoad={this.handleLoading}
		        source={{uri: WEB_URL+'/privacyPolicyForApp?id=en'}}			        
			/>	
		}
	}
	render() {
		
		return (			
				<View style={{ flex: 1, backgroundColor: '#FFF' }}>	
					{this.state.isLoading && <PreLoader />}
					{this.getContent()}					
			   </View>
		)
	}
}

const styles = StyleSheet.create({
		container:{
			flex: 1,
			justifyContent: 'space-between',
			alignItems:'center',
			backgroundColor:'#ecf2f9'
		}
});

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Policy)

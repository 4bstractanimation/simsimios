import React, { Component } from 'react';
import {
	View,
	Text,
	Alert,
	ListView,
	Button,
	Image,
	StyleSheet,
	TouchableHighlight,
	TouchableOpacity,
	ScrollView,
	Platform,
	Dimensions,
	AsyncStorage,
	ActivityIndicator,
	TextInput,
	Picker,
	DatePickerIOS,
} from 'react-native';

import Header from './Header';
import AdDetails from './AdDetails';
import Navigation from './Navigation-Drawer'
import StarRating from 'react-native-star-rating';
const { width, height } = Dimensions.get('window');
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/SellerAds';
import Modal from "react-native-simple-modal";
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import DateTimePicker from 'react-native-modal-datetime-picker';
import getCurrencyValue from './getCurrencyValue.js';
import CheckBox from 'react-native-check-box';
import DeviceInfo from 'react-native-device-info';
// import getSymbolFromCurrency from 'currency-symbol-map';
import GetPackages from './GetPackages';
import getUserBalance from './getUserBalance';
import ModalDate from 'react-native-modalbox';
import API_URL from '../configuration/configuration';
import Toast from 'react-native-simple-toast';
import {AppEventsLogger} from "react-native-fbsdk";
import {db} from '../configuration/Config';
import RNIap,{
	Product,
	ProductPurchase,
	acknowledgePurchaseAndroid,
	purchaseUpdatedListener,
	purchaseErrorListener,
	PurchaseError,
 }  from 'react-native-iap';

const items = Platform.select({
	ios: [
		'com.package.first',
		'com.package.second',
		'com.package.third',
		'com.package.fourth',
	],
	android: [
	 ''
	]
   });

class SellerAds extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: 'SellerAds',
		headerTitleStyle: { textAlign: 'center', alignSelf: 'center' },
		headerStyle: {
			backgroundColor: 'white',
		},
	});

	constructor(props) {
		super(props);
	try {
      AppEventsLogger.logEvent("Pin Clicked");
   		 }catch (e) {

    	}
    	    if (props.map === true) {
      db.ref(
          'SHOPS/SHOP/' +
            props.navigation.state.params.item.pin_name +
            '/total',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });
      db.ref(
          'SHOPS/SHOP/' + props.navigation.state.params.item.pin_name + '/map',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });

      // DATE UPDATE

      db.ref(
          'SHOPS/SHOP/' +
            props.navigation.state.params.item.pin_name +
            '/date/' +
            new Date().toDateString() +
            '/total',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });
      db.ref(
          'SHOPS/SHOP/' +
            props.navigation.state.params.item.pin_name +
            '/date/' +
            new Date().toDateString() +
            '/map',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });

      // Daily update
      db.ref('D_SHOPS/D_SHOP/' + new Date().toDateString() + '/total')
        .transaction(total => {
          return (total || 0) + 1;
        });
      db.ref('D_SHOPS/D_SHOP/' + new Date().toDateString() + '/map')
        .transaction(total => {
          return (total || 0) + 1;
        });
    } else {
      db.ref(
          'SHOPS/SHOP/' +
            props.navigation.state.params.item.pin_name +
            '/total',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });
      db.ref(
          'SHOPS/SHOP/' + props.navigation.state.params.item.pin_name + '/list',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });

      // DATE UPDATE

      db.ref(
          'SHOPS/SHOP/' +
            props.navigation.state.params.item.pin_name +
            '/date/' +
            new Date().toDateString() +
            '/total',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });
      db.ref(
          'SHOPS/SHOP/' +
            props.navigation.state.params.item.pin_name +
            '/date/' +
            new Date().toDateString() +
            '/list',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });
      // Daily update
      db.ref('D_SHOPS/D_SHOP/' + new Date().toDateString() + '/total')
        .transaction(total => {
          return (total || 0) + 1;
        });
      db.ref('D_SHOPS/D_SHOP/' + new Date().toDateString() + '/list')
        .transaction(total => {
          return (total || 0) + 1;
        });
    }
		this.state = {
			ads: [],
			// Ads:true,
			loginUserId: '',
			pinUserId: '',
			token: '',
			responseMsg: '',
			modalOpen: false,
			discountToggle: 'no',
			offerDescription: {},
			discount: {},
			discountPrice: 0,
			isStartDateVisible: false,
			isEndDateVisible: false,
			startDate: '',
			endDate: '',
			country: '',
			singleAdPrice: '',
			adId: '',
			currentCurrencyValue: 1,
			selectPackgae: 0,
			packageIndex: '',
			commercialBalance: 0,
			remaingAds: [],
			package: {},
			discountedPrice: {},
			toggeOfferCheckbox: false,
			payableAmount: '',
			clickRate: '',
			t_status: '',
			isRead:false,
			readId:"",
			isUserClick: false,
			showIosStartDate:false,
			showIosEndDate: false,
			startDateIOS : new Date(),
			endDateIOS : new Date(),
			isDisabled: false,
			isSubloading: false,
			packages: [],
			appleInAppKey:''
		};
		this.specialOfferAds = [];
		//this.offerData = [];

	}

	getAds() {
		
		this.specialOfferAds = [];
		const pid = this.props.navigation.state.params.item.id;
		let adStatus = '';
		const { isRTL } = this.props;
		if (this.props.ad_status && this.props.ad_status.length === 1) {
			adStatus = this.props.ad_status[0];
		}
		// console.warn('selectedCat-->>',this.props.selectedCat)
		const selectedCat = this.props.selectedCat;
		let url;
		if (selectedCat && Object.keys(selectedCat).length) {
			url = API_URL+'/get-adslist?pid=' + pid + "&adStatus=" + adStatus + `&${selectedCat.cat}=` + selectedCat.id;
		} else {
			url = API_URL+'/get-adslist?pid=' + pid + "&adStatus=" + adStatus;
		}
		fetch(url, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		}).then((response) => response.json())
			.then(async (responseData) => {
				  console.warn('ads-->>', responseData)
				if (responseData.status == 654864 && responseData.eData == null) {
					this.setState({ responseMsg: responseData.eMsg, ads: [] });
				}
				else if (responseData.status == 654864 && responseData.eData.eCode == 545864) {
					const message = isRTL ? 'فشل في الوصول الى الإعلانات ' : 'Failed to get Ads'
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if(responseData.status == 516324 && responseData.eData === null) {
					// console.warn('ads inside if-->  ')
					this.setState({ ads: [...responseData.adsWithOffer, ...responseData.adswithoutOffer], clickRate: responseData.clickRate, remaingAds: responseData.remaingAds, toggeOfferCheckbox: false, adExtendDuration: responseData.ad_duration, responseMsg: '' });
				}
			}).catch((err) => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	purchaseUpdateSubscription = null;
	purchaseErrorSubscription = null;

	async componentDidMount() {
		// console.warn('componentDidMount....')
		const { currencyCode } = this.props.country;

		this.getAds();
		let token;
		const appName = DeviceInfo.getApplicationName();
		const deviceName = DeviceInfo.getDeviceName();
		const deviceId = DeviceInfo.getDeviceId();
		this.setState({ appName, deviceId, deviceName });

		this.props.navigation.addListener('didFocus', async event => {
			this.getAds();
		});

		AsyncStorage.getItem('user_data').then(async json => {
			const userData = JSON.parse(json);
			
			if (userData) {
				token = userData.sData;
				this.setState({
					token: userData.sData,
					loginUserId: userData.userId,
					pinUserId: this.props.navigation.state.params.item.user_id,
					pinId: this.props.navigation.state.params.item.id,					
				});
				AsyncStorage.getItem('isProfileCompleted').then(json => {
					const profileData = JSON.parse(json);
					//console.warn('profileData-->>',profileData)
					this.setState({ userProfile: profileData.userProfile })
				});
				const userBalance = await getUserBalance(token);
      			this.setState({commercialBalance: userBalance.balance, packages: await GetPackages(token)});

      			
			}
		});

		//*************************************************************
		/**************	Apple in app purchage code start  *************/
		
		if(Platform.OS == 'ios') {
			try {

			const iOSProducts = await RNIap.getProducts(items);
            // console.warn("TCL: componentDidMount -> products", iOSProducts );
			this.setState({ iOSProducts });
		  } catch(err) {
			
			Alert.alert('purchase error', JSON.stringify(err));
		  }

		this.purchaseUpdateSubscription = purchaseUpdatedListener((purchase) => {
			console.warn('purchaseUpdatedListener', purchase);
			const receipt = purchase.transactionReceipt;
			if (receipt) {
				  if (Platform.OS === 'ios') {
					RNIap.finishTransactionIOS(purchase.transactionId);
				  } else {
				  // Retry / conclude the purchase is fraudulent, etc...
				}
			  }
		})

		this.purchaseErrorSubscription = purchaseErrorListener((error) => {
			this.setState({isPurchased:false})
			console.warn('purchaseErrorListener', error);
			Alert.alert('purchase error', JSON.stringify(error));
		  })
		}
		
		/********************** Apple in app purchage code end  ****************/
		//***********************************************************************

		if (currencyCode !== 'SAR') {
			this.setState({
				currentCurrencyValue: await getCurrencyValue(currencyCode),
			})
		}
	}

	async getUserBalance() {

		const userBalance = await getUserBalance(this.state.token);
      	this.setState({commercialBalance: userBalance.balance})
	}

	deleteAd(id) {
		const { isRTL } = this.props;
		const message = isRTL ? 'هل تريد مسح هذا الإعلان؟ ' : 'Do you want to delete this ad?';
		const yesText = this.props.isRTL ? 'نعم' : 'Yes';
		const noText = this.props.isRTL ? 'لا' :  'No';
		Alert.alert('',	message,
			[
				{ text: noText, style: 'cancel' },
				{ text: yesText, onPress: () => this.deleteAdData(id) },
			],
			{ cancelable: false }
		);
	}

	async deleteAdData(id) {
		// const getUserData = await AsyncStorage.getItem('user_data');
		//  const userData = JSON.parse(getUserData);
		const { isRTL } = this.props;
		const token = this.state.token;
		fetch(API_URL+'/deleteAds', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': token,
			},
			body: JSON.stringify({
				adsId: id
			})
		}).then((response) => response.json())
			.then(async (responseData) => {
				// console.warn(responseData);
				if (responseData.status == 654864 && responseData.eData.eCode == 545864) {
					
					const message = isRTL ? 'فشل في مسح الإعلان ' : 'Failed to delete Ad';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if (responseData.status == 516324 && responseData.eData === null) {
					// Alert.alert('Delete Ad', 'Ad deleted successfully');
					const message = isRTL ? 'تم مسح الإعلان بنجاح' : 'Ad deleted successfully';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
					this.getAds();
				}
			})
			.catch((err) => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}

	Capitalize(str) {
		if (!str) {
			return;
		}
		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	// modalDidOpen = () => console.warn("Modal did open.");

	modalDidClose = () => {
		this.setState({ modalOpen: false });
	};

	onSelect(index, value) {
		this.setState({ discountToggle: value });
	}

	handleStartDate = date => {
		const chosenDate = moment(date).format('YYYY-MM-DD');
		this.setState({ startDate: chosenDate, isStartDateVisible: false, endDate: '' });
	}

	handleEndtDate = date => {

		const chosenDate = moment(date).format('YYYY-MM-DD');
		this.setState({ endDate: chosenDate, isEndDateVisible: false });
	}

	goForPayment = (orderId, tId, cTid, currencyCode, payableAmount, userId,favCatId) => {
		const { appName, deviceId, deviceName } = this.state;
		const { name, email } = this.state.userProfile;
		const { isRTL } = this.props;
		const { deductedAmount, remainingCommercialBalance } = this.state;

		/** Full: full payment from wallet | Partial: partial payment from wallet */
		// let paymentStatus = payableAmount === 0 ? "Full" : "PaymentGateway";
		let paymentStatus = 'Full'
		

		if(payableAmount === 0 ) {		
			this.updateWalletBalance(deductedAmount, remainingCommercialBalance, paymentStatus, orderId, tId, cTid);
			/** Here no need to go to payment gateway because payable amout is zero. */
			return 0;
		}

		if (!email || !name) {
			const message = isRTL ? 'يرجى اكمال ملفك الشخص أولا ' : 'Please complete your profile first';
			Alert.alert('', message);
			return;
		}

		if(Platform.OS == 'ios') {
			
			const appleInAppKey = this.state.appleInAppKey;  // This is apple in app purchage key 

			RNIap.requestPurchase(appleInAppKey).then(purchase => {
					
					//console.warn('purchase Ios resp ->', purchase);
					this.sendPurchaseData(purchase, orderId, tId, cTid, payableAmount);
					//this.getResponseFromPaymentGateway();
			}).catch((error) => {					
					Alert.alert('Error', error.message)
			})

		} else {

		const url = "https://secure.innovatepayments.com/gateway/mobile.xml"
		const data = '<?xml version="1.0" encoding="UTF-8"?>\
		<mobile>\
		   	<store>20793</store>\
		 	<key>KnXhH-xHFZV#3FNf</key> \
		   	<device>\
			    <type>'+ deviceName + '</type>\
			    <id>'+ deviceId + '</id>\
			    <agent></agent>\
			    <accept></accept>\
		  	</device>\
		   	<app>\
			    <name>'+ appName + '</name>\
			    <version>beta</version>\
			    <user>'+ name + '</user>\
			    <id>'+ userId + '</id>\
		  	</app>\
		  <tran>\
		  <test>1</test>\
		  <type>PAYPAGE</type>\
		  <cartid>'+ orderId + '</cartid>\
		  <description>Transaction description</description>\
		   <currency>'+ 'AED' + '</currency>\
		    <amount>'+ payableAmount + '</amount>\
		  </tran>\
		  <billing>\
		    <email>'+ email + '</email>\
		  </billing>\
		</mobile>';

		fetch(url, {
			method: 'POST',

			headers: {
				'Accept': 'application/xml',
				'Content-Type': 'text',
			},
			body: data

		}).then(response => response.text())
			.then(responseData => {
				// console.warn('xml response-->>',responseData)
				//console.warn('xml response-->>',responseData.match('<start>(.*)</start>')[1])
				const start = responseData.match('<start>(.*)</start>')[1];
				const close = responseData.match('<close>(.*)</close>')[1];
				const abort = responseData.match('<abort>(.*)</abort>')[1];
				const code = responseData.match('<code>(.*)</code>')[1];
				const urls = { start, close, abort, code };

				this.props.navigation.navigate('Payment', { 
					urls, 
					screen: 'offer', 
					type: 'adsOffer', 
					orderId, 
					tId, 
					favCatId, 
					cTid, 
					deductedAmount, 
					remainingCommercialBalance, 
					paymentStatus,
					token: this.state.token, 
					onGoBack: () => this.getResponseFromPaymentGateway() 
				})

			}).catch(err => {
				Alert.alert('Error:', err);
			});
		}
	}

	sendPurchaseData = (purchase, orderId, tId, cTid, payableAmount) => {

	const iOSTransactionData = {
		iosReceipt : purchase.transactionReceipt,
		transactionDate : purchase.originalTransactionDateIOS,
		transactionId :  purchase.transactionId,
		productId : purchase.productId,
		planId : this.state.appleInAppKey,
		planType: 'Add Offer',
		orderId: orderId,
		discounted_price: payableAmount,
		tId,
		cTid
	};
	
		fetch(API_URL+'/apple-promotion-payment', {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: this.state.token					
				},
				body: JSON.stringify(iOSTransactionData)
			})
			.then(response => response.json())
			.then(responseData => {
				//console.warn('payment api resp --->>',responseData);
				// this.setState({isPurchased:false});
				// this.onClickCancel();
				this.getResponseFromPaymentGateway();
			})
			.catch(err => {
				Alert.alert('Error',err.message);
			});
	}

	getResponseFromPaymentGateway = () => {
		this.getAds();
		this.getUserBalance();
	}

	updateWalletBalance = (deductedAmount, remainingBalance, paymentStatus, orderId, tId, cTid) => {
		
		if( deductedAmount === 0 ) {
			return;
		}
		const { isRTL } = this.props;
		fetch(API_URL+'/update-wallet-balance', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': this.state.token,
			},
			body: JSON.stringify({
				type:"Ads",
				reason: "Payment for applying special offer on Ads",
				deduct_amount: deductedAmount, 
				remaining_balance: remainingBalance,
				status: paymentStatus, 
				orderId, 
				tId, 
				cTid
			})
		}).then((response) => response.json())
		.then(async (responseData) => {
			// console.warn('responseData-->>',responseData)
			if (responseData.status == 200) {
				// if(paymentStatus === "Full") {
					const title = isRTL ? 'نجاح' : 'Success'
					const message = isRTL ? '!تم اعتماد العرض بنجاح ' : 'Offer applied successfully!'
					Alert.alert(title, message);				
					this.getAds();
					this.getUserBalance();
				// }				
			}
		})
		.catch((err) => {
			const errorText = isRTL ? 'خطأ' : "Error";
			Alert.alert(errorText, err.message);				
		});
	}

	handleSubmitOffer = () => {		
		
		const { isRTL } = this.props;
		/*
		if (Object.values(this.state.offerDescription).length !== this.specialOfferAds.length) {
			Alert.alert('Error', 'Please fill the Description input box');
			return;
		}
		*/
		const totalNullDiscount = this.specialOfferAds.filter( ad => ad.discount === '');
		const errorText = isRTL ? 'خطأ' : "Error";
		if(totalNullDiscount.length) {
			const message = isRTL ?  'كل حقول الخصم اجبارية ' : 'All discount fields are mandatory';
			Alert.alert(errorText, message);
			return;
		}
		
		const { currencyCode } = this.props.country;
		const userId = this.state.loginUserId;
		const payableAmount = this.state.payableAmount;

		if (isNaN(payableAmount)) {
			const message = isRTL ? 'لم يتم إيجاد المبلغ ' : 'Payable Amount not found.';
			Alert.alert(errorText, message);
			return;
		}

		if (!currencyCode) {
			const message = isRTL ? 'لم يتم إيجاد العملة  ' : 'Currency code not found.';
			Alert.alert(errorText, message);
			return;
		}		

		const startDate = Platform.OS === 'ios' ?  this.extractDate(this.state.startDateIOS) : this.state.startDate;
		const endDate =  Platform.OS === 'ios' ? this.extractDate(this.state.endDateIOS) : this.state.endDate;
		const packageId = this.state.packageIndex;

		if (!startDate) {
			const message = isRTL ? 'حدد تاريخ البداية ' : 'Fill start date';
			Alert.alert('', message);
			return;
		}
		if (!endDate) {
			const message = isRTL ? 'حدد تاريخ النهاية ' : 'Fill end date';
			Alert.alert('', message);
			return;
		}
		if (!packageId) {
			const message = isRTL ? ' اختر باقة' : 'Select package';
			Alert.alert('', message);
			return;
		}
		// const pin_name = this.props.navigation.state.params.item.pin_name;
		// console.warn(pin_name)
		this.setState({isSubloading: true})
		const offerData = this.specialOfferAds.map(({ id, user_id, pin_id, add_price, discount, discountedPrice, offerDescription }) => ({
			adId: id,
			userId: user_id,
			pinId: pin_id,
			adPrice: add_price,
			disPercentage: discount,
			discountedPrice,
			startDate,
			endDate,
			offerDiscription: offerDescription ? offerDescription : "", // offerDiscription according to avinash i have  written this.
			packageId,
			packageValue:this.state.selectPackgae,
			clickRate: this.state.clickRate,
		}));

		console.warn('offerData req params-->>',offerData)
	
		if (startDate !== '' && endDate !== '' && packageId !== '') {
			fetch(API_URL+'/addOffer', {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
					'Authorization': this.state.token,
				},
				body: JSON.stringify({ offerData: offerData })
			}).then((response) => response.json())
				.then(async (responseData) => {
					//  console.warn('addOffer res-->',responseData);
					if (responseData.status == 654864 && responseData.eData.eCode == 545864) {
						this.setState({isSubloading: false})
						Alert.alert('Data not found!');
					} else if (responseData.status == 516324 && responseData.eData === null) {
						// Alert.alert('Wait!','We are ');

						const orderId = responseData.orderId;
						const favCatId = responseData.favCatId;
						// const tId = responseData.tId;
						// const cTid = responseData.cTid
						this.goForPayment(orderId, orderId, orderId, currencyCode, payableAmount, userId, favCatId)
						this.setState({
							modalOpen: false,
							startDate: '',
							endDate: '',
							discount: {},
							offerDescription: {},
							discountedPrice: {},
							discountToggle: 'No',
							isSubloading: false,
						});

					}
				})
				.catch((err) => {
					const errorText = isRTL ? 'خطأ' : "Error";
					Alert.alert(errorText, err.message);
				});

		} else {
			this.setState({isSubloading: false});
			Alert.alert('', 'Some fields are missing')
		}
	}

	setOfferDescription(description, index, id) {
		const singleAd = this.specialOfferAds[index];
		const offerDescription = { ...this.state.offerDescription };
		offerDescription[id] = description;
		this.setState({ offerDescription });
		singleAd.offerDescription = description;
		this.specialOfferAds[index] = singleAd;
	}

	setDiscount(discount, index, id) {
		const singleAd = this.specialOfferAds[index];
		const singleAdPrice = singleAd.add_price;
		//const id = singleAd.id;
		const { isRTL } = this.props;
		if (discount > 99) {
			const message = isRTL ? '100% ' : 'Discount percentage should be less than or equal to 100';
			Alert.alert('', message)
			//this.setState({discount: ''})
			return;
		}

		const discountState = { ...this.state.discount };
		const getDiscount = discount.replace(/[^0-9]/g, '');
		discountState[id] = getDiscount;
		const adPrice = parseFloat(singleAdPrice);
		const discountedPrice = { ...this.state.discountedPrice }
		discountedPrice[id] = getDiscount ? adPrice - (adPrice * getDiscount / 100) : 0;
		this.setState({ discount: discountState, discountedPrice });
		//console.warn(discountedPrice,discount)
		singleAd.discount = getDiscount;
		singleAd.discountedPrice = discountedPrice[id];

		this.specialOfferAds[index] = singleAd;

	}

	selectPackgaeValue = (packageValue, packageIndex) => {
		const selectPackgae = parseFloat(packageValue);		
		let appleInAppKey = null;
		if(this.state.packages[packageIndex - 1]) {
			appleInAppKey = this.state.packages[packageIndex - 1].key;
		}
		
		//console.warn('appleInAppKey-->>',appleInAppKey)
		const currentCurrencyValue = this.state.currentCurrencyValue;
		let commercialBalance = (this.state.commercialBalance * currentCurrencyValue).toFixed(2);
		let payableAmount;
		let remainingCommercialBalance;
		let deductedAmount;
		if (parseFloat(commercialBalance) >= selectPackgae) {
			payableAmount = 0;
			deductedAmount = selectPackgae;
			remainingCommercialBalance = commercialBalance - selectPackgae;
		} else {
			payableAmount = selectPackgae;
			deductedAmount = 0;
			remainingCommercialBalance = commercialBalance;
		}
		this.setState({ selectPackgae: packageValue, packageIndex, payableAmount, deductedAmount, remainingCommercialBalance, appleInAppKey })
		
	}

	extendingAdsHandler(adsId) {
		const { isRTL } = this.props;
		fetch(API_URL+'/extendAds', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': this.state.token,
			},
			body: JSON.stringify({
				adsId
			})
		}).then((response) => response.json())
		.then(async (responseData) => {
			//console.warn('responseData-->>',responseData)
			if (responseData.status == 654864 && responseData.eData.eCode == 545864) {
				Alert.alert('Data not found!');
			} else if (responseData.status == 516324 && responseData.eData === null) {
				// Alert.alert('Success!', 'Ad Extended successfully!');
				const message = isRTL ?  'تم تمديد الإعلان بنجاح ' : 'Ad Extended successfully!';
				Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				this.setState({
					modalOpen: false,
					offerDescription: '',
					startDate: '',
					endDate: '',
					discount: '',
					discountToggle: 'No'
				});
				this.getAds();
			}
		})
		.catch((err) => {
			const errorText = isRTL ? 'خطأ' : "Error";
			Alert.alert(errorText, err.message);
		});
	}

	showRemainingAds = () => {
		this.setState({ ads: [...this.state.ads, ...this.state.remaingAds], remaingAds: [] })
	}

	adDetailsAdded({ id, user_id, pin_id, product_name, add_price, ad_image }) {
		const obj = { id, user_id, pin_id, product_name, add_price, ad_image, discount: '',discountedPrice:add_price };

		const adIndex = this.specialOfferAds.findIndex(ad => ad.id === id);
		if (adIndex < 0) {
			this.specialOfferAds.push(obj)
		} else {
			this.specialOfferAds.splice(adIndex, 1)
		}
		//console.warn(this.specialOfferAds)
	}

	showSpecialOfferListMoldal = () => {
		const { isRTL } = this.props;
		if (this.specialOfferAds.length) {
			this.setState({ modalOpen: true })
		} else {
			const message = isRTL ? 'اختر اعلان' : 'Select ads'
			Alert.alert('', message);
		}
	}

	toggeOfferCheckboxHandler = () => {
		const { isRTL } = this.props;
		if (this.state.toggeOfferCheckbox) {
			this.specialOfferAds = [];
		} else {
			//console.warn('salim',this.state.userProfile);
			const { name, email } = this.state.userProfile;
			if (!name || !email) {
				const message = isRTL ? 'قبل اعتماد العروض الخاصة يجب اكمال ملفك الشخصي' : 'Before apply special offer you have to complete your profile'
				Alert.alert('', message);
				return;
			}

		}
		this.setState((prevState) => ({ toggeOfferCheckbox: !prevState.toggeOfferCheckbox }))
	}

	dateComp = (today, eDate) => {	

		if(eDate >= today) {
			return true;
		} else {
			return false;
		}
	}

	selectUserClick = () => {
	this.setState({isUserClick: !this.state.isUserClick})
}

	readMoreHandle = (i) => {
		
		this.setState({isRead:true,readId:i});
		
	}

	readLessHandle = (i) => {
		this.setState({isRead: false, readId:i})
	}

	extractDate = (newDate) => {	
	
       	var dd = String(newDate.getDate()).padStart(2, '0');
       	var mm = String(newDate.getMonth() + 1).padStart(2, '0'); //January is 0!
       	var yyyy = newDate.getFullYear();
        newDate = yyyy + '-' + mm + '-' + dd;

       return newDate;
    }

	setStartDateIOS = (newDate) => {
		this.setState({startDateIOS: newDate, showIosEndDate: false});
	}		
	
	showIosStartDatePicker = () => {
		this.setState({showIosStartDate:true,});
		this.refs.modal1.open()	
	}
	
	hideIosStartDatePicekr = () => {
		this.refs.modal1.close()
		
	}

	setEndDateIOS = (newDate) => {
		this.setState({endDateIOS: newDate});
		}

	showIosEndDatePicker = () => {
		this.setState({showIosEndDate:true,});
		this.refs.modal2.open()	
	}

	hideIosEndDatePicekr = () => {
		this.refs.modal2.close()
		
	}

	isAdHasOfferHandler(adData) {

		const { start_date, end_date } = adData;

		if(!start_date || !end_date) {
			return;
		}
		
		const startTime = new Date(start_date).setMilliseconds(0);
		const endDate = new Date(end_date)
		const endTime = endDate.setDate(endDate.getDate() + 1);
		const today = Date.now();

		return  today >= startTime && today < endTime ;		
	}

	getArabicConditionText(text) {
    
		if(!text) {
			return;
		}

		if(text.toLowerCase() === 'new') {
			return "جديد"
		} 
		
		if(text.toLowerCase() === "used") {
			return "مستعمل"
		}

		return text;
	}

	render() {
		
		// console.warn("adExtendDuration::   ", this.state.adExtendDuration)
		const { currencyCode } = this.props.country;
		const currentCurrencyValue = this.state.currentCurrencyValue;
		// console.warn('currency-->',currentCurret_status: responseData.allAds[0].t_status,ncyValue)
		const pinName = this.props.navigation.state.params.item.pin_name;
		// const pinId = this.props.navigation.state.params.item.id;
		const pinType = this.props.navigation.state.params.item.pin_type; // like Enterprise or Individual
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		const { navigate } = this.props.navigation;
		const type = 'edit';
		const adExtendDuration = this.state.adExtendDuration;
		const startDate = Platform.OS === 'ios' ? new Date(this.state.startDateIOS) : new Date(this.state.startDate);
		const miniEndDate = startDate.setDate(startDate.getDate() + 1);
		const packages = this.state.packages ? this.state.packages : [];
		const inputStyle = { borderRadius: 25, backgroundColor: '#fff' }
		const specialOfferClickCost = this.state.clickRate; 
		return (
			<View style={{ flex: 1 }}>
				{this.state.pinUserId == this.state.loginUserId && pinType && (pinType.toLowerCase() === "enterprise" || pinType == "فردي") && this.state.token  && this.state.ads.length ?
					<View>
						{this.state.toggeOfferCheckbox ?
							<View style={{ flexDirection: 'row' }}>
								<TouchableOpacity
									style={{ flex: 2, marginTop: 10, marginHorizontal: 10 }}
									onPress={this.showSpecialOfferListMoldal}
								>
									<View style={[styles.button, { marginLeft: 3, alignItems:'center', justifyContent:'center' }]}>
										<Text style={[styles.buttonText, { textAlign: 'center' }]}>{strings('offer.applyOffer')}</Text>
									</View>

								</TouchableOpacity>
								<TouchableOpacity
									style={{ flex: 1, marginTop: 10, marginHorizontal: 10 }}
									onPress={this.toggeOfferCheckboxHandler}
								>
									<View style={[styles.button, { marginLeft: 3, alignItems:'center', justifyContent:'center' }]}>
										<Text style={[styles.buttonText, { textAlign: 'center' }]}>{strings('offer.cancel')}</Text>
									</View>

								</TouchableOpacity>
							</View>
							:
							<TouchableOpacity
								onPress={this.toggeOfferCheckboxHandler}
								style={{ margin: 10, marginBottom:0  }}
							>
								<View style={[styles.button, {height: 30, marginLeft: 3, backgroundColor: '#b20715', alignItems:'center', justifyContent:'center' }]}>
									<Text style={[styles.buttonText, { textAlign: 'center', }]}>{strings('offer.addOffer')}</Text>
								</View>
							</TouchableOpacity>
						}
					</View>
					: null
				}
				{!this.state.responseMsg ? <ScrollView>

					{this.state.ads.length ? this.state.ads.map((item, i) => {
						// console.warn('item-->>', item)
						const expiryDate = moment(item.ad_duration, 'YYYY-MM-DD HH:mm:ss');
						// console.warn('expiryDate',expiryDate, +adExtendDuration)
						expiryDate.add(+adExtendDuration, 'days');
						const newDate = moment();
						const remainingDate = expiryDate.diff(newDate, 'days');
						// console.warn('remainingDate',remainingDate)
												
						
						// const endDate = moment().toDate(dt+' 11:59:59 pm').getTime();
						// const today = moment().toDate().getTime();
						// const dt = item.endDate;
						// const today = moment().toDate();
						// const endDate = moment(dt).toDate();						
						// const dateCheck = this.dateComp(today, endDate);

						// const isAdOffer = item.ads_have_offer; // Yes or No

						const isAdHasOffer = this.isAdHasOfferHandler(item);
						console.warn('isAdHasOffer--->>',isAdHasOffer, item.t_status)
						const offerDesc = item.offer_description;
						const discountAvailable = item.ads_have_offer;
						const discountPercentage = item.dis_percentage;
						console.warn("discount percan",discountPercentage)
						const discountedPrice = item.discounted_price;
						let rating = item.rating;
						rating = rating ? rating.toFixed(1) : '';
						item.pinName = pinName;	
						return (
							<TouchableOpacity
								onPress={() => { this.props.navigation.navigate('AdDetails', { item, pinDetails: this.props.navigation.state.params.item }) }}
								style={styles.container} key={i}
							>
								
								{ isAdHasOffer && 
									<View style={styles.adOffer}>

										<View style={{ flexDirection: 'row', alignItems: 'center' }}>
											
												<Text style={{color: '#cc0000', textDecorationLine: discountPercentage > "0" ? 'line-through' : '' }}>{parseFloat(item.add_price * currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
												{ discountPercentage > "0" ?
												<View style={{ flexDirection: 'row', alignItems: 'center' }}>
													<Text>{' / '}</Text>
													<Text style={{ fontWeight: 'bold', fontSize: 16, color: '#cc0000' }}>{parseFloat(discountedPrice * currentCurrencyValue).toFixed(2)} {currencyCode} </Text>
													<Text>{'  '}</Text>
													<Text style={{ fontSize: 12, fontWeight: 'bold', color: '#cc0000' }}>{discountPercentage}% OFF</Text>
												</View>
												: null
												}
										</View>
										{/* <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#cc0000' }}>{offerDesc}</Text> */}
									</View>
								}

								<View style={styles.container1}>
									<View style={{ flex: 1, flexDirection: 'row' }}>
										{this.state.toggeOfferCheckbox && !this.props.isRTL &&
											<CheckBox
												style={{ flexDirection: 'row' }}
												onClick={() => { this.adDetailsAdded(item) }}
											// isChecked={this.state.checked}
											// rightText={this.props.isRTL ? 'Apply Offer' : 'Apply Offer AR'}

											/>
										}
										{item.ad_image && item.ad_image.length ?
											<Image style={styles.image}
												source={{ uri: item.ad_image[0].image_name }}
											/>
											: <Image style={styles.image}
												source={require('../icons/blankImage.png')}
											/>

										}
										{this.state.toggeOfferCheckbox && this.props.isRTL &&
											<CheckBox
												style={{ flexDirection: 'row' }}
												onClick={() => { this.adDetailsAdded(item) }}
											// isChecked={this.state.checked}
											// rightText={this.props.isRTL ? 'Apply Offer' : 'Apply Offer AR'}

											/>
										}
									</View>
									<View style={{ flex: 2.6, marginLeft: 10 }}>
										<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
											<View style={{ flex: 1.2 }}>
												<Text style={[styles.title, { flexWrap: 'wrap' }]}>{this.Capitalize(item.product_name)} </Text>
											</View>
											<View style={{ flex: 2, justifyContent: 'flex-end', flexDirection: this.props.isRTL ? 'row-reverse' : 'row', paddingTop: 4 }}>
												{this.state.pinUserId == this.state.loginUserId && this.state.token ?
													<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
														<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
															<TouchableOpacity onPress={() => navigate('AddNewAd', { item, type, pinDetails: this.props.navigation.state.params.item })}>
																<Image style={styles.icons} source={require('./../icons/edit2x.png')} />
															</TouchableOpacity>
															<TouchableOpacity onPress={this.deleteAd.bind(this, item.id)}>
																<Image style={[styles.icons, { marginHorizontal: 2 }]} source={require('./../icons/delete2x.png')} />
															</TouchableOpacity>
														</View>
													</View>
													: null
												}
												<Text style={{ color: 'green' }}> {rating} </Text>
												<StarRating
													disabled={true}
													maxStars={5}
													rating={item.rating}
													starSize={13}
													emptyStarColor="#cacdd1"
													fullStarColor="green"
													emptyStar="star"
													containerStyle={{ paddingTop: 3 }}
												/>
											</View>
										</View>
										
										{ 
											item.description && item.description.length < 100 ?  
											
												<Text style={styles.description}>{this.Capitalize(item.description.substring(0, 100))}</Text>	
												: 

												
													!!item.description && this.state.isRead && i === this.state.readId ? 
													<Text style={styles.description}>{this.Capitalize(item.description)}
													 <Text onPress={() => this.readLessHandle(i)} style={{color:"darkgrey", fontSize:15}}> Read less...</Text></Text>
												: 
												
												!!item.description && <Text style={styles.description}>{this.Capitalize(item.description.substring(0, 100))} 
													<Text onPress={() => this.readMoreHandle(i)} style={{color:"grey", fontSize:15}}> Read more...</Text>
												</Text> 
												
										}
										
										<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row', justifyContent: 'space-between' }}>
											{ isAdHasOffer ?
											<View style={{ flex: 1, justifyContent: 'space-between', flexDirection: this.props.isRTL ? 'row-reverse' : 'row', marginTop: 5 }}>
											<Text style={[styles.price, { marginTop: 4, textDecorationLine:  'line-through'}]}>{(parseFloat(item.add_price) * currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
											 <Text style={[styles.price, { marginTop: 4 }]}>{(parseFloat(item.discounted_price) * currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
											{item.add_status ?
												<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
													<Text style={[styles.price, { color: '#000000' }]}> {this.props.isRTL ? "الحالة" : "Condition: "}</Text>
													<Text style={styles.price}>{this.props.isRTL ? this.getArabicConditionText(item.add_status) : item.add_status}</Text>
												</View>
												: null
											}
											</View>
										:
										<View style={{ flex: 1, justifyContent: 'space-between', flexDirection: this.props.isRTL ? 'row-reverse' : 'row', marginTop: 5 }}>
										<Text style={[styles.price, { marginTop: 4, textDecorationLine: 'none' }]}>{(parseFloat(item.add_price) * currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
										{item.add_status ?
											<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
												<Text style={[styles.price, { color: '#000000' }]}> {this.props.isRTL ? "الحالة" : "Condition: "}</Text>
												<Text style={styles.price}>{this.props.isRTL ? this.getArabicConditionText(item.add_status) : item.add_status}</Text>
											</View>
											: null
										}
								</View>
											}
												 {/* <Text style={[styles.price, { marginTop: 4 }]}>{(parseFloat(item.add_price) * currentCurrencyValue).toFixed(2)} {currencyCode}</Text> */}
											
										</View>
										
											{this.state.token !== "" && this.state.pinUserId === this.state.loginUserId && remainingDate < 0 &&
											<View style={{ flexDirection: 'row'}}>
												<TouchableOpacity
													onPress={() => { this.extendingAdsHandler(item.id) }}
													style={[styles.button, { marginTop: 10, backgroundColor: '#00b33c' }]}
												>
													<Text style={[styles.buttonText, { textAlign: 'center', fontSize:11}]}>{this.props.isRTL ? "تمديد الاعلان" : "Extend Ad"}</Text>
												</TouchableOpacity>
												<Text> </Text>
												<TouchableOpacity
													onPress={() => { this.deleteAd(item.id) }}
													style={[styles.button, { marginTop: 10, backgroundColor: '#912f2a' }]}
												>
													<Text style={[styles.buttonText, { textAlign: 'center' , fontSize:11}]}>{this.props.isRTL ? "إزالة الاعلان" : "Remove Ad"}</Text>
												</TouchableOpacity>											
									
											</View>
											}
									</View>
								</View>
							</TouchableOpacity>
						);
					})
						: <ActivityIndicator size="large" color="#40b2ef" />
					}

					{!!this.state.remaingAds.length &&
						<TouchableOpacity
							onPress={this.showRemainingAds}
							style={{ margin: 10, borderRadius: 25, alignItems: 'center', marginLeft: 10, padding: 15, backgroundColor: '#4286f4' }}>
							<Text style={{ color: '#FFFF', fontWeight: 'bold' }}>{this.props.isRTL ? 'عرض الإعلانات المتبقية' : 'Show Remaining Ads'}</Text>
						</TouchableOpacity>
					}

				</ScrollView> :
					<View style={{ marginTop: 25, alignItems: 'center', justifyContent: 'center', marginHorizontal: 10 }}>
						<Text style={{ fontWeight: 'bold', fontSize: 16 }}>{this.props.isRTL ? ' لإظهار نقطة البيع على الخريطة وفي القائمة يجب إضافة منتجات وخدمات فيها'  : 'To show sales point on map and list you need to add ads to it.'}</Text>
					</View>
				}
				<Modal
					closeOnTouchOutside={false}
					open={this.state.modalOpen}
					modalDidClose={this.modalDidClose}
					style={{ alignItems: "center" }}
				>
					<ScrollView keyboardShouldPersistTaps="handled">
						<View style={{ justifyContent: 'center' }}>
							<Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 20 }}>{this.props.isRTL ? 'اضف عرض' : 'Add Offer'} </Text>
						</View>

						{
							this.specialOfferAds.map((item, index) => {

								return (
									<View key={index}>
										<View style={[styles.container, { flexDirection: 'row' }]}>
											<View style={{ flex: 1, }}>
												{item.ad_image && item.ad_image.length ?
													<Image style={styles.image}
														source={{ uri: item.ad_image[0].image_name }}
													/>
													: <Image style={styles.image}
														source={require('../icons/blankImage.png')}
													/>

												}
											</View>
											<View style={{ flex: 2, marginLeft: 15, marginRight: 15 }}>
												<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }}>
													<View style={{ flex: 1.2 }}>
														<Text style={[styles.title, { flexWrap: 'wrap' }]}>{this.Capitalize(item.product_name)} </Text>
													</View>

												</View>

												<View style={{}}>
													<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row', marginTop: 5 }}>
														<Text style={[styles.price, { marginTop: 4 }]}>{(parseFloat(item.add_price) * currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
													</View>
													<Text style={{ fontSize: 10 }}>{this.props.isRTL ? 'ادخل نسبة خصم من 0 ألى 99%' : 'Enter Discount 0-99 in %'}</Text>
													<View style={{ flexDirection: 'row' }}>
														<TextInput
															maxLength={2}
															style={{ flex: 1, paddingVertical: Platform.OS === 'ios' ? 5 :0, backgroundColor: '#f5f5f0',fontSize:16 }}
															value={this.state.discount[item.id]}
															keyName={item.id}
															onChangeText={(val) => this.setDiscount(val, index, item.id)}
															placeholder=""
															keyboardType="numeric"
															underlineColorAndroid="transparent"
															defaultValue={item.discount.toString()}
														// onChangeText={(discount) => this.setState({discount}) }
														/><Text style={{fontSize:16}}>%</Text>
													</View>

													{!!this.state.discountedPrice[item.id] &&
														<View style={{ flexDirection: 'row' }}>
															<Text style={{ fontSize: 10, marginTop: 5 }}>{this.props.isRTL ? 'سعر مخفض' : 'Discounted Price'} </Text>
															<Text style={[styles.price, { marginTop: 4 }]}>{(parseFloat(this.state.discountedPrice[item.id]) * currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
														</View>
													}
												</View>

											</View>

										</View>
										<View >

											<TextInput
												style={{ marginHorizontal: 10, marginVertical: 5, paddingVertical: Platform.OS === 'ios' ? 5 : 0, paddingHorizontal: 10, backgroundColor: '#FFF',textAlign :  this.props.isRTL ? 'right': null }}
												underlineColorAndroid="transparent"
												placeholder={this.props.isRTL ? 'وصف العرض ' : "Offer Description"}
												onChangeText={(offerDescription) => this.setOfferDescription(offerDescription, index, item.id)}
												keyName={item.id}
												value={this.state.offerDescription[item.id]}
												maxLength={30}
											/>
										</View>
										<View style={{ borderWidth: 1, marginTop: 5, borderColor: '#cccdce' }} />
									</View>
								)
							})
						}

						<View>
							<Text style={{ color: "black", fontSize: 12 ,textAlign :  this.props.isRTL ? 'right': null}}>	{this.props.isRTL ? ' سيظهر منتجك أو خدمتك في قسم العروض الخاصة' : '1. Your product or service will appear in the special  &emsp;offers section'}</Text>
							<Text style={{ color: "black", fontSize: 12 ,textAlign :  this.props.isRTL ? 'right': null}}>	{this.props.isRTL ? 'سيتم ارسال تنبيهات للمستخدمين المهتمين بنفس الفئة' : '2. Notifications will be sent to users interested in this &emsp; category'}</Text>
							<Text style={{ color: "black", fontSize: 12 ,textAlign :  this.props.isRTL ? 'right': null}}>	{this.props.isRTL ? 'الإعلانات التي عليها عرض خاص ستظهر في اعلى قائمة منتجاتك وخدماتك' : '3. Promoted ads will appear on the top your products &emsp;and services list'}</Text>
							<Text style={{ color: "black", fontSize: 12,textAlign :  this.props.isRTL ? 'right': null }}>	{this.props.isRTL ? 'يتم احتساب الخصم على النقرة في حال قام الزبون بالنقر على اعلانك في قسم العروض الخاصة او على التنبيهات المرسلة لهم فقط ولا يتم احتساب الخصم في حال النقر على اعلانك داخل نقطة بيعك' : '4. Rates will only be applied for clicks in the special &emsp;offer page and notifications page'}</Text>
						</View>
						<View>
					{
							Platform.OS === 'ios' ? 
							<View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
									<TouchableOpacity
								onPress={this.showIosStartDatePicker}
								style={[styles.inputBox, {
									padding: Platform.OS === 'ios' ? 7 : 5,
									borderWidth: 1,
									borderRadius: 25,
									borderColor: '#00aaff',
									paddingHorizontal: 15
								}]}
							>

								<Text style={{ fontSize: 12, fontWeight: 'bold',textAlign :  this.props.isRTL ? 'right': null }}>{this.state.showIosStartDate ? this.state.startDateIOS.toString().substring(0,16) : this.props.isRTL ? 'تاريخ البداية ' : 'Start Date'}</Text>
							</TouchableOpacity>

							<TouchableOpacity
								onPress={this.showIosEndDatePicker}
								style={[styles.inputBox, {
									padding: Platform.OS === 'ios' ? 7 : 5,
									borderWidth: 1,
									borderRadius: 25,
									borderColor: '#00aaff',
									paddingHorizontal: 15
								}]}
							>

								<Text style={{ fontSize: 12, fontWeight: 'bold' ,textAlign :  this.props.isRTL ? 'right': null}}>{this.state.showIosEndDate ? this.state.endDateIOS.toString().substring(0,16) : this.props.isRTL ? 'تاريخ النهاية ' : 'End Date'}</Text>
							</TouchableOpacity>


							</View>


						:

						<View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
							<TouchableOpacity
								onPress={() => this.setState({ isStartDateVisible: true })}
								style={[styles.inputBox, {
									padding: Platform.OS === 'ios' ? 7 : 5,
									borderWidth: 1,
									borderRadius: 25,
									borderColor: '#00aaff',
									paddingHorizontal: 15
								}]}
							>

								<Text style={{ fontSize: 12, fontWeight: 'bold' ,textAlign :  this.props.isRTL ? 'right': null}}>{this.state.startDate ? this.state.startDate : this.props.isRTL ? 'تاريخ البداية ' : 'Start Date'}</Text>
							</TouchableOpacity>
							<DateTimePicker
								format="YYYY/MM/DD"
								isVisible={this.state.isStartDateVisible}
								onConfirm={this.handleStartDate}
								onCancel={() => this.setState({ isStartDateVisible: false })}
								minimumDate={new Date()}
							/>
							<TouchableOpacity
								onPress={() => this.setState({ isEndDateVisible: true })}
								style={[styles.inputBox, {
									padding: Platform.OS === 'ios' ? 7 : 5,
									borderWidth: 1,
									borderRadius: 25,
									borderColor: '#00aaff',
									paddingHorizontal: 15
								}]}
							>

								<Text style={{ fontSize: 12, fontWeight: 'bold' ,textAlign :  this.props.isRTL ? 'right': null}}>{this.state.endDate ? this.state.endDate : this.props.isRTL ? 'تاريخ النهاية ' : 'End Date'}</Text>
							</TouchableOpacity>
							<DateTimePicker
								format="YYYY/MM/DD"
								isVisible={this.state.isEndDateVisible}
								onConfirm={this.handleEndtDate}
								onCancel={() => this.setState({ isEndDateVisible: false })}
								minimumDate={new Date(miniEndDate)}
							/>
						</View>

					}</View>

						<View style={{ marginTop: 10 }}>
							<Text style={{color:'#000000',textAlign :  this.props.isRTL ? 'right': null}}>{this.props.isRTL ? 'التكلفة على النقرة ' : 'Cost Per Click'}: {parseFloat(this.state.clickRate * currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
						</View>

						<View style={{ flexDirection: 'row', justifyContent: 'space-between',width:"90%" }}>
							<Text style={{ marginTop: 15, color: '#000000' ,textAlign :  this.props.isRTL ? 'right': null}}>{this.props.isRTL ? 'نقرات المستخدمين' : 'Users Clicks'}{'(in ' + currencyCode + ')'}</Text>
						
							{ 
							Platform.OS === 'ios' ?
							<View style={{ width:"37%",top:3}}>
							
							<View style={{
														borderWidth:1,
														borderColor:'#00aaff',
														paddingVertical:5,													
														justifyContent:'center', 
														alignItems:'center',
														width:'65%',
														borderRadius:25
													}}>
							
							<TouchableOpacity onPress={this.selectUserClick}>
							<Text style={{fontSize:16,color:'black',textAlign :  this.props.isRTL ? 'right': null}}>{this.state.selectPackgae ? this.state.selectPackgae +' '+ currencyCode :  this.state.isUserClick ? this.props.isRTL ? 'تم' : 'Done': this.props.isRTL ? 'اختار' : 'Select'}</Text>
							</TouchableOpacity>
							</View>
							</View>
							: <View style={{left:5,top:7,borderColor:'#00aaff', borderWidth:1, borderRadius:25}}> 
								<Picker
									selectedValue={this.state.selectPackgae}
									style={{ width: 165 , height: 30,}}
									itemStyle={{margin: 0, fontSize:8,}}
									onValueChange={this.selectPackgaeValue}>
									<Picker.Item label={strings('offer.selectClicks')} value="" />
									{	
										packages.map((pack, index) => <Picker.Item
											key={index}
											label={Math.ceil(pack.package_cost)/specialOfferClickCost + strings('offer.clicks') +' ('+ (parseFloat(pack.package_cost) * currentCurrencyValue).toFixed(2) + ' ' + currencyCode + ')'}
											value={(parseFloat(pack.package_cost) * currentCurrencyValue).toFixed(2)}
										/>)
									}
								</Picker> 
							 </View>
							}
						</View>
						<View style={{top:10,flexDirection:'row-reverse',bottom:10}}>
						
							{ 
								this.state.isUserClick ? 
								<View style={{borderWidth:.5,borderRadius:5, backgroundColor:'white', width:230,padding:0,margin:0}}>
								<Picker
									selectedValue={this.state.selectPackgae}
									style={{ width: 230,height:200,top:-5 }}
									onValueChange={this.selectPackgaeValue}>
									<Picker.Item label={strings('offer.selectClicks')} value="" />
									{	
										packages.map((pack, index) => <Picker.Item
											key={index}
											label={Math.ceil(pack.package_cost)/specialOfferClickCost + strings('offer.clicks')+' ('+ (parseFloat(pack.package_cost) * currentCurrencyValue).toFixed(2) + ' ' + currencyCode + ')'}
											value={(parseFloat(pack.package_cost) * currentCurrencyValue).toFixed(2)}
										/>)
									}
								</Picker>
								</View>
								: 
								null
								
							}
						</View>
						
						<View style={{ flexDirection: 'row', justifyContent: 'flex-end',top:10 }}>

							<Text style={{ fontWeight: 'bold', color: '#000000',textAlign :  this.props.isRTL ? 'right': null}} >{this.props.isRTL ? ' الارصدة المتاحة المعادة ' : 'Available Returned Funds'}: {(parseFloat(this.state.commercialBalance) * currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
						</View>

						{this.state.selectPackgae ?
							<View style={{ flexDirection: 'row', justifyContent: 'flex-end',top:10 }}>
								<Text style={{ flexDirection: 'row', justifyContent: 'flex-end', fontWeight: 'bold', color: '#000000',textAlign :  this.props.isRTL ? 'right': null }}>{this.props.isRTL ? 'المبلغ المطلوب دفعه  ' : 'Payable Amount'} : {this.state.payableAmount +' '+ currencyCode}</Text>
							</View>
							: null
						}
						<View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 15 }}>
							<TouchableOpacity
								style={{
									margin: 5,
									padding: 10,
									paddingHorizontal: 15,
									backgroundColor: '#00aaff',
									borderRadius: 25,
								}}
								onPress={this.handleSubmitOffer}>
							{this.state.isSubloading ? <ActivityIndicator size="small" color="red" /> : <Text style={{ color: '#FFF', fontWeight: 'bold' }}>{this.props.isRTL ? 'ارسل ' : 'Submit'}</Text>}	
								
							</TouchableOpacity>
							<TouchableOpacity
								style={{ margin: 5, padding: 10, paddingHorizontal: 15, backgroundColor: '#e60000', borderRadius: 25, }}
								onPress={() => this.setState({ isSubloading: false, modalOpen: false, discountToggle: false, discount: '', discountPrice: 0, selectPackgae: 0 })}>
								<Text style={{ color: '#FFF', fontWeight: 'bold' ,textAlign :  this.props.isRTL ? 'right': null}}>{this.props.isRTL ? 'الغاء' : 'Cancel'}</Text>
							</TouchableOpacity>
						</View>
					</ScrollView>
				</Modal>

				<ModalDate style={{ borderRadius:20,justifyContent:"center",alignItems:"center",height:"42%",width:"80%" }}
						position={"center"} ref={"modal1"} isDisabled={this.state.isDisabled}>
							<View style={{backgroundColor:"white",marginTop:15,borderRadius:20}}>									
								<DatePickerIOS
									date={new Date(this.state.startDateIOS)}
									onDateChange={this.setStartDateIOS}
									minimumDate = {new Date()}
									mode='date'
								/>
								<TouchableOpacity onPress={this.hideIosStartDatePicekr}> 
									<View style={{justifyContent:"center",alignItems:"center",}}>
									<Text style={{ fontSize:25,fontWeight:"bold",color:"grey",marginHorizontal:"42%",marginBottom:8,marginTop:3,width:50,textAlign :  this.props.isRTL ? 'right': null}}>{this.props.isRTL ? 'حسنا' :'OK'}</Text>
									</View>
								</TouchableOpacity>
							</View>
							
				</ModalDate>

				<ModalDate style={{ borderRadius:20,justifyContent:"center",alignItems:"center",height:"42%",width:"80%" }}
						position={"center"} ref={"modal2"} isDisabled={this.state.isDisabled}>
							<View style={{backgroundColor:"white",marginTop:15,borderRadius:20}}>									
								<DatePickerIOS
									date={new Date(this.state.endDateIOS)}
									onDateChange={this.setEndDateIOS}
									minimumDate = {moment(this.state.startDateIOS, 'YYYY-MM-DD').toDate()}
									mode='date'
								/>
								<TouchableOpacity onPress={this.hideIosEndDatePicekr}> 
									<View style={{justifyContent:"center",alignItems:"center",}}>
									<Text style={{ fontSize:25,fontWeight:"bold",color:"grey",marginHorizontal:"42%",marginBottom:8,marginTop:3,width:50,textAlign :  this.props.isRTL ? 'right': null}}>{this.props.isRTL ? 'حسنا' :'OK'}</Text>
									</View>
								</TouchableOpacity>
							</View>
							
				</ModalDate>

			</View>
		);

	}
}

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL,
		ad_status: state.search.filterData.ad_status,
		country: state.network.countryLocation,
		selectedCat: state.search.selectedCat
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SellerAds)

import React, { Component } from 'react';
import {
	View, 
	Text, 
	StyleSheet, 
	TouchableOpacity, 
	TextInput, 
	ScrollView, 
	Alert,
	AsyncStorage,
	ActivityIndicator,	
} from 'react-native';
import { strings, isRTL } from '../../locales/i18n';
import { connect } from 'react-redux';
import Toast from 'react-native-simple-toast';

import API_URL from '../configuration/configuration';

class ChangePassword extends Component {
	constructor(props) {
		super(props);
		this.state = {
			oldPassword: '',
			newPassword: '',
			rePassword: ''
		}
	}

	static navigationOptions = ({navigation}) => {
		let  state = navigation.state;
		if (state.params !== undefined) {
			return {
		    title: (state.params.isRTL === true) ? 'تغيير كلمة السر' : 'Change Password', 
		    // headerRight: <NotificationHeader navigation={navigation} />
			}
		}
	}

	async componentDidMount() {
		const {setParams} = this.props.navigation;
    	setParams({isRTL: this.props.isRTL});
    	const getUserData = await AsyncStorage.getItem('user_data');
		const userData = JSON.parse(getUserData);
		
		const token = userData.sData;

		const userCredential = JSON.parse(await AsyncStorage.getItem('userCredential'));
		this.setState({ token: token, userPassword:userCredential.password, userCredential: userCredential });
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
    	this.props.navigation.setParams({ isRTL: nextProps.isRTL });
    	this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}

	submit = () => {
		const oldPassword = this.state.oldPassword.trim();
		const newPassword = this.state.newPassword.trim();
		const rePassword = this.state.rePassword.trim();
		const { isRTL } = this.props;
		if(oldPassword !== this.state.userPassword) {			
			const message = isRTL ? 'كود التوثيق المدخل خطأ' : 'Old password not matched'
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			return;
		}
		if(oldPassword && newPassword && rePassword) {
			if(newPassword === rePassword){
				fetch(API_URL+'/change-password', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: this.state.token
			},
			body: JSON.stringify({
				password_old: oldPassword,
				password: newPassword,
				password_confirmation: rePassword,
			})
		})
			.then(response => response.json())
			.then(responseData => {
				 console.warn(responseData);
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 545864
				) {
					
					const message = isRTL ? 'فشل في تحديث كلمة المرور' : 'Failed to update the password';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if(responseData.status === 654864 && responseData.eData.eCode === 236486) {
					Alert.alert('Error', responseData.eData.errors.join(''))
				 } else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					const message = isRTL ? 'تم تغيير كلمة المرور بنجاح' : 'Password changed successfully';
					AsyncStorage.setItem('userCredential',JSON.stringify({ ...this.state.userCredential, password: newPassword }))
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
					this.props.navigation.goBack();
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
			} else {
				
				const message = isRTL ? 'كلمة المرور المعادة غير مطابقة!' : 'Re-password not mached!';
				Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			}
		} else {
			
			const message = isRTL ? 'arb missing' : 'All fields are required!';
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
		}
	}
	render() {
		
		const isRTL = this.props.isRTL;
		const {navigate} = this.props.navigation;
		return (
			<ScrollView style={styles.container} keyboardShouldPersistTaps="always">
					<View style={styles.content}>
						<TextInput 
							secureTextEntry={true}
							placeholder={isRTL? 'أدخل كلمة المرور القديمة' : 'Enter old password'}
							style={[styles.inputBox, isRTL? {textAlign: 'right'}:{}]}
							onChangeText={(text) => this.setState({oldPassword: text})}
							value={this.state.title}
							underlineColorAndroid='white'
						/>
						<TextInput 
							secureTextEntry={true}
							placeholder={isRTL? 'أدخل كلمة المرور الجديدة' :'Enter new password'}
							style={[styles.inputBox, isRTL? {textAlign: 'right'}:{}]}
							onChangeText={(text) => this.setState({newPassword: text})}
							value={this.state.title}
							underlineColorAndroid='white'
						/>
						<TextInput 
							secureTextEntry={true}
							placeholder={isRTL? 'أعد إدخال كلمة المرور الجديدة' :'Re-enter new password'}
							style={[styles.inputBox, isRTL? {textAlign: 'right'}:{}]}
							onChangeText={(text) => this.setState({rePassword: text})}
							value={this.state.title}
							underlineColorAndroid='white'
						/>						
						<TouchableOpacity style={styles.submitBtn} onPress={this.submit}>					
							<Text style={styles.btnText}>{ isRTL? 'أرسل' : 'Submit'}</Text>					
						</TouchableOpacity>
						{/*
						<TouchableOpacity onPress={()=> {navigate('ForgotPassword')}}>
							<Text style={{textDecorationLine: 'underline', color:'#33a5ea',textAlign : this.props.isRTL ? 'right' : 'left'}}>{this.props.isRTL ? 'نسيت كلمة المرور؟' : 'Forgot Password'}</Text>
						</TouchableOpacity>
						*/}
						
					</View>					
				</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1, 
		backgroundColor:'#ecf2f9',
	},
	content: {
		margin:10
	},
	inputBox: {
		backgroundColor: '#FFF',
		borderRadius:25,
		padding:10,
		marginTop:10,		
	},
	submitBtn: {
		margin: 30,
		borderRadius:25,
		backgroundColor: '#33a5ea',
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 15,
	},
	btnText: {
		color: '#FFF',
		fontWeight: 'bold'
	}
});

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL
	};
}

export default connect(mapStateToProps)(ChangePassword);

import React, { Component } from 'react';
//import react in our project
import { Text, View, Image } from 'react-native';
//import all the components we needed
export default class  CustomBlinkingImage extends Component {
  constructor(props) {
    super(props);
    this.state = { showText: true };
    // Change the state every second or the time given by User.
    setInterval(() => {
        this.setState(previousState => {
          return { showText: !previousState.showText };
        });
      },
      // Define blinking time in milliseconds
      1000
    );
  }
  componentWillUnmount(){
    clearInterval();
  }
  render() {
    return (
        <View>
            {this.state.showText?
        <Image	style={{
            marginTop: 5,
            width: 50,
            top: 10,
            height: 50,
            resizeMode: 'contain',
            tintColor:'red'
        }}

        
        source={require('../../images/handIndication.png')}
    />
    :
    null}
    
    </View>
    );
  }
}
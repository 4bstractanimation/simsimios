import React, { Component } from 'react';
import {
	View,
	StatusBar,
	Text,
	Image,
	StyleSheet,
	TouchableOpacity,
	TextInput,
	AsyncStorage
} from 'react-native';
import * as notificationAction from '../actions/notificationAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class NotificationHeader extends Component {
	constructor(props) {
		super(props);
		this.state = {
			token: '',
			notification_count: null
		};
	}
	componentDidMount() {
		AsyncStorage.getItem('user_data').then(json => {
			if (json) {
				const userData = JSON.parse(json);
				this.setState({
					token: userData.sData
				});
				this.props.getData(userData.sData);
			}
		});
	}
	componentWillReceiveProps(nextProps) {
    if (nextProps.check) {
      this.props.getData(this.state.token)
    }
  }

	render() {

		const notificationCount = this.props.notifCount
		return (
			<View style={{ marginHorizontal: 10 }}>
				{this.state.token ? (
					<TouchableOpacity
					
						onPress={() =>							
							this.props.navigation.navigate('Notification')
						}

					
					>
						{notificationCount ? (
									<View style={styles.notification}>
										<Text
											style={{
												marginHorizontal: 0,
												color: 'white',
												fontSize: 10,
												textAlign: 'center'
											}}
										>
											{notificationCount}
										</Text>
									</View>
								) : null}
						<Image
							style={{
								marginTop: notificationCount
									? -10
									: 0,
								width: 28,
								height: 28
							}}
							source={require('../icons/alert3x.png')}
						/>
					</TouchableOpacity>
				) : null}
			</View>
		);
	}
}
const styles = StyleSheet.create({
	notification: {
		borderRadius: 50,
		width: 20,
		height: 20,
		backgroundColor: '#00ADDE',
		marginLeft: 16,
		zIndex: 2
	}
});

function mapStateToProps(state) {
	return {
		notifCount: state.notification.notificationCount,
		check: state.notification.check
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({  ...notificationAction }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationHeader);

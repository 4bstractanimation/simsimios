import React, { Component } from 'react';
import {
	View,
	Text,
	Alert,
	ActivityIndicator,
	ScrollView,
	TouchableOpacity,
	TextInput,
	picker,
	KeyboardAvoidingView,
	StyleSheet,
	Picker,
	TouchableWithoutFeedback,
	Image,
	PixelRatio,
	NativeModules,
	Dimensions,
	AsyncStorage,
	Share,
	Keyboard,
	Platform,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import RNPickerSelect from 'react-native-picker-select';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/AddNewAd';
import SocialSharing from './SocialSharing';
import getCurrencyCode from './Currency.js';
import PreLoader from './PreLoader';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import ImageResizer from 'react-native-image-resizer';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

class AddNewAd extends Component {
	static navigationOptions = ({ navigation }) => {
		let state = navigation.state;
		if (state.params !== undefined) {
			return {
				title:
					state.params.isRTL === true
						? 'إضافة إعلان جديد'
						: 'Add New Ad',
				headerRight: (
					<View style={{ marginHorizontal: 10 }}>
						<TouchableOpacity
							onPress={() => navigation.navigate('Notification')}
						>
							<Image
								style={{ width: 30, height: 30 }}
								source={require('../icons/alert3x.png')}
							/>
						</TouchableOpacity>
					</View>
				)
			};
		}
	};

	constructor(props) {
		super(props);

		this.state = {
			adNo: '',
			adId: null,
			editImg: false,
			status: props.isRTL ? 'جديد' : 'New',
			price: null,
			productName: '',
			categoryList: [],
			category: null,
			subCategoryList: [],
			subCategory: null,
			subSubCategoryList: [],
			subSubCategory: null,
			description: '',
			notes: '',
			photos: [],
			adImg: [],
			pinId: '',
			toggle: false,
			serviceProof: '',
			fileName: '',
			country: '',
			catValue: '',
			isLoading: false,
			catName: '',
			docUri: '',
			docType: '',
			docName: '',
			loader: false,

		};
		this.addImage = this.addImage.bind(this);
		(this.type = ''),
			(this.selectedCategory = this.selectedCategory.bind(this));
		this.selectedSubCategory = this.selectedSubCategory.bind(this);
	}

	async componentDidMount() {
		// console.warn('pin-->>',this.props.navigation.state.params)
		//Auto filled data for Edit Ad
		// if (this.props.navigation.state.params.item) {
		// 	const item = this.props.navigation.state.params.item;

		// 	this.setState({
		// 		adNo: item.ad_number,
		// 		price: item.add_price,
		// 		notes: item.notes,
		// 		description: item.description,
		// 		productName: item.product_name,
		// 		adId: item.id,

		// 	});
		// }
		this.type = this.props.navigation.state.params.type;


		const { setParams } = this.props.navigation;
		setParams({ isRTL: this.props.isRTL });
		const getUserData = await AsyncStorage.getItem('user_data');
		const userData = JSON.parse(getUserData);
		const token = userData.sData;
		this.setState({
			pinId: this.props.navigation.state.params.pinId,
			token: token
		});

		this.getCategory(); // getting main categories

		//For edit Ad data
		const data = this.props.navigation.state.params.item;
		if(this.type !== 'edit') {
			this.getAdNumber(token);
			return;
		}

		let adImage = [];
		let photos = [];
		let img = data.ad_image.map(item => {
			adImage.push(item.image_name);
			photos.push({ path: item.image_name });
		});
		if (data) {
			this.setState({
				adId: data.id,
				adNo: data.ad_number,
				status: data.add_status,
				price: data.add_price,
				productName: data.product_name,
				description: data.description,
				notes: data.notes,
				adImg: adImage,
				photos: photos,
				category: data.category_id,
				subCategory: data.sub_category_id,
				subSubCategory: data.sub_sub_category,
				docUri: data.service_document,
				docName: data.doc_name,

			});
			// console.warn("data services"+JSON.stringify(data));
			
		}
	}

	getCategory() {
		const isRTL = this.props.isRTL;
		fetch(API_URL+'/get-category',
			{
				//Category select from here which is dynamic data
				method: 'GET',
				headers: {
					Accept: 'application/json',

					'Content-Type': 'application/json'
				}
			}
		)
			.then(response => response.json())
			.then(responseData => {
				 // console.warn('categoryList-->>',responseData.categoryList)
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 545864
				) {
					
					const message = isRTL ? 'فشل في الوصول الى الفئات' : 'Failed to get category';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					for (let i = 0; i < responseData.categoryList.length; i++) {
						responseData.categoryList[i].value = responseData.categoryList[i].id;
						responseData.categoryList[i].label = isRTL ? responseData.categoryList[i].arb_name : responseData.categoryList[i].name;
						responseData.categoryList[i].color = '#0a0a0a';
					}
					this.setState({
						categoryList: responseData.categoryList
					});
					 // console.warn('categoryList--->>',responseData.categoryList)
					 const { item } = this.props.navigation.state.params;
					 
					 if(item) {
					 	this.selectedCategory(item.category_id)
					 } else {
					 	this.selectedCategory(responseData.categoryList[0].id, 0)
					 }
					
				}
			})
			.catch(err => {
				Alert.alert('Error',err.message);
			});
	}
	getAdNumber(token) {
		//Auto generated adNumber
		fetch(API_URL+'/auto-generate-adnumber',
			{
				method: 'GET',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					Authorization: token
				}
			}
		)
			.then(response => response.json())
			.then(responseData => {
				if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					this.setState({ adNo: responseData.autoGenerate });
					//setTimeout(() => console.warn(this.state.adNo), 200);
				} else {
					Alert.alert('Please Login');
				}
			})
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
			this.props.navigation.setParams({ isRTL: nextProps.isRTL });
			this.props.navigation.state.params.isRTL = nextProps.isRTL;
		}
	}

	selectedCategory(value, index) {
		// console.warn('selectedCategory-->>', value)
		if(!value) {
			return;
		}
		const category = this.state.categoryList.filter(cat => cat.id == value);
		const { isRTL } = this.props;
		this.setState({
			category: value, catValue: value, catName: category[0].name, subSubCategoryList: [],
			subSubCategory: ''
		});
		const url =	API_URL+'/get-sub-category?cid=' +value;
		
		fetch(url, {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.then(responseData => {
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 545864
				) {
					
					const message = isRTL ? 'فشل في الوصول الى الفئات الفرعية' : 'Failed to get sub-category';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {

					const subcat = responseData.subCategoryList.filter(item => item.name !== "Special Offers");
					for (let i = 0; i < subcat.length; i++) {
						subcat[i].value = subcat[i].id;
						subcat[i].label = isRTL ? subcat[i].arb_name : subcat[i].name;
						subcat.color = '#0a0a0a';
					}
					this.setState({
						subCategoryList: subcat,
					});
					//console.warn('subCategoryList-->>', subcat)
					const { item } = this.props.navigation.state.params;
					if(item) {
						this.selectedSubCategory(item.sub_category_id)
					} else{
						this.selectedSubCategory(subcat[0].id)
					}
					
				}
			})
			.catch(err => {
				console.warn(err);
			});
	}

	selectedSubCategory(value) {
		if(!value) {
			return;
		}
		this.setState({ subCategory: value });
		const url = API_URL+'/get-sub-sub-category?scid=' +	value;
			const { isRTL } = this.props;
		fetch(url, {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.then(responseData => {
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 545864
				) {
					
					const message = isRTL ? 'فشل في الوصول الى الفئات الفرعية' : 'Failed to get sub-sub-category';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {

					const subSubCat = responseData.subSubcategoryList.filter(item => item.name !== "Special Offers");

					for (let i = 0; i < subSubCat.length; i++) {
						subSubCat[i].value = subSubCat[i].id;
						subSubCat[i].label = isRTL ? subSubCat[i].arb_name : subSubCat[i].name;
						subSubCat.color = '#0a0a0a';
					}
					if (subSubCat.length !== 0) {
						const { item } = this.props.navigation.state.params;

						if (Platform.OS === 'android') {
							this.setState({
								subSubCategoryList: subSubCat,
								subSubCategory: item ? item.sub_sub_category : subSubCat[0].name
							})
						} else {
							this.setState({
								subSubCategoryList: subSubCat,
								subSubCategory: item ? item.sub_sub_category : subSubCat[0].id
							})
						}
					}
					// if(subSubCat.length !== 0){
					// 	this.setState({
					// 		subSubCategoryList: subSubCat,
					// 		subSubCategory:subSubCat[0].name
					// 	});
					// }					

					//console.warn('subSubcategoryList-->>',responseData.subSubcategoryList)
					//setTimeout(() => {console.warn(this.state.category);}, 300);
				}
			})
			.catch(err => {
				// console.warn(err);
			});
	}

	shareOnSocialMedia = () => {
		let shareOptions = {
			title: 'title here',
			message: 'message here',
			url: 'http://facebook.github.io/react-native/',
			subject: 'This is news data' // for email
		};
		Share.share(shareOptions);
	};

	_handleSubmit() {

		Keyboard.dismiss();
		const { isRTL } = this.props;
		const token = this.state.token;
		const { navigate } = this.props.navigation;
		const { currencyCode } = this.props.country;
		// const currencyCode = 'SAR';
		
		if(!this.state.price) {
			const message = isRTL ? 'يرجى ادخال السعر ' : 'Please enter the price';
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);

			return;
		}
		 this.setState({ isLoading: true })

		let url = '';
		let data = {};
		// console.warn('this.state.adImg ', this.state.adImg);
		if (this.type === 'edit') {
			url = API_URL+'/editAds';
			data = {
				ad_id: this.state.adId,
				ad_image: this.state.editImg ? this.state.adImg : [],
				product_name: this.state.productName,
				category_id: this.state.category,
				sub_category_id: this.state.subCategory,
				sub_sub_category: this.state.subSubCategory,
				ad_status: this.state.catName.toLowerCase() === "services" ? "" : this.state.status,
				price: this.state.price,
				notes: this.state.notesres,
				description: this.state.description,
				currencyCode: currencyCode,
				docUri: this.state.docUri,
				docName : this.state.docName,
			};
		} else {
			url = API_URL+'/create-ads';
			data = {
				adNumber: this.state.adNo,
				ad_image: this.state.adImg,
				pin_id: this.state.pinId,
				product_name: this.state.productName,
				category_id: this.state.category,
				sub_category_id: this.state.subCategory,
				sub_sub_category: this.state.subSubCategory,
				ad_status: this.state.catName.toLowerCase() === "services" ? "" : this.state.status,
				price: this.state.price,
				notes: this.state.notes,
				description: this.state.description,
				currencyCode: currencyCode,
				docUri: this.state.docUri,
				docName : this.state.docName,
				docType: this.state.docType,
			};
		}
		 // console.warn(data);
		 // return;
		fetch(url, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: token
			},

			body: JSON.stringify(data)
		})
			.then(response => response.json())
			.then(responseData => {
					//console.warn('ad response-->>', responseData)
				if (
					responseData.status == 502368 &&
					responseData.eData.eCode == 684605
				) {
					this.setState({ isLoading: false })
					Alert.alert('', responseData.eData.errors.join());
				} else if (
					responseData.status == 805465 &&
					responseData.eData === null
				) {
					if(this.type === 'edit') {
						const message = isRTL ? 'تم تحديث الإعلان بنجاح' : 'Ad updated successfully';
						Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);

					} else {
						const message = isRTL ? 'تم نشر الإعلان بنجاح ' : 'Ad submitted successfully';
						Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
					}
					
					
					this.getAdNumber(token);

					this.setState({
						status: '',
						price: null,
						productName: '',
						category: null,
						subCategory: null,
						subSubCategory: null,
						notes: '',
						description: '',
						isLoading: false,
						photos: [],
						adImg: []
					});
					this.props.navigation.goBack();	
				}
			})
			.catch(err => {
				this.setState({ isLoading: false })
				Alert.alert('Error',err.message);
			});
	}



	addImage() {
		ImagePicker.openPicker({
			multiple: true,
			includeBase64: true,
			compressImageQuality: 0.2
		})
			.then(response => {
				 response.splice(3)
				const images = [];
					response.map((img, i) => {
							const imagePath = img.data;
							//console.warn('TCL: imagePath -> r3  ',imagePath)
							images.push(imagePath);
					
				});
				this.setState({ photos: response, adImg: images, editImg: true });
			})
			.catch(e => alert(e));
	}


	// addImage() {
	// 	ImagePicker.openPicker({
	// 		multiple: true,
	// 		includeBase64: true
	// 	})
	// 		.then(r => {
	// 			// console.warn('TCL: addImage -> r 1 ')
	// 			const images = [];
	// 				r.map((img, i) => {
	// 					// console.warn('TCL: addImage -> r  2')
	// 						// this.setState({ adImg: [img.data], editImg:true });
	// 						const imagePath = img.path;
	// 						console.warn('TCL: imagePath -> r3  ',imagePath)
	// 						ImageResizer.createResizedImage(imagePath, 600, 400, 'JPEG', 50, 0)
	// 							.then( async (response) => {	
	// 							console.log('TCL: addImage -> response', response)
	// 						// let base64Image = await this.convertIntoBase64(response.uri);	
	// 						// console.warn('base64Image-->>', base64Image)				
	// 						// images.push(base64Image)
	// 						this.setState({adImg: images})

	// 					})
					
	// 			});

	// 			this.setState({ photos: r, adImg: images, editImg: true });
	// 			//setTimeout(() => {console.warn("data", this.state.photos[0].data);}, 300);
	// 		})
	// 		.catch(e => alert(e));
	// }

	// async convertIntoBase64(fileUri) {

	// 	console.warn('TCL: addImage -> r6  ')

	// 	if (Platform.OS === 'ios') {
	// 		let arr = fileUri.split('/')
	// 		const dirs = RNFetchBlob.fs.dirs
	// 		filePath = `${dirs.DocumentDir}/${arr[arr.length - 1]}`
  //           console.warn('TCL: filePath', filePath)
	// 	  } else {
	// 		filePath = fileUri
	// 	  }

	// 	return RNFetchBlob.fs.readFile(filePath, 'base64')
	// 		.then( (data) => {
	// 			console.warn('TCL: data  ', data);
	// 			return data;
				
	// 		})
	// }

	// addImage() {
	// 	ImagePicker.openCamera({
	// 		mediaType:'photo',	  
	// 	  	compressImageMaxWidth:400,
	// 	  	compressImageMaxHeight:300,
	// 	  	compressImageQuality:0.4,
	// 	  	includeBase64: true
	// 	}).then(img => {
	// 		this.setState({photos: img, adImg: [...this.state.adImg, img.data], editImg:true})

	// 	}).catch(e => alert(e));
	// }

	toggleVisible = () => {
		this.setState(prevState => {
			return { toggle: !prevState.toggle };
		});
	};

	// uploadImage() {
	// 	ImagePicker.openPicker({
	// 		includeBase64: true
	// 	})
	// 		.then(item => {
	// 			const str = item.path;
	// 			var filename = str.replace(/^.*[\\\/]/, '');
	// 			this.setState({
	// 				serviceProof: item.data,
	// 				fileName: filename
	// 			});
	// 		})
	// 		.catch(e => alert(e));
	// }

	 docHandle() {
    
		this.setState({loader:true});
		DocumentPicker.show({
			filetype: [DocumentPickerUtil.allFiles()],
		}, async (error, res) => {
			if (res !== null) {
				let docUri = await this.docUpload(res.uri);
				console.warn('TCL: docHandle -> docUri', docUri)
				this.setState({ docUri: docUri, docType: res.type, docName: res.fileName, loader:false });	
			} 
			else {
				this.setState({loader:false});
				console.warn("fail");
			}

		});
	}


	 async docUpload(fileUri) {
		if (Platform.OS === 'ios') {
			path = fileUri.replace('file:', '');
			// let arr = path.split('/')
			// const dirs = RNFetchBlob.fs.dirs
			// filePath = `${dirs.DocumentDir}/${arr[arr.length - 1]}`
			return RNFetchBlob.fs.readFile(path, 'base64')
			.then((data) => {
				return data;	
			})
		  } else {
			filePath = fileUri;
			return RNFetchBlob.fs.readFile(filePath, 'base64')
			.then((data) => {
				return data;
			})
		  }		
	}

	priceHandler = text => {

		if(isNaN(text)) {
			return;
		}

		this.setState({ price: text })
	}
								

render() {
	// console.warn('pinType-->>',this.props.navigation.state.params.pinType)
	const { currencyCode } = this.props.country;
	const styles = StyleSheetFactory.getSheet(this.props.isRTL);
	const blankImage = 3;
	const blankAvatar = [];
	const pinType = this.props.navigation.state.params.pinType || this.props.navigation.state.params.item.pin_type || this.props.navigation.state.params.pinDetails.pin_type;
	// console.warn('TCL: render -> pinType ', pinType);
	// console.warn('TCL: this.state.catName', this.state.catName)
	const isRTL = this.props.isRTL;
	let status = this.props.isRTL ? [{ label: "جديد", value: "جديد" },
	{ label: "مستعمل", value: "مستعمل" }] : [{ label: "New", value: "New" },
	{ label: "Used", value: "Used" }];
	if (this.state.photos.length > 0) {
		this.state.photos.map((img, i) => {
			blankAvatar.push(
				<Image
					key={i}
					source={{ uri: img.path }}
					style={styles.image}
				/>
			);
		});
	} else {
		for (let i = 0; i < blankImage; i++) {
			blankAvatar.push(<View key={i} style={styles.blankSquare} />);
		}
	}
	return (
		<KeyboardAvoidingView   behavior={Platform.OS === 'ios' ? "position" : null}>
			<ScrollView
				style={{
					backgroundColor: '#ecf2f9',
					paddingHorizontal: 10
				}}
				keyboardShouldPersistTaps={Platform.OS === 'ios' ? "never" : "always"}
			>
				<View style={[styles.imgContainer]}>{blankAvatar}</View>
				<View style={styles.adnum_imgContainer}>
					<View
						style={[
							styles.adnumberContainer,
							{ flexDirection: 'row' }
						]}
					>
						<Text
							style={{
								marginTop: 15,
								fontWeight: 'bold',
								marginLeft: 10
							}}
						>
							Ad Number:{' '}
						</Text>
						<TextInput
							style={[
								styles.adnum,
								{ flex: 1, alignItems: 'flex-end' }
							]}
							value={this.state.adNo}
							underlineColorAndroid="white"
							keyboardType={'default'}
							placeholderTextColor="#7A7A7A"
							placeholder="4521346"
							editable={false}
						/>
					</View>
					<TouchableOpacity
						style={[styles.adnumberContainer, styles.addimg]}
						onPress={this.addImage}
					>
						<Text style={styles._buttonText}>
							{strings('adNewAd.add_image')}
						</Text>
					</TouchableOpacity>
				</View>

				<View style={[styles.status_priceContainer, { flex: 1,}]}>

					{this.state.catName !== "Services" ?
						<View style={[styles.serviceContainer, { flexDirection: this.props.isRTL ? 'row-reverse' : 'row' }]}>
							<View style={isRTL ? styles.statusContainerRev : styles.statusContainer}>


								<Text style={[styles.status, { flex: 0.8 }]}>
									{strings('adNewAd.status')}
								</Text>
							</View>
							<View style={isRTL ? styles.chooseRev : styles.choose}>
								{Platform.OS === 'android' ? <Picker
									style={{ flex: 0.9, color: 'grey' }}
									selectedValue={this.state.status}
									mode="dropdown"
									onValueChange={(itemValue, itemIndex) =>
										this.setState({ status: itemValue })
									}
								>
									{/* <Picker.Item label="Choose" value="" /> */}
									<Picker.Item label={isRTL ? 'جديد' : "New"} value={isRTL ? 'جديد' : "New"} />
									<Picker.Item label={isRTL ? 'مستعمل' : "Used"} value={isRTL ? 'مستعمل' : "Used"} />
								</Picker> :
									<View style={{ left: 20, top: 15 }}>
										<RNPickerSelect
											placeholder={{
												label: 'Product type',
												value: null //this.state.status
											}}
											value={
												this.state.status
											}
											onValueChange={(itemValue, index) =>
												this.setState({ status: itemValue },() => console.warn(this.state.status))
											}
											style={{
												placeholderColor: 'black',
												inputIOS : {
													color:'grey',
													placeholderColor:'grey',
													fontWeight:'bold',
												}
											}}
											hideIcon
											items={status}
										/>
									</View>
								}
							</View>
						</View>
						: null
					}
					<View
						style={[
							styles.priceContainer,
							{
								flex: 0.45,
								flexDirection: isRTL ? 'row-reverse' : 'row',
								height:50,
								justifyContent: 'space-around',
								alignItems: 'center'
							}
						]}
					>
						<TextInput
							style={[styles.adnum, { flex: 0.4}]}
							value={this.state.price}
							keyboardType='numeric'
							onChangeText={this.priceHandler}
							placeholder={isRTL ? "سعر" : "Price "}
							value={this.state.price}
							underlineColorAndroid="white"
							placeholderTextColor="#7A7A7A"
						/>
						<Text style={{flex:0.4, fontWeight: 'bold' }}>{currencyCode}</Text>
					</View>
				</View>

						


				<View style={[styles.adnumberContainer, { justifyContent: 'center' }, styles.product]}>
					<TextInput
						style={styles.adnum}
						value={this.state.productName}
						onChangeText={text =>
							this.setState({ productName: text })
						}
						placeholder={strings('adNewAd.product')}
						value={this.state.productName}
						underlineColorAndroid="white"
						keyboardType={'default'}
						placeholderTextColor="#7A7A7A"
					/>
				</View>

				<View style={[styles.adnumberContainer, styles.product]}>
					{Platform.OS === 'android' ?
						<Picker
							style={{
								flex: 1,
								marginHorizontal: 10,
								color: 'grey'
							}}
							selectedValue={this.state.catValue}
							onValueChange={(itemValue, itemPosition) => {
								this.selectedCategory(itemValue, itemPosition)
								}
							}

						>

							{this.state.categoryList.map((item, key) => (
								<Picker.Item
									key={key}
									label={isRTL ? item.arb_name : item.name}
									value={item.id}
								/>
							))}
						</Picker> :
						<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop:18, marginHorizontal:20 }}>
							<RNPickerSelect
								placeholder={{
									label: 'Select your Category',
									value: null
								}}
								value={
									this.state.catValue
								}
								onValueChange={(itemValue, itemPosition) => {
								this.selectedCategory(itemValue, itemPosition)
								}
								}
								style={{
									placeholderColor: '#7A7A7A',
									inputIOS : {
										color:'grey',
										placeholderColor:'grey',
										fontWeight:'bold',
									}
								}}
								hideIcon
								items={this.state.categoryList}
							/>
						</View>
					}
				</View>
				{/* {this.state.catValue == "service" && pinType == "Individual" ? (
					<View
						style={[styles.adnumberContainer, styles.product]}
					>
						<TouchableOpacity
							onPress={this.uploadImage.bind(this)}
							style={{
								flexDirection: this.props.isRTL
									? 'row-reverse'
									: 'row',
								justifyContent: 'space-between'
							}}
						>
							{this.state.fileName ? (
								<Text style={styles.adnum}>
									{this.state.fileName}
								</Text>
							) : (
									<Text
										style={{
											marginVertical: 15,
											marginHorizontal: 15
										}}
									>
										Upload Service Proof
									</Text>
								)}
							<Image
								style={{
									marginRight: 15,
									marginVertical: 15,
									width: 23,
									height: 20
								}}
								source={require('./../icons/upload2x.png')}
							/>
						</TouchableOpacity>

					</View>

				) : null} */}
				<View
					style={{
						flexDirection: this.props.isRTL
							? 'row-reverse'
							: 'row',
						justifyContent: 'space-between'
					}}
				>
					<View style={[styles.adnumberContainer, styles.subCat]}>
						{Platform.OS === 'android' ?
							<Picker
								style={{ marginHorizontal: 10, color: 'grey' }}
								selectedValue={this.state.subCategory}
								onValueChange={(itemValue, itemIndex) =>
									this.selectedSubCategory(itemValue)
								}
							>

								{this.state.subCategoryList.length ?
									this.state.subCategoryList.map((item, key) => (
										<Picker.Item
											key={key}
											label={isRTL && item.arb_name ? item.arb_name : item.name}
											value={item.id}
										/>
									)) : <Picker.Item label="No Sub Category" />
								}
							</Picker> :
							<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop:18, marginHorizontal:20 }}>
								{this.state.subCategoryList.length ?
									<RNPickerSelect
										placeholder={{
											label: 'Select Sub Category',
											value: null
										}}
										value={
											this.state.subCategory
										}
										onValueChange={(itemValue, index) =>
											this.selectedSubCategory(itemValue)
										}
										style={{
											placeholderColor: '#7A7A7A',
											inputIOS : {
										color:'grey',
										placeholderColor:'grey',
										fontWeight:'bold',
									}
										}}
										hideIcon
										items={this.state.subCategoryList}
									/> :
									<Text> No Data </Text>}
							</View>
						}
					</View>
					<View style={[styles.adnumberContainer, styles.subCat]}>
						{Platform.OS === 'android' ?
							<Picker
								style={{ marginHorizontal: 10, color: 'grey' }}
								selectedValue={this.state.subSubCategory}
								onValueChange={(itemValue, itemIndex) =>
									this.setState({ subSubCategory: itemValue })
								}
							>

								{this.state.subSubCategoryList.length ? this.state.subSubCategoryList.map(
									(item, key) => (
										<Picker.Item
											key={key}
											label={isRTL ? item.arb_name : item.name}
											value={item.id}
										/>
									)
								) : <Picker.Item label="No Sub Sub Category" />

								}
							</Picker> :
							<View style={{ flexDirection: isRTL ? 'row-reverse' : 'row', marginTop:18, marginHorizontal:20 }}>
								{this.state.subSubCategoryList.length ?
									<RNPickerSelect
										placeholder={{
											label: 'Select your Category',
											value: null
										}}
										value={
											this.state.subSubCategory
										}
										onValueChange={(itemValue, index) =>
											this.setState({ subSubCategory: itemValue })
										}
										style={{
											placeholderColor: '#7A7A7A',
											inputIOS : {
										color:'grey',
										placeholderColor:'grey',
										fontWeight:'bold',
									}
										}}
										hideIcon
										items={this.state.subSubCategoryList}
									/> :
									<Text> No Data </Text>}
							</View>
						}
					</View>
				</View>

				{/********* for DocPicker *************/}
				{ this.state.catName === "Services" && pinType.toLowerCase() === "individual"  ?
					<View>
						<View style={styles.chooseDOC}>
							<TouchableOpacity onPress={this.docHandle.bind(this)}>
								<Text>{this.state.docName ? this.state.docName : isRTL ? "دليل الممارسة" : "Proof of Practice"}</Text>
							</TouchableOpacity>
							{this.state.loader ? <ActivityIndicator size="small" color="grey" /> : null}
						</View>
						{/*<Text> {isRTL ? "يجب أن يكون حجم الملف أقل من 500 كيلوبايت" : " File size should be under 500 KB"} </Text> */}
					</View>
					: null
				}
				<View style={styles._descriptionbox}>
					<TextInput
						style={styles.adnum}
						value={this.state.description}
						onChangeText={text =>
							this.setState({ description: text })
						}
						multiline={true}
						underlineColorAndroid="white"
						textAlignVertical="top"
						numberOfLines={4}
						keyboardType={'default'}
						placeholderTextColor="#7A7A7A"
						value={this.state.description}
						placeholder={strings('adNewAd.description')}
						blurOnSubmit={true}
						onSubmitEditing={()=>{Keyboard.dismiss()}}
						returnKeyType="done"
					/>
				</View>
				<View style={[styles.adnumberContainer, { justifyContent: 'center' }, styles.product]}>
					<TextInput
						style={styles.adnum}
						value={this.state.notes}
						onChangeText={text =>
							this.setState({ notes: text })
						}
						placeholder={strings('adNewAd.notes')}
						value={this.state.notes}
						underlineColorAndroid="white"
						keyboardType={'default'}
						placeholderTextColor="#7A7A7A"
					/>
				</View>
				{this.state.isLoading ?
					<PreLoader /> :
					//<ActivityIndicator size="small" color="#ffffff" /> :
					<TouchableOpacity
						style={[styles.adnumberContainer, styles.submit]}
						onPress={this._handleSubmit.bind(this)}
					>
						<Text style={styles._buttonText}>
							{strings('adNewAd.submit')}
						</Text>
					</TouchableOpacity>
				}
			</ScrollView>
			<View style={{ zIndex: 0 }}>
				<SocialSharing
					toggle={this.state.toggle}
					toggleVisible={this.toggleVisible}
				/>
			</View>
		</KeyboardAvoidingView>
	);
}
}

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL,
		country: state.network.countryLocation
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(AddNewAd);

import React, { Component } from 'react';
import { View, Text,Alert, ListView,Button, Image, StyleSheet, TouchableOpacity, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import API_URL from '../configuration/configuration';

class SellerAdsTab extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			achtiveTab: false,
			pinId: '',
			loginUserId: '',
			pinUserId: '',
			token: '',
			usedAds : '',
			totalAds : '',
		}
	}

	componentDidMount(){
		AsyncStorage.getItem('user_data').then(json => {
	    const userData = JSON.parse(json);
	    if(userData) {
	    	this.setState({
	    	token: userData.sData, 
	    	loginUserId: userData.userId,
	    	pinUserId: this.props.navigation.state.params.item.user_id,
	    	pinId: this.props.navigation.state.params.item.id,
	    	pinType: this.props.navigation.state.params.item.pin_type
	    })
			this.checkNoOfAds(userData.sData);
		
			this.props.navigation.addListener('didFocus', async event => {
				this.checkNoOfAds(userData.sData);
			});
		}
		});
	}

	checkNoOfAds = (token) => {
		const url = API_URL+'/check-no-of-ads';
		const pinId = this.state.pinId;
	 	
		fetch(url, {
	      method: 'POST',
	      headers: {
	        Accept: 'application/json',
	        'Content-Type': 'application/json',
	        Authorization: token,
				},
				body: JSON.stringify({pin_id : pinId}),
	    })
	      .then(response => response.json())
	      .then( responseData => {
					console.warn('check-no-of-ads-->> ',responseData); 
			 	if(responseData.status == 200 ) {
	          		this.setState({usedAds : responseData.data.created_ads, totalAds :responseData.data.allowed_ads });         
	        	}
	      })
	      .catch(err => {        
	       Alert.alert('Error', err.message)
	      });

	}


createNewAd = () => {

	// console.warn("TCL: SellerAdsTab -> createNewAd -> usedAds", this.state.usedAds,this.state.totalAds)
	const pinId = this.state.pinId;
	const pinType = this.state.pinType;
	if(this.state.usedAds < this.state.totalAds){
    
		this.props.navigation.navigate('AddNewAd', {pinId,pinType})
	}else{
		const msg = this.props.isRTL ? 'تم الوصول إلى الحد الأقصى للإعلان' : 'You reach maximum number of ads in this sales point, upgrade your account to add more ads'
		const msg2 = this.props.isRTL ? 'قم بترقية حسابك' : 'Upgrade your account';
		const noText  = this.props.isRTL ? 'لا' :  'No';
		Alert.alert('',msg,
		[
			{ text : msg2, onPress: () => this.props.navigation.replace('Subscription')},
			{ text : noText, }
		],
		{cancelable:false}
		);
		return;
	}
}

	render() {
		// console.warn(this.props.isRTL)
		const isRTL =this.props.isRTL
		const pinId = this.state.pinId;
		const pinType = this.state.pinType;
		const { navigate } = this.props.navigation;
		const tabIndex = this.props.tabIndex;
		return (
			<View style={[styles.tabs, {flexDirection: isRTL ? 'row-reverse' : 'row', }]}>
				<View style={{flex:2.1}}>
					<TouchableOpacity 
						style={tabIndex===1 ? styles.activeTabBox : styles.tabBox} 
						onPress={() => {this.props.setTabIndex(1)}}
						>
						<Text style={tabIndex===1 ? styles.acttiveText : styles.text}>{ isRTL? "إعلانات" : "Ads"}</Text>
					</TouchableOpacity>
				</View>
				<View style={{flex:2.5,}}>
					<TouchableOpacity 
						style={ tabIndex===2 ? [styles.activeTabBox, { marginLeft:5 }] : [styles.tabBox, { marginLeft:5 }]}
						onPress={() => {this.props.setTabIndex(2)}}
					>
						<Text style={tabIndex===2 ? styles.acttiveText : styles.text}>{ isRTL ? "البائع" : "Overview"}</Text>
					</TouchableOpacity>
				</View>
				<View style={{flex:2.5,}}>
					<TouchableOpacity 
						style={tabIndex===3 ? [styles.activeTabBox, { marginLeft:5 }] : [styles.tabBox, { marginLeft:5 }]}
						onPress={() => {this.props.setTabIndex(3)}}
					>
						<Text style={tabIndex===3 ? styles.acttiveText : styles.text}>{isRTL ? "اخبار" :"News Feed"}</Text>
					</TouchableOpacity>
				</View>
				
				{ this.state.loginUserId == this.state.pinUserId && this.state.token ? <View style={{flex:3}}>
						<TouchableOpacity style={[styles.addTabBox, {display: tabIndex==2 || tabIndex==3 ? 'none' : "flex"}, {marginLeft: 5}]}								
							onPress={this.createNewAd}
						>	
							<Image style={{width:12, height:12, resizeMode: 'contain', marginHorizontal:4}} 
									source={require('../icons/add3x.png')}
							/>							
							<Text style={styles.addText}>
								{isRTL? 'ضف اعلان' : "New Ad"}
							</Text>
						</TouchableOpacity>
	
						{ tabIndex ==3 &&
						<TouchableOpacity style={[styles.addTabBox, {marginLeft: 5}]}								
							onPress={() => {navigate('AddNewNews', {pinId, loginUserId: this.state.loginUserId})}}
						>			
						<Image style={{width:12, height:12, marginHorizontal:3}} 
									source={require('../icons/add3x.png')}
								/>					
							<Text style={styles.addText}>
								{isRTL? 'ضف اخبار' : 'Add News'}
							</Text>
						</TouchableOpacity>
					}
					</View>	
					: null					
				}	
						
			</View>

		);
	}
} 

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL
  };
}

export default connect(mapStateToProps)(SellerAdsTab);

const styles = StyleSheet.create({
	tabs: {
		
		marginHorizontal:15,
		 alignItems: 'center',
		marginTop: 10,
 	},
 	tabBox: {
 		borderRadius:25,
	    backgroundColor: '#FFF',
	    paddingVertical:10,
	    alignItems: 'center',
 	},
 	addTabBox: {
 		borderRadius:25,
	    backgroundColor: '#FFF',
	    padding: 5,
	    alignItems: 'center',
	    backgroundColor: '#4dc3ff',
	    flexDirection: 'row',
	    flex: 3
 	},
 	addText: {
 		fontWeight: 'bold',
 		color: '#ffffff',
 		textAlign : 'center'
 	},
 	text: {
 		fontWeight: 'bold',
 		color: '#000000',
 	},
 	activeTabBox: {
	 	borderRadius:25,
	    backgroundColor: '#000000',
	    paddingVertical:10,
	    alignItems: 'center',
 	},
 	acttiveText: {
 		fontWeight: 'bold',
 		color: '#ffffff',
 	}
})

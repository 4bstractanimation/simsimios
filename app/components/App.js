import React from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	StyleSheet,
	AsyncStorage,
	Platform
} from 'react-native';

import Navigation from './Navigation-Drawer';

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedValue: ''
		};
	}

	render() {
		return null;
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'space-between',
		padding: 2,
		backgroundColor: '#F1F6F7'
	}
});

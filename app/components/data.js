
const LATITUDE =  28.68519;
const LONGITUDE = 77.08521;
const SPACE = 0.01;
const data= [
  { "id":1,
    "isfavourite": true,
    "subscription":"Free",
    "title": "Car / Toyota / Corolla",
    "description": "Lorem ipsum dolor sit amet, indu consectetur adipiscing elit",
    "image": "../images/toyota.png",
    "coordinates": {
       "latitude": LATITUDE + SPACE,
       "longitude": LONGITUDE + SPACE 
     },
    "rating": "3.4",
    "sellerType":'Individual',
    "delivery":'yes'
  },
  { "id":2,
    "isfavourite": false,
    "subscription":"Free",
    "title": "Car / Toyota / Corolla",
    "description": "Lorem ipsum dolor sit amet, indu consectetur adipiscing elit",
    "image": './../../images/toyota.png',
    "coordinates": {
       "latitude": LATITUDE + SPACE + SPACE,
       "longitude": LONGITUDE + SPACE + SPACE
     },
    "rating": "3.4",
    "sellerType":'Enterprise',
    "delivery":'yes'
  },
  { "id":3,
    "isfavourite": false,
    "subscription":"Platinum",
    "title": "Car / Toyota / Corolla",
    "description": "Lorem ipsum dolor sit amet, indu consectetur adipiscing elit",
    "image": "../images/toyota.png",
    "coordinates": {
       "latitude": LATITUDE + SPACE,
       "longitude": LONGITUDE,
     },
    "rating": "3.4",
    "sellerType":'Enterprise',
    "delivery":'yes'
  },
  { "id":4,
    "isfavourite": false,
    "subscription":"Free",
    "title": "Car / Toyota / Corolla",
    "description": "Lorem ipsum dolor sit amet, indu consectetur adipiscing elit",
    "image": "../images/toyota.png",
    "coordinates": {
       "latitude": LATITUDE + SPACE,
       "longitude": LONGITUDE - SPACE,
     },
    "rating": "3.4",
    "sellerType":'Individual',
    "delivery":'no'
  },
  { "id":5,
    "isfavourite": true,
    "subscription":"Free",
    "title": "Car / Toyota / Corolla",
    "description": "Lorem ipsum dolor sit amet, indu consectetur adipiscing elit",
    "image": "../images/toyota.png",
    "coordinates": {
       "latitude": LATITUDE + SPACE + SPACE,
       "longitude": LONGITUDE + SPACE,
     },
    "rating": "3.4",
    "sellerType":'Individual',
    "delivery":'yes'
  },
  { "id":6,
    "isfavourite": true,
    "subscription":"Free",
    "title": "Car / Toyota / Corolla",
    "description": "Lorem ipsum dolor sit amet, indu consectetur adipiscing elit",
    "image": "../images/toyota.png",
    "coordinates": {
       "latitude":LATITUDE - SPACE ,
       "longitude": LONGITUDE 
     },
    "rating": "3.4",
    "sellerType":'Individual',
    "delivery":'no'
  },
 
]

export default data;

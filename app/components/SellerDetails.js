import React, { Component } from 'react';
import { Rating, AirbnbRating } from 'react-native-ratings';
import StarRating from 'react-native-star-rating';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
	View,
	Text,
	Image,
	ScrollView,
	StyleSheet,
	TouchableHighlight,
	TouchableOpacity,
	AsyncStorage
} from 'react-native';
import SellerAdsTab from './SellerAdsTab';
import Footer from './Footer';
import MapView, { Marker, Callout, ProviderPropType } from 'react-native-maps';
import PreLoader from './PreLoader';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/SellerDetails';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

class SellerDetails extends Component {
	constructor() {
		super();
		this.state = {
			data: '',
			userId: null,
			token: '',
			adminsetcount:0,
			PinRating:[]
		};
	}

	componentDidMount() {
		AsyncStorage.getItem('user_data').then(json => {
			if (json) {
				const userData = JSON.parse(json);
				this.setState({
					token: userData.sData,
					userId: userData.userId
				});
			}
		});
		console.warn(this.props.navigation.state.params.item);
		const data = this.props.navigation.state.params.item;
		this.getPinDetails(data);
	}

	getPinDetails(item) { fetch(API_URL+'/pinDetails',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            pinId: item.id,
          })
        }
      )
        .then(response => response.json())
        .then(async responseData => {
        	// console.warn('Pin details-->>', responseData)
          this.setState({
          	data: responseData.pinByid, 
          	adminsetcount: responseData.adminsetcount,
          	PinRating: responseData.PinRating,
          	membership:responseData.membership_status
          })
        })
        .catch(err => {
           console.log(err);
        });
	}

	// getWeekDays(weekend) {

	// 	const weekDays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
	// 	weekend = weekend ? weekend.split(',') : [];
	// 	// console.warn(weekend)
	// 	const days = weekDays.filter(day => !weekend.includes(day));
	// 	return days.toString();
	// }

	Capitalize(str) {
		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	changePlatinumToGold(planType) {
		const isRTL = this.props.isRTL;
		
		if(!planType) {
			return;
		}

	    if(planType.toLowerCase() === 'platinum') {
	      return isRTL ? 'ذهبي' : 'Golden';
	    } else if(planType.toLowerCase() === 'premium'){
	    	return isRTL ? 'تميز' : 'Premium';
	    }

	    return planType;
	}


	getArabicText(pinType) {
		//Here pinType or sellerType both are same.
		if(!pinType) {
			return;
		}

		if(pinType.toLowerCase() === 'enterprise') {
			return 'منشأة';
		} else {
			return 'فردي';
		}
	}

	render() {
		// console.warn('sellerDetail-->>',this.state.membership);
		let membership = this.state.membership;
		
		if(membership) {
			membership = membership.toLowerCase() === 'free' ? '' : membership;
		}

		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		const data = this.state.data;
		let count = '';
		const adminsetcount = this.state.adminsetcount;
		// console.warn('visitor--->>', data.visitor)
		const isVisitShow = data.visitor >= adminsetcount;
		if(isVisitShow) {
			count = String(data.visitor)
		}
		const val = count.split('');

		return (
			<View style={styles.container}>
				{data ? <ScrollView>
					<View style={styles.content}>
						<View style={styles.contentItem}>
							<View style={{ flex: 1.2 }}>
								<View style={{ justifyContent: 'flex-end' }}>
									{data.Favorites == 1 ? (
										<Image
											resizeMode="contain"
											style={{
												flex: 1,
												width: 30,
												height: 30,
												right: 0
											}}
											source={require('../icons/favourite2x.png')}
										/>
									) : null}
								</View>
								<Image
									resizeMode="contain"
									style={{
										flex: 1,
										width: null,
										height: null
									}}
									source={{ uri: data.pin_image }}
								/>
							</View>
							<View style={{ flex: 2, alignItems: 'center' }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.rate')}
								</Text>
								
								<View
									style={{
										flexDirection: this.props.isRTL
											? 'row-reverse'
											: 'row'
									}}
								>
									<Text
										style={{
											fontSize: 35,
											color: '#099e42'
										}}
									>
										{this.state.PinRating[0].rating ? this.state.PinRating[0].rating.toFixed(2) : null}
									</Text>
									<View
										style={{
											marginLeft: 15,
											justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										<StarRating
											disabled={false}
											maxStars={5}
											rating={this.state.PinRating[0].rating}
											starSize={15}
											emptyStarColor="#cacdd1"
											fullStarColor="green"
											emptyStar="star"
											starStyle={
												this.props.isRTL
													? { marginRight: 5 }
													: { marginLeft: 5 }
											}
											containerStyle={{ paddingTop: 5 }}
										/>
										{
										<View
											style={{
												flexDirection: this.props.isRTL
													? 'row-reverse'
													: 'row',
												paddingTop: 10
											}}
										>
											<Icon
												name="user"
												size={10}
												color="#4c4d4f"
												style={[
													styles.icons,
													{ marginLeft: 10 }
												]}
											/>											
											<Text style={styles.iconsText}>
												{this.state.PinRating[0].totalUserRating}
											</Text>
										</View>
									}
									</View>
								</View>
								
								<View
									style={{
										flexDirection: this.props.isRTL
											? 'row-reverse'
											: 'row',
										marginTop: 5
									}}
								>
									<Text style={[styles.contentItemText,{textAlign:this.props.isRTL ? 'right': null}]}>
										{strings('sellerDetail.pin_number')} :{' '}
									</Text>
									<Text style={{ color: '#2b93e2' }}>
										{data.pin_no}
									</Text>
								</View>
							</View>
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.sellerName')}
								</Text>
								<Text style={styles.contentItemText}>
									{data.pin_name}
								</Text>
							</View>
							<View style={{ flex: 1, marginLeft: 50 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.type')}
								</Text>
								<Text style={styles.contentItemText}>
									{this.props.isRTL ? this.getArabicText(data.pin_type) : data.pin_type}
								</Text>
							</View>
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.membership')}
								</Text>
								<Text style={styles.contentItemText}>
									{ this.changePlatinumToGold(membership) }
								</Text>
							</View>
							{this.state.userId === this.state.data.user_id && isVisitShow && (
								<View
									style={{
										flex: 1,
										alignItems: this.props.isRTL
											? 'flex-start'
											: null,
										marginLeft: 50
									}}
								>
									<View
										style={{
											flexDirection: this.props.isRTL
												? 'row-reverse'
												: 'row'
										}}
									>
										{ val.map((item, i) => {
											
											return (
												<View
													style={
														styles.counterContainer
													}
													key={i}
												>
													<Text
														style={
															styles.counterText
														}
													>
														{item}
													</Text>
												</View>
											);
										})}
									</View>
									<Text style={styles.contentItemText}>
										{strings('sellerDetail.visit')}
									</Text>
								</View>
							)}
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.timing')}
								</Text>
								<Text style={{fontWeight:'bold',textAlign:this.props.isRTL ? 'right': null}}>{this.props.isRTL ? 'خلال الاسبوع' : 'During Week'}</Text>
								{
								// <Text style={styles.contentItemText}>
								// 	{this.getWeekDays(data.working_from_wn)}
								// </Text>
								}
								<Text style={styles.contentItemText}>
									{data.timing_from_wd} - {data.timing_to_wd}
								</Text>
								<Text style={{fontWeight:'bold',textAlign:this.props.isRTL ? 'right': null}}>{this.props.isRTL ? 'نهاية الاسبوع' : 'During Weekend'}</Text>
								<Text style={styles.contentItemText}>
									{data.working_from_wn}
								</Text>
								<Text style={styles.contentItemText}>
									{data.timing_from_wn} - {data.timing_to_wn}
								</Text>
							</View>
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.description')}
								</Text>
								<Text style={styles.contentItemText}>
									{data.description}
								</Text>
							</View>
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.missing_delivery')}
								</Text>
								<Text style={styles.contentItemText}>
									{this.Capitalize(data.delivery_status)}
								</Text>
							</View>
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.delivery_notes')}
								</Text>
								<Text style={styles.contentItemText}>
									{data.delivery_note ? data.delivery_note : 'Not available'}
								</Text>
							</View>
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.location')}
								</Text>
								<Text style={styles.contentItemText}>
									{data.location}
								</Text>
							</View>
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.building')}
								</Text>
								<Text style={[styles.contentItemText,{textAlign:this.props.isRTL ? 'right': null}]}>
									{data.building ? data.building : 'Not available'}
								</Text>
							</View>
							<View style={{ flex: 1, marginLeft: 50 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.floor')}
								</Text>
								<Text style={[styles.contentItemText,{textAlign:this.props.isRTL ? 'right': null}]}>
									{data.floor ? data.floor : 'Not available'}
								</Text>
							</View>
						</View>
						<View style={styles.contentItem}>
							<View style={{ flex: 1 }}>
								<Text style={[styles.contentItemHeading,{textAlign:this.props.isRTL ? 'right': null}]}>
									{strings('sellerDetail.store')}
								</Text>
								<Text style={styles.contentItemText}>
									{data.office_no ? data.office_no : 'Not available'}
								</Text>
							</View>
						</View>
						{ /*
						 data.contact_visibility === "yes"  &&
							<View>
								<Text style={styles.contentItemHeading}>Contact</Text>
								<Text>{data.contact_no}</Text>
							</View>
						*/
						}
						<View
							style={{
								flexDirection: this.props.isRTL
									? 'row-reverse'
									: 'row',
								marginBottom: 300,
								marginTop:10
							}}
						>
							<View style={{ flex: 1 }}>
								<MapView style={styles.map}
									region={{
										latitude: data.lat,
											longitude: data.lng,
											latitudeDelta: 0.0922/40,
      										longitudeDelta: 0.0421/20,
									}}
									>
									<Marker
										coordinate={{
											latitude: data.lat,
											longitude: data.lng,
										}}
									/>
								</MapView>
							</View>
						</View>
					</View>
					{
						// <View style={{ margin: 10, alignItems: 'center' }}>
						// 	<TouchableOpacity style={styles.abuseButton}>
						// 		<Text
						// 			style={{ color: '#ffffff', fontWeight: 'bold' }}
						// 		>
						// 			{strings('sellerDetail.report')}
						// 		</Text>
						// 	</TouchableOpacity>
						// </View>
					}
				</ScrollView>: <PreLoader />}
			</View>
		);
	}
}

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SellerDetails);

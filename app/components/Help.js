import React, { Component } from 'react';
import { View, 
	StatusBar, 
	Text, 
	Image, 
	ScrollView, 
	StyleSheet, 
	TouchableOpacity, 
	Dimensions, 
	TextInput, 
	Keyboard,
	AsyncStorage,
	ActivityIndicator,
	Alert,
} from 'react-native';
import { connect } from 'react-redux';
import Toast from 'react-native-simple-toast';
import Communications from 'react-native-communications';
import API_URL from '../configuration/configuration';

class Help extends Component {

	static navigationOptions = ({ navigation }) => {
		//console.warn('navigation', navigation)
		let {state} = navigation;
		
		
		
		if (state.params !== undefined) {
			const helpTitle = (state.params.isRTL === true) ? 'مساعدة' : 'Help';
			const { title } = state.params;
			return {
    		title: title ? title : helpTitle,
    		
    	}}
	}	
	
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			description: '',
			token: '',
			userId: '',
			infoLoading: true,
			headerTitle: '',
		}
	}

	componentDidMount() {
		const {setParams} = this.props.navigation;
		const headerTitle = this.props.navigation && this.props.navigation.state && this.props.navigation.state.params ? this.props.navigation.state.params.title: 'Help';
    	setParams({isRTL: this.props.isRTL});
		AsyncStorage.getItem('user_data').then(json => {
			const data = JSON.parse(json);
			if(data.sData) {
				this.setState({token:data.sData, userId: data.userId, headerTitle})
				this.getContactInfo(data.sData)
			}
			
		})
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
    	this.props.navigation.setParams({ isRTL: nextProps.isRTL });
    	this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}

	getContactInfo(token) {
		
		fetch(API_URL+'/get-contact-info', 
			{ method: 'POST',
			 headers: 
			 { 
			 'Accept': 'application/json', 
			 'Content-Type': 'application/json', 
			 'Authorization': token 
			}
			 }).then((response) => response.json()) 
			 .then((responseData) => { 
			 	 //console.warn(" get-contact-info ->> ",responseData);
			 	 if (responseData.status == 200 ) { 
		 			this.setState({contactInfo: responseData.data, infoLoading: false}); 
		 		}}) 
			 .catch((err) => { 
			  Alert.alert('Error', err.message) 
			});
	}
	submit = () => {
		console.warn('userID', this.state.userId)
		const headerTitle = this.state.headerTitle;
		const description = this.state.description;
		const url = headerTitle == 'Help' ? API_URL+'/user-request-help' : API_URL+'/user-request-subscription';
		const { isRTL } = this.props;
		if(this.state.title.trim() && this.state.description.trim()){
			fetch(url, {
				method: 'POST',
	  		headers: { 
	   			'Accept': 'application/json', 
	   			'Content-Type': 'application/json',
	   			'Authorization': this.state.token
				},
	  		body: JSON.stringify({
	  			userId: this.state.userId,
	  			title: this.state.title,
	  			comment: this.state.description
			  })
				}).then((response) => response.json())
				  .then(async (responseData) => { 
				  console.warn('help--->',responseData)
				
				 const message = isRTL ? 'تم ارسال الرسالة بنجاح' : 'Message sent successfully';
				Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
					this.setState({title:'', description: ''})
					this.props.navigation.goBack();
				})
				 .catch((error) => {
	          		const errorText = isRTL ? 'خطأ' : "Error";
					Alert.alert(errorText, err.message);
	    });			
		} else {
			const message = isRTL ? 'كل الحقول مطلوبة ' : 'All fields are required';
			Alert.alert('',message)
		}
		
	}

	render() {
		console.warn('this',this.state.title)
		const isRTL = this.props.isRTL;
		return (
				<ScrollView style={styles.container}>
					<View style={styles.content}>
						<TextInput 
							placeholder={isRTL? "عنوان الرساله" : 'Title'}
							
							underlineColorAndroid='white' 
							style={[styles.inputBox,{textAlign: this.props.isRTL ? 'right':null}]}
							onChangeText={(text) => this.setState({title: text})}
							value={this.state.title}
						/>
						<TextInput 
							placeholder={isRTL? "الوصف" : 'Description'}
							underlineColorAndroid='white' 
							textAlignVertical="top"
							style={[styles.inputBox, {height:150,textAlign: this.props.isRTL ? 'right':null}]}
							multiline={true}
							numberOfLines={10}
							onChangeText={(text) => this.setState({description: text})}
							value={this.state.description}
							blurOnSubmit={true}
							onSubmitEditing={()=>{Keyboard.dismiss()}}
							returnKeyType="done"
						/>
						<TouchableOpacity style={styles.submitBtn} onPress={this.submit}>					
							<Text style={styles.btnText}>{ isRTL? "أرسل" : "Submit"}</Text>					
						</TouchableOpacity>
					</View>		
					 <ContactInfo isRTL={isRTL} contactInfo={this.state.contactInfo} infoLoading={this.state.infoLoading} />	
				</ScrollView>
		)
	}
} 

const ContactInfo = (props) => {

	const { infoLoading, contactInfo } = props;

	if(infoLoading) {
		return <ActivityIndicator />
	}

	const {contact, email } = contactInfo; 

	return(
		<View style={styles.content}>
			<View style={{flexDirection: 'row', padding:5}}>
				<Text>Email: </Text>
				<Text>{email}</Text>
			</View>

			<View style={{flexDirection:'row',marginTop:10, padding:5}}>
				<Text>Contact: </Text>
				<Text>{contact}{'  '}</Text>
				<TouchableOpacity style={styles.callButton} onPress={() => Communications.phonecall(contact, true)}>
					<Text style={styles.callText}>Call</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

export default connect(mapStateToProps)(Help);

const styles = StyleSheet.create({
	container: {
		flex: 1, 
		backgroundColor:'#ecf2f9',
	},
	content: {
		margin:10
	},
	callText: {
		color:'#ffffff',
		paddingHorizontal:25
	},
	callButton: {
		backgroundColor: '#00a640', 
		borderRadius:25
	},
	inputBox: {
		
		backgroundColor: '#FFF',
		borderRadius:25,
		padding:15,
		marginTop:10,		
	},
	submitBtn: {
		margin: 30,
		borderRadius:25,
		backgroundColor: '#33a5ea',
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 15,
	},
	btnText: {
		color: '#FFF',
		fontWeight: 'bold'
	}
});



import React, { Component } from 'react';
import { 
	Text, 
	View, 
	ScrollView, 
	StyleSheet, 
	Image, 
	TouchableOpacity,
	Modal, 
	Button,
	Platform,
	TextInput,
	AsyncStorage,
	Alert,
	Picker,
	ActivityIndicator,
	RefreshControl,
} from 'react-native';
import SearchInput,{ createFilter } from 'react-native-search-filter';
import RNPickerSelect from 'react-native-picker-select';
import Data from './data';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Message';
import I18n from 'react-native-i18n';
import moment from 'moment';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import NotificationHeader from './NotificationHeader';
import Toast from 'react-native-simple-toast';

import API_URL from '../configuration/configuration';

const KEYS_TO_FILTERS = ['to_user_name', 'from_user_name'];

class Message extends Component {
	static navigationOptions = ({navigation}) => {
		let  state = navigation.state;
		if (state.params !== undefined) {
			return {
		    title: (state.params.isRTL === true) ? 'رسائل' : 'Messages',
		    headerRight: <NotificationHeader navigation={navigation} />
		  }
		}
	}

	constructor(){
		super();
		this.state = {
			isLoading: true,
			searchText: '',
			selectedMessage: false,
			text:'',
			message: [],
			status:'',
			// readMsg: false,
			refreshing: false,
			token: ''
		}
		this.handleSearch = this.handleSearch.bind(this);
		this.userDetail = {};
	}

	async getAllMessage(){ 
		const getUserData = await AsyncStorage.getItem('user_data'); 
		const userData = JSON.parse(getUserData);
		this.userDetail = userData;
		const { isRTL } = this.props;
		const token = userData.sData; 
		// this.setState({token})
		fetch(API_URL+'/chatmessage', 
			{ method: 'GET',
			 headers: 
			 { 
			 'Accept': 'application/json', 
			 'Content-Type': 'application/json',
			 'Authorization': token }
			 }).then((response) => response.json()) 
			 .then((responseData) => { 
			 	// console.warn('chatmessage-->>',responseData)
			 	if (responseData.status == 654864 && responseData.eData && responseData.eData.eCode == 564626){ 
			 		this.setState({message: {}, isLoading: false, refreshing: false});
		 		} else if (responseData.status == 516324 && responseData.eData === null) { 
		 			// console.warn('message',responseData);
		 			this.setState({message: responseData.Allmessage, isLoading: false, refreshing: false});
		 		}}) 
			 .catch((err) => {
			  	const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	} 

	 componentDidMount() { 
 		const {setParams} = this.props.navigation; 
 		setParams({isRTL: this.props.isRTL}); 
 		this.getAllMessage(); 
 	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
    	this.props.navigation.setParams({ isRTL: nextProps.isRTL });
    	this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}

	handleSearch() {
		this.setState({searchText: this.state.text})
	}

	onPressFilter(){
		this.setState({selectedMessage: true})
	}

	deleteMessageConfirm(id) {
		const message = this.props.isRTL ? 'هل تريد حذف هذه الرسالة ؟' : 'Do you want to delete this Message?';
		const yesText = this.props.isRTL ? 'نعم' : 'Yes';
		const noText = this.props.isRTL ? 'لا' :  'No';
		Alert.alert(
		  '',
		  message,
		  [		    
		    {text: noText, style: 'cancel'},
		    {text: yesText, onPress: () => this.deleteMessage(id)},
		  ],
		  { cancelable: false }
		);

		
	}

	async deleteMessage(toUserId) {
		const { isRTL } = this.props;
		const fromUserId = this.userDetail.userId
		const token = this.userDetail.sData;
		fetch(API_URL+'/deleteMsg', {
  			method: 'POST',
    		headers: { 
     			'Accept': 'application/json', 
     			'Content-Type': 'application/json',
     			'Authorization': token,
    			},
    		body: JSON.stringify({ 
    			fromId:fromUserId,
				toID: toUserId
    		})
		}).then((response) => response.json())
		  .then(async (responseData) => { 
		  	// console.warn('message--->>>',responseData)
		  	if(responseData.status == 654864 && responseData.eData.eCode == 545864){
		  		const message = isRTL ? 'فشل في مسح الرسالة ' : 'Failed to delete Message'
		  		Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
		  	} else if(responseData.status == 516324 && responseData.eData === null){
		  		// Alert.alert('Delete Message','Message deleted successfully');
		  		const message = isRTL ? 'تم مسح الرسالة بنجاح ' : 'Message deleted successfully'
		  		Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
		  		this.getAllMessage();
		  	} 
		  })
		  .catch((err) => { 
		  	const errorText = isRTL ? 'خطأ' : "Error";
			Alert.alert(errorText, err.message);
		  
		});
	}

	_onRefresh() {
		 this.setState({refreshing: true});
		this.getAllMessage();
	}

	async readMessages(userId){
		const fromUserId = this.userDetail.userId
		const token = this.userDetail.sData;
		console.warn('iseri: ', userId);
		const { isRTL } = this.props;
		fetch(API_URL+'/all-msg-read', {
  			method: 'POST',
    		headers: { 
     			'Accept': 'application/json', 
     			'Content-Type': 'application/json',
     			'Authorization': token,
    			},
    		body: JSON.stringify({ 
    			userId: userId
    		})
		}).then((response) => response.json())
		  .then((responseData) => {
		  	if(responseData.status == 516324 && responseData.eData === null){
		  		this.getAllMessage(); 
		  	} 
		  })
		  .catch((err) => { 
		   Alert.alert('Error',err.messages);
		});
	} 


	handleMessage = (ob) => {
		const { navigate } = this.props.navigation;
		navigate('ChatMessages',{ob})
		this.readMessages(ob.userId);
	}

	render(){

		// console.log("Message Testing",this.props.isRTL)
		// console.warn("Message Test",this.state.message)
	
		const isRTL = this.props.isRTL;
		let status = this.props.isRTL ? [{label: "الكل", value: ""},
		{label: "مقروء", value: "1"},
		{label: "غير مقروء", value: "0"}] :[{label: "All", value: ""},
		{label: "Read", value: "1"},
		{label: "Unread", value: "0"}];
		const { navigate } = this.props.navigation;
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		// let allMessages = [];
		// if(this.state.status === "0" || this.state.status === "1" ) {
		// 	// console.warn('salim....')
		// 	allMessages = this.state.message.filter(item => item.seen == this.state.status);
		// } else {
		// 	allMessages = this.state.message;
		// }
		// filteredData = allMessages.filter(createFilter(this.state.searchText, KEYS_TO_FILTERS));
		const messageList=[];
		const message = this.state.message;
		let i = 0;
		if(Object.keys(message).length) {
			for(index in message) {
				const ob = message[index];
				const toUserId = ob.userId;
				i++;
				messageList.push( <TouchableOpacity 
	            	onPress={() => {
	            		navigate('ChatMessages',{ob})
	            	}} 
	            	key={index}
	            >
	              <View style={[styles._messageBox, i===1 ? {borderTopRightRadius: 20, borderTopLeftRadius: 20} : ''] } >
	              	
	              	<View style={ styles._textBox }>             		
	                  	<View style={{flex: 0.89}}>	
		                  		<Text style={ [styles._description, {fontSize:20, fontWeight:'bold'},{textAlign:this.props.isRTL ? 'right': null}] }>{ob.userName}</Text>	                  	
		                  	</View>
	                	<View style={ styles._descriptionContainer }>
	                		
	                		<View style={{flex: 0.89}}>	
		                  		<Text style={ [styles._description,{textAlign:this.props.isRTL ? 'right': null}] }>{ob.message}</Text>	                  	
		                  	</View>
		                  
	                  	<View style={{flex: 0.11, marginBottom: Platform.OS == 'ios' ?40 : null}}>
	                  		<TouchableOpacity style={{backgroundColor: 'transparent'}} onPress={this.deleteMessageConfirm.bind(this, toUserId)}>
		                		<Image style={[styles._icon, {resizeMode:'contain', }]} source={require('../icons/delete2x.png')}/>
		                	</TouchableOpacity>
		               	</View>
		              	</View>
	                </View>
	              </View>
	             </TouchableOpacity>)
			}
		}
		

		return(
			<View style={ styles.container }>	
				{ /*
				<View style={{flexDirection: this.props.isRTL? 'row-reverse' : 'row', justifyContent: 'space-between', alignItems: 'center'}}>
					<View style={[styles.searchContainer, {flexDirection: this.props.isRTL? 'row-reverse' : 'row', justifyContent:'space-between', alignItems: 'center'}]}>
						<View style={ [styles._textbox, {height: Platform.OS === 'ios' ? '60%' : '100%', marginTop: Platform.OS === 'ios' ? 15 : 0, flex:4}] }>
							<SearchInput 
					            onChangeText = {(text) => this.setState({searchText:text})}
					            style={styles._searchText} 
					            placeholder= {strings('message.search')}
					            keyboardType={'default'}
					            underlineColorAndroid='white'
					            returnKeyType="go"
						   />
						</View>
			        		<Image style={{width:20, height:20, marginHorizontal:5, resizeMode: 'contain', marginRight: 10}} source={require('../icons/search3x.png')}/>					
					</View>			

					<View style={ styles._buttonFilter }>
						{Platform.OS === 'android' ? 
						<Picker
							style={{color:'grey'}}
					        selectedValue={this.state.status}
					        onValueChange={(itemValue, itemIndex) => this.setState({status: itemValue})}>						 
					        <Picker.Item label={isRTL? " الكل" : "All"} value="" />
					        <Picker.Item label={isRTL? "مقروء" : "Read"} value="1" />
					        <Picker.Item label={isRTL? "غير مقروء" : "Unread"} value="0" />
						</Picker>:
						<View style={{left: 20}}>
							<RNPickerSelect
								placeholder={{
									label: 'Select your CountryCode',
									value: null
								}}
								value= {
									this.state.status
								}		
								onValueChange={(itemValue, index) =>
									this.setState({status: itemValue})
								}
								style={{
									placeholderColor: '#7A7A7A'
								}}
								hideIcon
								items={status}
							/>
						</View> 
					}
		  			</View>
	  			</View>
	  		*/}

				<ScrollView 
		         refreshControl={
			          <RefreshControl
			            refreshing={this.state.refreshing}
			            onRefresh={this._onRefresh.bind(this)}
			          />
			        }
            	>
			{ this.state.isLoading ? 
				<PreLoader />
				: messageList.length ? messageList : <NoDataFound />
				 
			}
				</ScrollView>
			</View>
			)
	}
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Message)

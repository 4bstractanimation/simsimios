import React from 'react';
import {Text, View, Alert, TouchableOpacity, StyleSheet} from 'react-native';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/style';

class ChangeLanguage extends React.Component {
	constructor() {
		super();
		this.handleLogout = this.handleLogout.bind(this)
		this.stopAppExecution = this.stopAppExecution.bind(this)
		this.language = '';
	}

	handleLogout(setLanguage) {
		var self = this;
		Alert.alert('oops', 'Do you want to change language',
			[{text: 'ok', onPress: () => {this.stopAppExecution(setLanguage)}}]
		)
	}

	stopAppExecution(setLanguage) {
		this.language = setLanguage;
		if (setLanguage === 'arabic') {
			this.props.setLayout();
		} else if(setLanguage === 'english') {
			this.props.getLayout();
		}
	}

	render() {
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		return (
			<View>
				<TouchableOpacity underlayColor='#FFFFFF' onPress={() => this.handleLogout('arabic')}>
          <Text style={{}}>ChangeLanguage to arabic</Text>
        </TouchableOpacity>
        <TouchableOpacity underlayColor='#FFFFFF' onPress={() => this.handleLogout('english')}>
          <Text style={{}}>ChangeLanguage to english</Text>
        </TouchableOpacity>
        <Text style={styles.welcome}>Change language to {this.language}</Text>
			</View>
		)
	}
}

const styles = StyleSheet.create({
 	container:{
 		flex:1,
 		justifyContent:'center'
 	}
 })

 function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeLanguage)

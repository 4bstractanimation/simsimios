import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Clipboard, TextInput, ScrollView, Alert, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
// import Clipboard from 'react-native-clipboard';
import Share from 'react-native-share';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Invite';
import Toast from 'react-native-simple-toast';

const APP_STORE_LINK = 'https://apps.apple.com/us/app/simsim-marktes/id1464661512?ls=1';
			
const PLAY_STORE_LINK = 'https://play.google.com/store/apps/details?id=com.simsimproject';


class InviteFriends extends Component {
	constructor(props) {
		super(props);
		this.state = {
			urlValue: Platform.OS === 'ios' ? APP_STORE_LINK : PLAY_STORE_LINK
		}
	}

  static navigationOptions = ({ navigation }) => {
		let {state} = navigation;
		if (state.params !== undefined) {
			return {
    		title: (state.params.isRTL === true) ? ' ادعو معارفك' : 'Invite Friends'
    }}
	}

	componentDidMount() {
		const {setParams} = this.props.navigation;
    	setParams({isRTL: this.props.isRTL});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
    	this.props.navigation.setParams({ isRTL: nextProps.isRTL });
    	this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}

  shareOnSocialMedia = () => {
  	const AppURL = Platform.OS === 'ios' ? APP_STORE_LINK : PLAY_STORE_LINK;
  	let shareOptions = {
			title: "SimSim",
			message: "SimSim Markets",
			url: AppURL,
		}
		Share.open(shareOptions);
  }

  copyUrlValue = async () => {
  	await Clipboard.setString(this.state.urlValue);
  	Toast.showWithGravity('SimSim Markers app url copied', Toast.LONG, Toast.CENTER);
  }	

	render() {
		// const shadowStyle = {
		// 	shadowOpacity: 1,
		// 	shadowRadius: 10,
		// 	shadowColor: 'lightgray'
		// }
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		return (
			<View style={styles.container}>
				<ScrollView>

					<View style={{ flexDirection: this.props.isRTL ? 'row-reverse' : 'row'}}>
						<View style={styles.urlBox}>					
							<TextInput 
								editable={false}
								value={this.state.urlValue}
								placeholder={strings('invite.link')}
								underlineColorAndroid='transparent'
								style={styles.urlInput}
							/>
							<Icon name="link" size={30} color="#616263" style={styles.linkIcon}/>					
						</View>
						<TouchableOpacity style={styles.copyButton} onPress={this.copyUrlValue}>
							<Text style={styles.copyButtonText}> 
								{strings('invite.copy')}
							</Text>
						</TouchableOpacity>
					</View>
					<TouchableOpacity style={styles.inviteFriends} onPress={this.shareOnSocialMedia}>					
						<Text style={styles.copyButtonText}>{strings('invite.invite')}</Text>					
					</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(InviteFriends)


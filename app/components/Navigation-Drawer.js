import React, { Component } from 'react';
import {
	Dimensions,
	Image,
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	ScrollView,
	AsyncStorage,
	Alert,
	I18nManager
} from 'react-native';
//import i18n from 'react-native-i18n';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { NavigationActions,SafeAreaView } from "react-navigation";
import ListView from './ListView';
import SellerAds from './SellerAds';
import ResetPassword from './ResetPassword';
import Policy from './Policy';
import SignUp from './SignUp';
import Login from './Login';
import Otp from './Otp';
import SellerContainer from './SellerContainer';
import Icon from 'react-native-vector-icons/FontAwesome';
import Maps from './maps/Maps';
import AddPin from './AddPin';
import Home from './maps/HomeMap';
import Profile from './Profile';
import InviteFriends from './InviteFriends';
import Notification from './Notification';
import Subscription from './Subscription';
import History from './History';
import ManageCommercial from './ManageCommercial';
import Favourites from './Favourites';
import Language from './changeLanguage';
import NavigationHeader from './NotificationHeader';
import { strings } from './../../locales/i18n.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as layoutActions from '../actions/layout';
import API_URL from '../configuration/configuration';
import OneSignal from 'react-native-onesignal'; 
import Toast from 'react-native-simple-toast';
import TermAndCondition from './TermAndCondition';

const { width, height } = Dimensions.get('window');

let routeConfig = {
	Home: { screen: Home },
	Policy: { screen: Policy },
	SignUp: { screen: SignUp },
	Login: { screen: Login },
	ListView: { screen: ListView },
	// SellerContainer: {screen: SellerContainer},
	Profile: { screen: Profile },
	InviteFriends: { screen: InviteFriends },
	Notification: { screen: Notification },
	Favourites: { screen: Favourites },
	History: { screen: History },
	ManageCommercial: { screen: ManageCommercial },
	Subscription: { screen: Subscription }
};

class SideMenu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loginToken: '',
			routeName: 'Home',
			notificationCount: null,
			changeLanguageFlag: 0,
		};
		this.setLanguage = this.setLanguage.bind(this);
		OneSignal.init("11a642c1-9671-4e10-ac73-ca5fba2c0cdf");
    	OneSignal.addEventListener('opened', this.onOpened);
	}

	componentDidMount() {

		this.props.navigation.setParams({
			// setLayout: this.props.setLayout,
			// getLayout: this.props.getLayout,
			routeName: 1
		});
		// this.setLoginToken();

		this.setLanguage();
	}

	// setLoginToken = () => {

	// 	if(this.state.loginToken) {
	// 		return 0;
	// 	}
		
	// 	AsyncStorage.getItem('user_data').then(data => {

	// 		if (data) {
	// 			const userData = JSON.parse(data);
	// 			this.setState({
	// 				loginToken: userData.sData
	// 			});
	// 			this.props.navigation.setParams({
	// 				notificationCount: userData.notiCount
	// 			});
	// 		} else {
	// 			this.setState({
	// 				loginToken: ''
	// 			});
	// 		}
	// 	});
	// }

	onOpened = (openResult) => {
	 	// console.warn('Message: ', openResult.notification.payload.body);
	    // console.warn('data: ', openResult.notification.payload.additionalData);
	 	const data = openResult.notification.payload.additionalData;
	 	
	 	const { navigate } = this.props.navigation;
	 	switch(data.type) {

	 		case 'message': navigate('Message'); 
	 			break;
	 		case 'news': const newsNotification = {id: data.pinId, pin_name: data.pin_name, isfavourite: true}
				 this.countVisitors(data.pinId)
				 
	 			navigate('SellerContainer',{item: newsNotification}); 
	 			break;
	 		case 'pin': const specialOfferAlarmNotification = {id: data.pinId, pin_name: data.pin_name, isfavourite: true}
	 			this.countVisitors(data.pinId)
	 			navigate('SellerContainer',{item: specialOfferAlarmNotification}); 
	 			break;
	 		case 'pin approved': navigate('Pin');
	 			break;
	 		case 'publish': navigate('Pin');
	 			break;
	 		case 'offer': const item1 = {id: data.pinId, pin_name: data.pinName };
	 			this.countVisitors(data.pinId)
	 			navigate('SellerContainer',{item: item1}); 
	 			break;
	 		// case 'CommercialNotify': const item2 = {id: data.data.pinId, pin_name: data.data.pinName };
	 		// 	// this.countVisitors(item2.id)
	 		// 	navigate('SellerContainer',{item: item2}); 
	 		// 	break;
	 		case 'budget': 
	 			navigate('ManageCommercial'); 
	 			break;
	 		case 'commercials': 
	 			navigate('ManageCommercial'); 
	 			break;
	 		case 'commercial request': 
	 			navigate('ManageCommercial'); 
	 			break;
	 		case 'plan': 
	 			navigate('Subscription');
	 			break;	 
	 		case 'extendads': 
	 			navigate('SellerContainer',{item: data.pinDetail});
	 			break;	
 			case 'ADSEXTENSION': 
 				navigate('SellerContainer',{item: {id: data.id, pin_name: data.pin_name}});
 				break;	 			
	 		case 'CommercialNotify':	 		 
	 			// const userId = data.data.user_id;
				 // navigate('SellerContainer',{item: {id: data.data.pinId, pin_name: data.data.pinName}});
				 console.warn('13');
				 const userId = data.data.user_id;
				 let adStatus = '';
				 fetch(
					 API_URL +
						 '/get-adslist?pid=' +
						 data.data.pinId +
						 '&adStatus=' +
						 adStatus,
					 {
						 method: 'GET',
						 headers: {
							 Accept: 'application/json',
							 'Content-Type': 'application/json',
						 },
					 },
				 )
					 .then(response => response.json())
					 .then(async responseData => {
						 if (responseData.status == 516324 && responseData.eData === null) {
							 navigate('SellerContainer', {
								 item: {
									 ...responseData,
									 id: data.data.pinId,
									 pin_name: data.data.pinName,
								 },
							 });
						 }
					 })
					 .catch(err => {
						 Alert.alert('Error', err.message);
					 });
	 			this.countVisitors(data.data.pinId)
	 			fetch( API_URL+'/update-pushnotification-amount', {
					method: 'POST',
					headers: { 
					'Accept': 'application/json', 
					'Content-Type': 'application/json',
					'Authorization': this.state.userToken
				},
				body: JSON.stringify({userId: userId, pushId: data.id})
				}).then((response) => response.json())
				.then((responseData) => {
					// console.warn('deduct-->>', responseData)					
					
				}).catch((err) => { 
					Alert.alert('Error',err.message);
				
				});	 			 
	 			break;
	 		default: break;
	 		
	 	}
	 }

	 countVisitors(pinid) {
	  
	    const url = API_URL+'/no-of-visitor';
	    fetch(url, {
	      method: 'POST',
	      headers: {
	        Accept: 'application/json',
	        'Content-Type': 'application/json'
	      },
	      body: JSON.stringify({
	        pinId: pinid
	      })
	    })
	      .then(response => response.json())
	      .then( responseData => {
	        // console.warn('no-of-visitor-->',responseData);
	      })
	      .catch(err => {
	        Alert.alert('Error',err.message);
	      });
	}
	componentWillReceiveProps(props) {
		
		if (props.isRTL != this.props.isRTL) {
			this.props.navigation.setParams({ isRTL: props.isRTL });
			this.props.navigation.state.params.isRTL = props.isRTL;
		}
		 if(props.loginData.sData !== this.props.loginData.sData) {
		    if(props.loginData.sData) {
		        this.setState({ loginToken:props.loginData.sData, seller_type: props.loginData.userData.seller_profile_type })
		    } else {
		        this.setState({loginToken: '', seller_type: ''});
		    }
	    } else {
	        if(props.loginData.sData) {
	          this.setState({ token:props.loginData.sData, seller_type: props.loginData.userData.seller_profile_type })
	        }
		}

		
	}

	removePushToenFromDB() {

	  	const { loginToken } = this.state;
	  	
	  	if(!loginToken) {
	  		return;
	  	}
	  	
	  	const url = API_URL+'/remove-playerId';
	    fetch(url, {
	      method: 'POST',
	      headers: {
	        Accept: 'application/json',
	       'Content-Type': 'application/json',
	        Authorization: loginToken
	      }
	    })
	      .then(response => response.json())
	      .then( responseData => {
	        // console.warn('response-->',responseData);
	      })
	      .catch(err => {
	        Alert.alert('Error',err.message);
	      });
	  
	}

	logoutConfirmHandler = () => {
		const message = this.props.isRTL ? 'هل تريد تسجيل الخروج؟' : 'Do you want to logout?';
		const yesButtonText = this.props.isRTL ? 'نعم' : 'Yes';
		const noButtonText = this.props.isRTL ? 'لا' :  'No';
		Alert.alert(
		  '',
		  message,
		  [		    
		    {text: noButtonText, style: 'cancel'},
		    {text: yesButtonText, onPress: () => this.logout()},
		  ],
		  { cancelable: false }
		);
	}

	async logout() {
		await AsyncStorage.removeItem('user_data', error => {
			if(error) {
				Alert.alert('Error',"Could not logout")
			} else {
				this.props.resetLoginData();
				this.props.setSellerType('')
				const { navigate } = this.props.navigation;
				const isRTL = this.props.isRTL;
				
				this.removePushToenFromDB();
				this.setState({ loginToken: '' });
				
				// Alert.alert('', 'Your account is successfully logged out');
				const message = isRTL ? 'تم تسجيل الخروج من حسابك بنجاح' : 'Your account is successfully logged out';
				Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);

				navigate('Home');
			}
		});
		
		
	}



	// getSellerSubscriptionPlan(token) {

	//     const url = API_URL+'/get-seller-subscription-plan';

	//     fetch(url, {
	//       method: 'POST',
	//       headers: {
	//         Accept: 'application/json',
	//         'Content-Type': 'application/json',
	//         Authorization: token
	//       }
	//     })
	//       .then(response => response.json())
	//       .then(async responseData => {
	//         // console.warn('get-seller-subscription-plan-->>',responseData);
	//         if( responseData && responseData.status == 200 ) {
	//           this.setState({sellerSubscriptionPlan:responseData.data});
	//         }	        
	//     })
 //        .catch(err => {        
 //       		Alert.alert('Error', err.message)
 //        });
	// }

	setLanguage(){
		AsyncStorage.getItem('isRTL').then(value => {
			if(value == 0){
				this.props.getLayout();
			}else{
				this.props.setLayout();
			}
			
		});
	}

	changeLanguage = async (isRTL) => {
		 const playerId = await AsyncStorage.getItem('playerId');
		
		 if(playerId != null){
				//	console.warn("TCL: playerId", playerId)
					fetch(API_URL+'/setLanguage',{
						method: 'POST',
						headers:{
							'Accept':'application/json',
							'Content-Type':'application/json',
						},
						body: JSON.stringify({
							language: isRTL ? 'en' : 'ar',
							player_id: playerId
						})
					}).then((response) => response.json())
					.then((responseData) => {
						 //console.warn('response-->>', responseData)
						// if(responseData.status == 200){
							
						// }else if(responseData.status == 654864){
						// 	Alert.alert('Error in')
						// }
					}).catch( error => {
						Alert.alert('Error',error.message)
					})
				}


					
				

		if (this.state.changeLanguageFlag) {
			this.setState({changeLanguageFlag:0}, ()=> {
				AsyncStorage.setItem(
      'isRTL',
      JSON.stringify(this.state.changeLanguageFlag)
    );
			})
			this.props.getLayout();
		} else {
			this.setState({changeLanguageFlag:1}, () => {
				AsyncStorage.setItem(
			      'isRTL',
			      JSON.stringify(this.state.changeLanguageFlag)
			    );
			})
			this.props.setLayout();
		}
	}

	componentWillUnmount() {
	    OneSignal.removeEventListener('opened', this.onOpened);	   
	}
	
	render() {
		
		const { isRTL } = this.props;
		// let seller_type = this.state.seller_type;
	//	console.warn("TCL: Navigation render -> seller_type", this.props.sellerType)
		// if(this.props.loginData.userData) {
		// 	seller_type = this.props.loginData.userData.seller_profile_type;
		// }
		// console.warn("TCL: SideMenu -> changeLanguage -> isRTL", isRTL)
		const { navigate, state } = this.props.navigation;
		const loginToken = this.state.loginToken;
		// const loginToken = state.params ? state.params.token : "";

		const routeName = state.params ? state.params.routeName : 1;
		return (
			<SafeAreaView style={styles.container}>
				<ScrollView>
					<View
						style={[
							styles.manuItem,
							{ marginTop: routeName ? 80 : 25 }
						]}
					>
						<Text
							style={[
								styles.manuItemText,
								this.state.routeName == 'Home'
									? { color: '#23aded' }
									: ''
									,{textAlign: this.props.isRTL ? 'right' : 'left',}
							]}
							onPress={evt => {
								this.props.navigation.setParams({
									routeName: 1
								});
								this.setState({ routeName: 'Home' });
								navigate('Home');
							}}
						>
							{isRTL ? 'الصفحة الرئيسية' : 'Home'}
						</Text>
					</View>
					<View style={styles.manuItem}>
						<Text
							style={[styles.manuItemText,{textAlign: this.props.isRTL ? 'right' : 'left',}]}
							onPress={() => this.changeLanguage(isRTL)}
						>
							{isRTL ? 'English' : 'عربي'}
						</Text>
					</View>
					{loginToken ? (
						<View>
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'Profile'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										this.setState({ routeName: 'Profile' });
										this.props.navigation.setParams({
											routeName: 0
										});
										navigate('Profile');
									}}
								>
									{isRTL ? 'الملف الشخصي' : 'Profile'}
								</Text>
							</View>
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'Messages'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										navigate('DrawerClose');
										navigate('Message');
									}}
								>
									{isRTL ? 'رسائل' : 'Messages'}
								</Text>
							</View>
							
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'Favourites'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										this.setState({
											routeName: 'Favourites'
										});
										this.props.navigation.setParams({
											routeName: 0
										});
										navigate('Favourites');
									}}
								>
									{isRTL ? 'المفضلة' : 'Favourites'}
								</Text>
							</View>
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName ==
										'Promotion alert'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										// this.setState({routeName:'Promotion alert'})
										navigate('DrawerClose');
										navigate('Promotion');
									}}
								>
									{isRTL
										? 'منبه العروض'
										: 'Promotion Alarm'}
								</Text>
							</View>

							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'History'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										this.setState({ routeName: 'History' });
										this.props.navigation.setParams({
											routeName: 0
										});
										navigate('History');
									}}
								>
									{isRTL ? 'المحفوظات' : 'History'}
								</Text>
							</View>

							<View style={styles.seprator} />
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == "PIN's"
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										// this.setState({routeName:"PIN's"})
										navigate('DrawerClose');
										navigate('Pin');
									}}
								>
									{isRTL ? 'نقاط البيع' : "Sales Point"}
								</Text>
							</View>
							{ this.props.sellerType && this.props.sellerType.toLowerCase() === 'enterprise' ? (
								<View style={styles.manuItem}>
									<Text
										style={[
											styles.manuItemText,
											this.state.routeName ==
											'Manage Promotions'
												? { color: '#23aded' }
												: ''
												,{textAlign: this.props.isRTL ? 'right' : 'left',}
										]}
										onPress={() => {
											this.setState({
												routeName: 'Manage Promotions'
											});
											this.props.navigation.setParams({
												routeName: 0
											});
											navigate('ManageCommercial');
										}}
									>
										{isRTL
											? 'إدارة الإعلانات التجارية'
											: 'Manage Promotions'}
									</Text>
								</View>
							) : null}
							
							<View
								style={[
									styles.manuItem,
									{
										flexDirection: 'row',
										justifyContent: isRTL
											? 'flex-end'
											: null
									}
								]}
							>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'Subscription'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										this.setState({
											routeName: 'Subscription'
										});
										this.props.navigation.setParams({
											routeName: 0
										});
										navigate('Subscription');
									}}
								>
									{isRTL ? 'إدارة الاشتراك' : 'Subscription & Transactions'}
								</Text>
								{ /*
								<Text
									style={styles.subscriptionType}
									onPress={() => {
										Alert.alert(
											'Upgrade to Platinum',
											'Upgrade your subscription to avail more benefits',
											[
												{
													text: 'Upgrade',
													onPress: () =>
														navigate('Subscription')
												}
											]
										);
									}}
								/>
								*/}
							</View>
							
							<View style={styles.seprator} />
							
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'Invite friends'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										this.setState({
											routeName: 'Invite friends'
										});
										this.props.navigation.setParams({
											routeName: 0
										});
										navigate('InviteFriends');
									}}
								>
									{isRTL ? 'أرسل دعوة لمعارفك' : 'Invite friends'}
								</Text>
							</View>
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'Privacy policy'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										this.setState({
											routeName: 'Privacy policy'
										});
										navigate('Policy');
									}}
								>
									{isRTL
										? 'سياسة الخصوصية'
										: 'Privacy policy'}
								</Text>
							</View>
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'Help'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										this.setState({ routeName: 'Help' });
										navigate('DrawerClose');
										navigate('Help');
									}}
								>
									{ isRTL ? "مساعدة" : "Help"}
								</Text>
							</View>
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										{ marginBottom: 25 }
										,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={this.logoutConfirmHandler}
								>
									{isRTL ? 'الخروج' : 'Logout'}
								</Text>
							</View>
						</View>
					) : (
						<View>
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'Login'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										// this.setState({routeName:'Login'})
										navigate('Login');
									}}
								>
									{isRTL ? 'تسجيل الدخول' : 'Login'}
								</Text>
							</View>
							<View style={styles.manuItem}>
								<Text
									style={[
										styles.manuItemText,
										this.state.routeName == 'SignUp'
											? { color: '#23aded' }
											: ''
											,{textAlign: this.props.isRTL ? 'right' : 'left',}
									]}
									onPress={() => {
										this.setState({ routeName: 'SignUp' });
										navigate('SignUp');
									}}
								>
									{isRTL ? 'سجل' : 'SignUp'}
								</Text>
							</View>
						</View>
					)}
				</ScrollView>
			</SafeAreaView>
		);
	}
}

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL,
		loginData: state.loginReducer.loginUserData,
		sellerType : state.network.sellerType,
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch);
}

let Data = connect(mapStateToProps, mapDispatchToProps)(SideMenu);
let setLanguage = 0;
console.warn("TCL: Data", Data);


const Navigation = DrawerNavigator(routeConfig, {
	
	contentComponent: Data,
	drawerPosition: 'left',
});

const NavigationAR = DrawerNavigator(routeConfig, {
	contentComponent: Data,
	drawerPosition: 'right'
});

/***
 ** This is Drawer header
 */

Navigation.navigationOptions = ({ navigation }) => {
	
	const open = navigation.state.index;
	let setLayout, getLayout, isRTL, x;
	if (navigation.state.routes[0].params) {
		// setLayout = navigation.state.routes[0].params.setLayout;
		// getLayout = navigation.state.routes[0].params.getLayout;
		isRTL = navigation.state.routes[0].params.isRTL;
		x = navigation.state.routes[0].params.routeName;
	}
	
	const drawerBar = (
		<View style={{ flex: 1, flexDirection: 'row' }}>
			<TouchableOpacity
				style={{ marginHorizontal: 10, marginVertical: 10 }}
				onPress={() => {
					navigation.navigate('DrawerToggle');
				}}
			>
				<Image
					style={{ width: 32, height: 32, backgroundColor: 'white' }}
					source={require('../icons/manu-icon.png')}
				/>
			</TouchableOpacity>
		</View>
	);
	const langIconImage = <View />
	// const langIconImage = (
	// 	<TouchableOpacity
	// 		onPress={() => {
	// 			if (setLanguage) {
	// 				setLanguage = 0;
	// 				getLayout();
	// 			} else {
	// 				setLanguage = 1;
	// 				setLayout();
	// 			}
	// 			navigation.navigate('DrawerToggle');
	// 		}}
	// 	>
	// 		<Image
	// 			style={{
	// 				flex: 1,
	// 				resizeMode: 'contain',
	// 				width: 30,
	// 				marginLeft: 20,
	// 				marginRight: 20
	// 			}}
	// 			source={require('../icons/globe.png')}
	// 		/>
	// 	</TouchableOpacity>
	// );
	const bellIcon = (
		<NavigationHeader navigation={navigation} />
	);
	// const headerPostion = open == 0 ? 'headerLeft' : 'headerRight';
	const title = isRTL ? 'قائمة ' : 'Menu';
	const header1 = isRTL ? 'headerRight' : 'headerLeft';
	const header2 = isRTL ? 'headerLeft' : 'headerRight';
	if (open) {
		return {
			title,
			headerBackTitle: null,
			headerRight: drawerBar,
			headerStyle: x
				? {
						backgroundColor: '#fff',
						position: 'absolute',
						top: 0,
						left: 0,
						right: 0,
				  }
				: { backgroundColor: '#fff' }
		};
	} else {
		return { [header1]: drawerBar, [header2]: bellIcon };
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginBottom: 25
	},
	manuItem: {
		marginTop: 25,
		marginLeft: 40,
		marginRight: 40
	},
	manuItemText: {
		fontSize: 20,
		fontWeight: 'bold',
		color: '#595959'
	},
	seprator: {
		borderBottomColor: '#edeff2',
		borderBottomWidth: 1,
		marginTop: 25
	},
	subscriptionType: {
		color: '#00aaff',
		marginLeft: 10
	},
	notification: {
		borderRadius: 64,
		width: 20,
		height: 20,
		backgroundColor: '#00ADDE',
		marginLeft: 16,
		zIndex: 5
	}
});

export default Navigation;

import React, { Component } from 'react';
import {
	Text,
	Image,
	View,
	ScrollView,
	FlatList,
	TextInput,
	Button,
	StyleSheet,
	Dimensions,
	Alert,
	Picker,
	TouchableOpacity,
	AsyncStorage,
	KeyboardAvoidingView,
	Keyboard,
	Platform,
} from 'react-native';
import CheckBox from 'react-native-check-box';
import DeviceInfo from 'react-native-device-info';
// import FcmNot from './fcmNotification';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Login';
import PreLoader from './PreLoader';
import countriesCode from './CountriesCode';
import RNPickerSelect from 'react-native-picker-select';
import OneSignal from 'react-native-onesignal';
import API_URL from '../configuration/configuration';

class Login extends Component {
	static navigationOptions = ({ navigation }) => {
		let { state } = navigation;
		if (state.params !== undefined) {
			return {
				title: state.params.isRTL === true ? 'تسجيل الدخول' : 'Sign In',
				headerTitleStyle: {
					flex: 1,
					textAlign: 'center',
					alignSelf: 'center'
				},
				headerRight: <View />
			};
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			secureTextEntry: true,
			checked: true,
			mobile: '',
			password: '',
			token: null,
			isLoading: '',
			isLogin: '',
			countryCode: '+966',
			deviceId:'',
			error:false,
			userToken: '',
			invalid: false,
			playerId: ''
		};
	}

	async componentWillMount() {

		OneSignal.init("11a642c1-9671-4e10-ac73-ca5fba2c0cdf");
	    OneSignal.addEventListener('ids', this.onIds);

		const getUserData = await AsyncStorage.getItem('user_data');
		if (getUserData) {
			const userData = JSON.parse(getUserData);
			const token = userData.sData;
			this.setState({ token: token });
		}
	}

	componentWillUnmount() {
	    OneSignal.removeEventListener('ids', this.onIds);
	}

	onIds = (device) => {
		 console.warn('playerId-->>',device)
	    this.setState({playerId: device.userId})
	    
	}

	async componentDidMount() {
		OneSignal.configure();
		// const deviceId = await DeviceInfo.getDeviceId();
		// this.setState({deviceId: deviceId});
		// const deviceVer = await DeviceInfo.getSystemName();
		const { setParams } = this.props.navigation;
		setParams({ isRTL: this.props.isRTL });

		AsyncStorage.getItem('userCredential').then(json => {
			const credential = JSON.parse(json);
			if (credential) {
				this.setState({
					mobile: credential.mobile,
					password: credential.password,
					checked: true,
					countryCode: credential.countryCode
				});
			}
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
			this.props.navigation.setParams({ isRTL: nextProps.isRTL });
			this.props.navigation.state.params.isRTL = nextProps.isRTL;
		}
	}

	getUserDetails(token) {
		fetch(API_URL+'/get-user-detail', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token
        }
      })
        .then(response => response.json())
        .then(responseData => {
        	
        	let isCompleted = false;

        	const { gender, dob, name, mobile, email } = responseData.userData;
        	if( gender && dob && name && mobile && email) {
        		isCompleted = true;
	        	
        	}
        	AsyncStorage.setItem('isProfileCompleted', JSON.stringify({
        		isCompleted,
        		counter: 1,
        		userProfile: { gender, dob, name, mobile, email }
	        }))
     
        })
        .catch(err => {
          Alert.alert('Error',err.message);
        });
	}


	onLoginPress() {
		// Alert.alert(this.state.playerId)
		// this.setState({error:'', invalid:''});
		Keyboard.dismiss();
		const { isRTL } = this.props;
		if(this.state.mobile.charAt(0) == 0 || this.state.mobile.charAt(0) == '٠'){

			this.setState({error:true})

		}else{
			this.setState({ isLoading: true, error: false });
		const { navigate } = this.props.navigation;
		fetch(API_URL+'/login', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				countryCode: this.state.countryCode,
				userId: this.state.mobile,
				password: this.state.password,
				deviceId: this.state.playerId,
				playerId: this.state.playerId
			})
		})
			.then(response => response.json())
			.then(responseData => {
				  console.warn('login-->>',responseData)
				if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 236486
				) {

					const errorText = isRTL ? 'خطأ' : "Error";
					const message = isRTL ? 'فشل في تسجيل الدخول ' : 'Failed to Login';
					Alert.alert(errorText);
				} else if (
					responseData.status == 654864 &&
					responseData.eData.eCode == 646483
				) {
					this.setState({invalid:true, isLoading: false})
				} else if (
					responseData.status == 516324 &&
					responseData.eData === null
				) {
					const token = responseData.sData;
					this.getUserDetails(token);
					let userCredential = null;
					if (this.state.checked) {
						const mobile = this.state.mobile;
						const password = this.state.password;
						const countryCode = this.state.countryCode;
						userCredential = { mobile, password, countryCode };
					}

					// await AsyncStorage.setItem('user_data', JSON.stringify(responseData));
					AsyncStorage.multiSet([
						['user_data', JSON.stringify(responseData)],
						['userCredential', JSON.stringify(userCredential)]
					]);
					this.setState({ isLogin: true, userToken:token });
					this.props.setLoginData(responseData);
					navigate('Home');
					// this.props.navigation.goBack()
					// this.notification();
				}
			})
			.catch(err => {
				console.warn(err);
			});
		}
		
	}

	

	toggle() {
		this.setState({ checked: !this.state.checked });
	}

	showPass() {
		if (this.state.secureTextEntry) {
			this.setState({ secureTextEntry: false });
		} else {
			this.setState({ secureTextEntry: true });
		}
	}

	onChanged(text) {
		
		const reg_arNumbers = /^[\u0660-\u0669]/;	
		if (reg_arNumbers.test(text)){
			this.setState({
				mobile: text, isLoading: false
			});
		} 
	     else {
			this.setState({
				mobile: text.replace(/[^0-9]/g, ''), isLoading: false
			});
		}
	}

	render() {
		// const countriesCode = this.state.countriesCode;
		for (let i = 0; i < countriesCode.length; i++) {
			countriesCode[i].value = countriesCode[i].dial_code;
			countriesCode[i].label = countriesCode[i].dial_code+'-'+countriesCode[i].name;
			countriesCode[i].color = '#000000';
		}
		const { navigate } = this.props.navigation;
		const token = this.state.token;
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		if (!this.state.token) {
			return (
				<KeyboardAvoidingView style={styles.container}>
					<ScrollView style={{ padding: 20 }} 
						keyboardShouldPersistTaps="always"
					>
						<View style={{ alignItems: 'center' }}>
							<Image
								style={styles.logo}
								source={require('../icons/logo1x.png')}
							/>
						</View>
						{this.state.invalid && <Text style={{color:'red', textAlign:'center'}}>Invalid login details</Text>}
						<View style={{ flexDirection:'row',}}>
							<View style={styles.ccode}>
							{ Platform.OS === 'android' ?
								<Picker

									selectedValue={this.state.countryCode}
									onValueChange={(itemValue, itemIndex) =>
										this.setState({ countryCode: itemValue })
									}
								>
									<Picker.Item label="Code" />
									{countriesCode.map((item, key) => (
										<Picker.Item
											key={key}
											label={item.dial_code+'-'+item.name}
											value={item.dial_code}
										/>
									))}
								</Picker>
								: <View style={{left: 10, top: 15}}>
									<RNPickerSelect
										placeholder={{
											label: 'Select CountryCode',
											value: null
										}}
										value= {
											this.state.countryCode
										}		
										onValueChange={(itemValue, index) =>
											this.setState({ countryCode: itemValue })
										}
										style={{
											placeholderColor: 'grey'
										}}
										hideIcon
										items={countriesCode}
									/>
								</View>
							}
							</View>
							<View style={styles._textbox}>
								<TextInput
									style={styles._textboxText}
									underlineColorAndroid="white"
									maxLength={11}
									returnKeyType="next"
									onChangeText={text => this.onChanged(text)}
									keyboardType={'numeric'}
									placeholderTextColor="#545454"
									placeholder={strings('login.mobile')}
									value={this.state.mobile}
								/>
							</View>
						</View>
						{this.state.error && <Text style={{color:"red"}}>{this.props.isRTL ? '50xxxxx77 :'+'اكتب رقم جوالك من دون صفر في بداية الرقم، مثلا' : 'Write your mobile number without zero at begining, example: 50xxxxx77'}</Text>}
						<View
							style={[
								styles._textbox,
								{
									flexDirection: this.props.isRTL
										? 'row-reverse'
										: 'row',
									justifyContent: 'space-between'
								}
							]}
						>
							<View
								style={[
									styles._textbox,
									{ width: '80%', marginTop: 0 }
								]}
							>
								<TextInput
									style={[styles._textboxText]}
									onChangeText={text =>
										this.setState({ password: text})
									}
									onFocus={() => {this.setState({isLoading: false})}}
									underlineColorAndroid="white"
									secureTextEntry={this.state.secureTextEntry}
									keyboardType={'default'}
									placeholderTextColor="#545454"
									placeholder={strings('getDetail.password')}
									returnKeyType="go"
									value={this.state.password}
								/>
							</View>
							<TouchableOpacity
								onPress={this.showPass.bind(this)}
							>
								<Image
									style={{
										resizeMode: 'contain',
										width: 25,
										marginVertical: 10,
										marginHorizontal: 15
									}}
									source={require('./../icons/show2x.png')}
								/>
							</TouchableOpacity>
						</View>
						<View
							style={{
								flexDirection: this.props.isRTL
									? 'row-reverse'
									: 'row',
							}}
						>	
						{ this.props.isRTL ? <CheckBox
								style={{ flex: 1, padding: 10}}
								onClick={this.toggle.bind(this)}
								isChecked={this.state.checked}
								leftText={strings('login.remember')}
								checkBoxColor={'#00ADDE'}
							/>
							:
							<CheckBox
								style={{ flex: 1, padding: 10 }}
								onClick={this.toggle.bind(this)}
								isChecked={this.state.checked}
								rightText={strings('login.remember')}
								checkBoxColor={'#00ADDE'}
							/>
						}
							<TouchableOpacity
								onPress={() => navigate('ForgotPassword')}
							>
								<Text style={styles._text}>
									{' '}
									{strings('login.forgot_password')}{' '}
								</Text>
							</TouchableOpacity>
						</View>
						{this.state.isLoading ? (
							<PreLoader />
						) : (
							<TouchableOpacity
								style={styles._button}
								onPress={this.onLoginPress.bind(this)}
							>
								<Text style={styles._buttonText}>
									{strings('login.sign_in')}
								</Text>
							</TouchableOpacity>
						)}
						<View
							style={{
								flexDirection: this.props.isRTL
									? 'row-reverse'
									: 'row',
								justifyContent: 'center',
								marginTop: 20
							}}
						>
							<Text style={{ fontWeight: 'bold' }}>
								{strings('login.new_user')}{' '}
							</Text>
							<TouchableOpacity
								onPress={() => navigate('SignUp')}
							>
								<Text
									style={{
										fontWeight: 'bold',
										textDecorationLine: 'underline'
									}}
								>
									{strings('login.sign_up')}
								</Text>
							</TouchableOpacity>
						</View>
					</ScrollView>
				</KeyboardAvoidingView>
			);
		} else {
			return this.props.navigation.navigate('Home', { token });
		}
	}
}

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

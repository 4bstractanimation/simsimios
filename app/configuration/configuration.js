/*const API_URL = 'http://simsim-markets.com:8000/api';
export const WEB_URL = 'http://simsim-markets.com:8000';*/

const API_URL = 'https://www.simsim-markets.com/api';
export const WEB_URL = 'https://www.simsim-markets.com';

// const API_URL = 'http://192.168.1.104:8000/api';
// export const WEB_URL = 'http://192.168.1.104:8000';

export default API_URL;

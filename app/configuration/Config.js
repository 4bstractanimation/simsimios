import Firebase from "firebase";
const firebaseConfig = {
   	apiKey: "AIzaSyAP8Wt-86lnRb-YWNpsGm7LUw9-iiYnI8c",
    authDomain: "simsim-acdc6.firebaseapp.com",
    databaseURL: "https://simsim-acdc6.firebaseio.com",
    projectId: "simsim-acdc6",
    storageBucket: "simsim-acdc6.appspot.com",
    messagingSenderId: "409395255385",
    appId: "1:409395255385:web:90f9cd0eb5187955713948",
    measurementId: "G-CYW45BLB7M"
  };

const app = Firebase.initializeApp(firebaseConfig);
export const db = app.database();

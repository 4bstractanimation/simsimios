import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  InteractionManager
} from 'react-native';
import { StackNavigator, addNavigationHelpers } from 'react-navigation';
import App from './../components/App';
import Maps from './../components/maps/Maps';
import ListView from './../components/ListView';
// import HomeFilter from './../components/homeFilter';
import Navigation from './../components/Navigation-Drawer';
import FloatingButton from './../components/FloatingButton';
// import Login from './../components/Login';
import ForgotPassword from './../components/ForgotPassword';
import Policy from './../components/Policy';
// import SignUp from './../components/SignUp';
import AddPin from './../components/AddPin';
import SellerAds from './../components/SellerAds';
import SellerDetails from './../components/SellerDetails';
import NewsFeed from './../components/NewsFeed';
import AddNewAd from './../components/AddNewAd';
//import Home from './../components/maps/HomeMap';
import Profile from './../components/Profile';
import Message from './../components/Message';
import Otp from './../components/Otp';
import Pin from './../components/pin';
import TransactionHistory from './../components/TransactionHistory';
import History from './../components/History';
import Promotion from './../components/PromotionAlerts';
import Favourites from './../components/Favourites';
import ManageCommercial from './../components/ManageCommercial';
import AddNew from './../components/AddNew';
import Payment from './../components/Payment';
import SellerContainer from './../components/SellerContainer';
import AdDetails from './../components/AdDetails';
import AddNewNews from './../components/AddNewNews';
import Header from './../components/Header';
import AddOffers from './../components/AddOffer';
import Language from './../components/changeLanguage';
import GetUserLocation from './../components/GetUserlocation';
import ResetPassword from './../components/ResetPassword';
import ChatMessages from './../components/ChatMessages';
import ChangePassword from './../components/ChangePassword';
import Help from './../components/Help';
import Notification from './../components/Notification';
// import NotificationHeader from './../components/NotificationHeader';
import PaymentPlan from './../components/paymentPlan';
import TermAndCondition from './../components/TermAndCondition';
import Subscription from './../components/Subscription';
const Routes = StackNavigator(
  {
    DrawerStack: { screen: Navigation },
    App: { screen: App },
    MapView: { screen: Maps },
    ListView: { screen: ListView },
    FloatingButton: { screen: FloatingButton },
    ForgotPassword: { screen: ForgotPassword },
    Policy: { screen: Policy },
    AdDetails: { screen: AdDetails },
    AddPin: { screen: AddPin },
    SellerAds: { screen: SellerAds },
    SellerDetails: { screen: SellerDetails },
    NewsFeed: { screen: NewsFeed },
    AddNewAd: { screen: AddNewAd },
    Profile: { screen: Profile },
    Message: { screen: Message },
    AddNewNews: { screen: AddNewNews },
    Otp: { screen: Otp },
    Pin: { screen: Pin },
    GetUserLocation: { screen: GetUserLocation },
    TransactionHistory: { screen: TransactionHistory },
    History: { screen: History },
    Promotion: { screen: Promotion },
    Favourites: { screen: Favourites },
    ManageCommercial: { screen: ManageCommercial },
    AddNew: { screen: AddNew },
    AddOffer: { screen: AddOffers },
    Language: { screen: Language },
    ResetPassword: { screen: ResetPassword },
    SellerContainer: { screen: SellerContainer },
    ChatMessages: { screen: ChatMessages },
    Help: { screen: Help },
    ChangePassword: { screen: ChangePassword },
    PaymentPlan: { screen: PaymentPlan },
    TermAndCondition: { screen: TermAndCondition },
    Payment: { screen: Payment },
    Notification: { screen: Notification },
    Subscription: {screen: Subscription},
  },
  {
    headerMode: 'screen',
    navigationOptions: {
      headerTitleStyle: {       
        textAlign: 'center',
        alignSelf: 'center',
      },
      headerRight: <View />
    }
  }
);

export default Routes;
